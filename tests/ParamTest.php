<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\ABase;
use jf\php\generator\collection\Attributes;
use jf\php\generator\IAttributes;
use jf\php\generator\IUses;
use jf\php\generator\Param;
use jf\php\generator\TAttributes;
use jf\php\generator\TCollectionItem;
use jf\php\generator\TDescription;
use jf\php\generator\TDocType;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Param::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IItem::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TDescription::class,
                TCollectionItem::class,
                TDocType::class
            ],
            'properties' => [
                'attributes'       => Attributes::class,
                'description'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'           => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'name'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'readonly'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'reference'        => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'scope'            => [
                    'type'  => 'string',
                    'value' => ''
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed',
                'variadic'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Param::__toString
    // Este método delega en buildCode así que probamos ambos métodos
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Param::new()->__toString());
    $runner->assertEquals([], Param::new()->buildCode());

    $runner->assertEquals('$abc', (string) Param::fromArray([ 'name' => 'abc' ]));
    $runner->assertEquals('float $xyz = 1.1', (string) Param::fromArray([ 'name' => 'xyz', 'value' => 1.1 ]));
    $runner->assertEquals('bool $xyz = FALSE', (string) Param::fromArray([ 'name' => 'xyz', 'value' => FALSE ]));
    $runner->assertEquals('NULL $xyz', (string) Param::fromArray([ 'name' => 'xyz', 'null' => 'NULL' ]));
    $runner->assertEquals('NULL $xyz = NULL', (string) Param::fromArray([ 'name' => 'xyz', 'value' => NULL ]));
    $runner->assertEquals(
        '?bool $xyz = FALSE',
        (string) Param::fromArray([ 'name' => 'xyz', 'null' => 'NULL', 'value' => FALSE ])
    );
    $runner->assertEquals('A|array|NULL $z', (string) Param::fromArray([ 'name' => 'z', 'type' => 'NULL|A[]|A' ]));
    $runner->assertEquals('mixed ...$xyz', (string) Param::fromArray([ 'name' => 'xyz', 'variadic' => TRUE ]));

    $runner->assertEquals(
        '#[A] #[B(1)] float $xyz = 1.1',
        (string) Param::fromArray(
            [
                'attributes' => [
                    [ 'name' => 'A' ],
                    [ 'name' => 'B', 'arguments' => [ 1 ] ]
                ],
                'name'       => 'xyz',
                'value'      => 1.1
            ]
        )
    );

    //------------------------------------------------------------------------------
    // Param::renderTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Param::new()->renderTags());
    $runner->assertEquals(
        '@param mixed ...$xyz',
        (string) Param::fromArray([ 'name' => 'xyz', 'variadic' => TRUE ])->renderTags()[0]
    );
    $runner->assertEquals(
        '@param bool $xyz',
        (string) Param::fromArray([ 'name' => 'xyz', 'value' => FALSE ])->renderTags()[0]
    );
    $runner->assertEquals(
        '@param NULL $xyz',
        (string) Param::fromArray([ 'name' => 'xyz', 'null' => 'NULL' ])->renderTags()[0]
    );
    $runner->assertEquals(
        '@param bool|NULL $xyz',
        (string) Param::fromArray([ 'name' => 'xyz', 'null' => 'NULL', 'value' => FALSE ])->renderTags()[0]
    );
    $runner->assertEquals(
        '@param A|A[]|NULL $z Lorem ipsum.',
        (string) Param::fromArray([ 'description' => 'lorem ipsum', 'name' => 'z', 'type' => 'NULL|A[]|A' ])->renderTags()[0]
    );
};