<?php

namespace jf\php\generator\tests;

use jf\Base\AAssignExtras;
use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Base\TDasherize;
use jf\php\generator\ABase;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    class ABaseTest extends ABase
    {
        public string $prop   = '';

        public array  $values = [];

        public function __toString() : string
        {
            return static::class;
        }

        public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
        {
            $this->values = $values;

            return parent::assign($values, $map, $prefixes);
        }
    }

    $runner->testClassDefinition(
        ABase::class,
        [
            'extends'    => AAssignExtras::class,
            'implements' => [
                IAssign::class,
                IToArray::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'properties' => [
                'extras' => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ],
            'traits'     => [
                TDasherize::class
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // ABase::__construct
    //------------------------------------------------------------------------------

    $value = random_bytes(16);
    $runner->assertEquals([ 'a' => 1, 'b' => $value ], ABaseTest::fromArray([ 'a' => 1, 'b' => $value ])->values);

    //------------------------------------------------------------------------------
    // ABase::assign
    //------------------------------------------------------------------------------

    $sut   = ABaseTest::new();
    $value = random_bytes(16);
    $runner->assertEquals([ 'a' => 1 ], $sut->assign([ 'a' => 1, 'prop' => $value ]));
    $runner->assertEquals($value, $sut->prop);

    //------------------------------------------------------------------------------
    // ABase::getClassname
    //------------------------------------------------------------------------------

    $runner->assertEquals('ABase', ABase::getClassname());
    $runner->assertEquals('ABaseTest', ABaseTest::getClassname());

    //------------------------------------------------------------------------------
    // ABase::new
    //------------------------------------------------------------------------------

    $runner->assertInstanceOf(ABase::class, ABaseTest::new());
    $runner->assertInstanceOf(ABaseTest::class, ABaseTest::new());
};