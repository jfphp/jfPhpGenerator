<?php

namespace jf\php\generator\tests;

use jf\php\generator\Formatter;
use jf\tests\Runner;

return function (Runner $runner)
{
    enum FormatterUnitEnum
    {
        case A;

        case B;
    }

    enum FormatterBackedEnum: string
    {
        case A = 'a';

        case B = 'b';
    }

    $runner->testClassDefinition(
        Formatter::class,
        [
            'properties' => [
                'count'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'int',
                    'value'    => 1
                ],
                'lineWidth' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'int',
                    'value'    => 80
                ]
            ]
        ]
    );

    $data1 = [
        'a' => NULL,
        'b' => [
            'c' => FALSE,
            'd' => TRUE,
            'e' => [
                'f' => 'hola',
                'g' => 1,
                'h' => 2.5
            ]
        ]
    ];

    $data2 = [
        NULL,
        FALSE,
        TRUE,
        'hola',
        1,
        2.5,
        [
            0,
            1,
            2
        ]
    ];

    $data3 = [
        'a' => NULL,
        'b' => [
            0,
            1,
            'c' => FALSE,
            'd' => TRUE,
            2
        ]
    ];

    //------------------------------------------------------------------------------
    // Formatter::addBackslash()
    //------------------------------------------------------------------------------

    $tests = [
        'Abc'      => 'Abc',
        '\Abc\Def' => 'Abc\Def',
        '\Gh\Ijk'  => '\Gh\Ijk',
        '\L\M\No'  => 'L\M\No',
    ];
    $runner->assertEquals('', Formatter::addBackslash(''));
    foreach ($tests as $expected => $test)
    {
        $runner->assertEquals($expected, Formatter::addBackslash($test));
    }
    $runner->assertArrayResult(array_keys($tests), array_values(Formatter::addBackslash($tests)));

    //------------------------------------------------------------------------------
    // Formatter::extractName()
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Formatter::extractName(''));
    $runner->assertEquals('Abc', Formatter::extractName('Abc'));
    $runner->assertEquals('Cde', Formatter::extractName('\Cde'));
    $runner->assertEquals('Jk', Formatter::extractName('Fghi\Jk'));
    $runner->assertEquals('Opq', Formatter::extractName('\Lmn\Opq'));

    //------------------------------------------------------------------------------
    // Formatter::formatJson()
    //------------------------------------------------------------------------------

    $runner->assertEquals('""', Formatter::formatJson(''));
    $runner->assertEquals('"\u{001b}"', Formatter::formatJson("\x1B"));

    //------------------------------------------------------------------------------
    // Formatter::formatRows()
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Formatter::formatRows([]));
    $runner->assertEquals([ [ 'a' ], [ 'bcd' ] ], Formatter::formatRows([ [ 'a' ], [ 'bcd' ] ]));
    $runner->assertEquals([ [ 'a  ', '1234' ], [ 'bcd', '5' ] ], Formatter::formatRows([ [ 'a', 1234 ], [ 'bcd', 5 ] ]));
    $runner->assertEquals([ [ 'a  ', '1234' ], [ 'bcd', '5' ] ], Formatter::formatRows([ [ 'a', '', 1234 ], [ 'bcd', '', 5 ] ]));

    // Si una columna es multilínea se debe rellenar las demás columnas con espacios en blanco.
    $lipsum = wordwrap(
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat',
        25
    );
    $runner->assertEquals(
        [
            [ 'a  ', "lorem ipsum\n    sit amet" ],
            [ 'bcd', ucfirst(trim(preg_replace('/^/m', '    ', $lipsum))) ],
            [ 'x  ', '7' ]
        ],
        Formatter::formatRows([ [ 'a', "lorem ipsum\nsit amet" ], [ 'bcd', $lipsum ], [ 'x', 7 ] ])
    );

    //------------------------------------------------------------------------------
    // Formatter::formatValue()
    // El método `formatValue` llama al resto de métodos `format*` de la clase.
    //------------------------------------------------------------------------------

    $runner->assertEquals("''", Formatter::formatValue(''));
    $runner->assertEquals('[]', Formatter::formatValue([]));
    $runner->assertEquals('NULL', Formatter::formatValue(NULL));
    $runner->assertEquals('FALSE', Formatter::formatValue(FALSE));
    $runner->assertEquals('TRUE', Formatter::formatValue(TRUE));
    $runner->assertEquals('1', Formatter::formatValue(1));
    $runner->assertEquals('PHP_EOL', Formatter::formatValue(PHP_EOL));
    $runner->assertEquals("'\\\\'", Formatter::formatValue('\\'));
    $runner->assertEquals('"a\nb"', Formatter::formatValue("a\nb"));
    $runner->assertEquals('0', Formatter::formatValue(0));
    $runner->assertEquals('0.0', Formatter::formatValue(0.0));
    $runner->assertEquals('0.1', Formatter::formatValue(0.1));
    $runner->assertEquals('"\u{001b}"', Formatter::formatValue("\x1B"));
    $runner->assertEquals(Runner::getVariable('data1', __FILE__), Formatter::formatValue($data1));
    $runner->assertEquals(Runner::getVariable('data2', __FILE__), Formatter::formatValue($data2));
    $runner->assertEquals(Runner::getVariable('data3', __FILE__), Formatter::formatValue($data3));
    $count            = Formatter::$count;
    Formatter::$count = 0;
    $runner->assertEquals("[\n    'a' => 1\n]", Formatter::formatValue([ 'a' => 1 ]));
    $runner->assertEquals("[\n    1.23\n]", Formatter::formatValue([ 1.23 ]));
    Formatter::$count = 1;
    $runner->assertEquals("[ 'a' => 1 ]", Formatter::formatValue([ 'a' => 1 ]));
    $runner->assertEquals('[ 1.23 ]', Formatter::formatValue([ 1.23 ]));
    Formatter::$count = $count;

    $runner->assertEquals(
        "[\n    1.23,\n    FALSE,\n    'abc',\n    'A',\n    'b'\n]",
        Formatter::formatValue([ 1.23, FALSE, 'abc', FormatterUnitEnum::A, FormatterBackedEnum::B ])
    );
    //------------------------------------------------------------------------------
    // Formatter::getFqcn()
    //------------------------------------------------------------------------------

    $runner->assertEquals('A', Formatter::getFqcn('A'));
    $runner->assertEquals('A', Formatter::getFqcn('\A'));
    $runner->assertEquals('A\B', Formatter::getFqcn('A\B'));
    $runner->assertEquals('A\B', Formatter::getFqcn('B', 'A'));
    $runner->assertEquals('A\B', Formatter::getFqcn([ 'name' => 'B', 'namespace' => 'A' ]));

    //------------------------------------------------------------------------------
    // Formatter::indent() & Formatter::unindent()
    //------------------------------------------------------------------------------

    foreach (
        [
            [ '', '    ' ],
            [ 'a', '    a' ],
            [ "a\nb", "    a\n    b" ],
            [ [ 'a', 'b' ], [ '    a', '    b' ] ],
            [ "\na\nb\n\nc\n", "    \n    a\n    b\n    \n    c\n" ]
        ] as $data)
    {
        $text       = $data[0];
        $indented   = $data[1];
        $unindented = $data[2] ?? $text;
        $runner->assertEquals($indented, Formatter::indent($text));
        $runner->assertEquals($unindented, Formatter::unindent($indented));
    }

    //------------------------------------------------------------------------------
    // Formatter::ltrim()
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Formatter::ltrim(''));
    $runner->assertEquals('Abc', Formatter::ltrim('Abc'));
    $runner->assertEquals('Cde', Formatter::ltrim('\Cde'));
    $runner->assertEquals('Fghi\Jk', Formatter::ltrim('Fghi\Jk'));
    $runner->assertEquals('Lmn\Opq', Formatter::ltrim('\Lmn\Opq'));

    $runner->assertEquals(
        [ '', 'Abc', 'Cde', 'Fghi\Jk', 'Lmn\Opq' ],
        Formatter::ltrim([ '', 'Abc', '\Cde', 'Fghi\Jk', '\Lmn\Opq' ])
    );

    //------------------------------------------------------------------------------
    // Formatter::splitName()
    //------------------------------------------------------------------------------

    $runner->assertArrayResult([ '', '' ], Formatter::splitName(''));
    $runner->assertArrayResult([ '', 'Abc' ], Formatter::splitName('Abc'));
    $runner->assertArrayResult([ 'Abc', 'Cde' ], Formatter::splitName('Abc\Cde'));

    //------------------------------------------------------------------------------
    // Formatter::wordwrap()
    //------------------------------------------------------------------------------

    $current              = Formatter::$lineWidth;
    Formatter::$lineWidth = 5;
    $string               = 'a b c d e f g h i j k l m n o p q r s t u v w x y z';
    $runner->assertEquals("a b c\nd e f\ng h i\nj k l\nm n o\np q r\ns t u\nv w x\ny z", Formatter::wordwrap($string, 0));
    $runner->assertEquals("a b c\nd e f\ng h i\nj k l\nm n o\np q r\ns t u\nv w x y z", Formatter::wordwrap($string));
    Formatter::$lineWidth = 10;
    $runner->assertEquals("a b c d e\nf g h i j\nk l m n o\np q r s t\nu v w x y\nz", Formatter::wordwrap($string, 0));
    $runner->assertEquals("a b c d e\nf g h i j\nk l m n o\np q r s t\nu v w x y z", Formatter::wordwrap($string));
    Formatter::$lineWidth = $current;
};