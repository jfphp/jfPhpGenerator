<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TTranslations;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TTranslationsTest extends ABase
    {
        use TTranslations;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TTranslations::class,
        [
            'properties' => [
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TTranslations::tr
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TTranslationsTest::tr(''));
    $runner->assertEquals('Pregunta 1 de 5', TTranslationsTest::tr('Pregunta {0} de {1}', 1, 5));
    $runner->assertEquals('abc', TTranslationsTest::tr('abc'));
    TTranslationsTest::$translations['abc'] = 'def';
    $runner->assertEquals('def', TTranslationsTest::tr('abc'));
    unset(TTranslationsTest::$translations['abc']);
    $runner->assertEquals('abc', TTranslationsTest::tr('abc'));
};