<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\ABase;
use jf\php\generator\collection\TraitMethods;
use jf\php\generator\IUses;
use jf\php\generator\TDocTags;
use jf\php\generator\TName;
use jf\php\generator\TraitItem;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        TraitItem::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IItem::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TDocTags::class,
                TName::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'methods'      => TraitMethods::class,
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TraitItem::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TraitItem::new()->__toString());
    $runner->assertEquals('use Ab;', TraitItem::fromArray([ 'name' => 'Ab' ])->__toString());
    $runner->assertEquals('use A\B;', TraitItem::fromArray([ 'name' => 'A\B' ])->__toString());
    $runner->assertEquals('use B;', TraitItem::fromArray([ 'name' => '\A\B' ])->__toString());
    $runner->assertEquals('use C;', TraitItem::fromArray([ 'name' => 'A\B', 'alias' => 'C' ])->__toString());
    $runner->assertEquals('use C;', TraitItem::fromArray([ 'name' => '\A\B', 'alias' => 'C' ])->__toString());

    //------------------------------------------------------------------------------
    // TraitItem::addMethods & TraitItem::buildMethods
    // Este método delega en TCollection::_addToCollection() y TCollection::_getCollection
    //------------------------------------------------------------------------------

    $sut = TraitItem::fromArray([ 'name' => 'WithMethods', 'namespace' => 'Abc' ]);
    $sut->addMethods(
        [
            [ 'name' => 'method1' ],
            [ 'name' => 'method2', 'alias' => 'meth2' ],
            [ 'name' => 'method3', 'alias' => 'meth3', 'scope' => 'private' ]
        ]
    );
    $runner->assertArrayResult(
        [
            'method1 as private _method1;',
            'method2 as meth2;',
            'method3 as private meth3;'
        ],
        $sut->buildMethods()
    );
    $runner->assertEquals(
        <<<PHP
use WithMethods
{
    WithMethods::method1 as private _method1;
    WithMethods::method2 as meth2;
    WithMethods::method3 as private meth3;
}
PHP,
        (string) $sut
    );

    $runner->assertEquals(
        <<<PHP
/**
 * @use A\B\C
 */
use WithMethodsAndTags;
PHP,
        (string) TraitItem::fromArray(
            [
                'name'      => 'WithMethodsAndTags',
                'namespace' => 'Tags',
                'tags'      => [ [ 'name' => 'use', 'value' => 'A\B\C' ] ]
            ]
        )
    );
    //------------------------------------------------------------------------------
    // TraitItem::getUses
    // Este método delega en TName::getNamespaceUses()
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TraitItem::new()->getUses());
    $runner->assertEquals([], TraitItem::fromArray([ 'name' => 'Ab' ])->getUses());
    $runner->assertEquals([ 'Ab' => '' ], TraitItem::fromArray([ 'name' => '\Ab' ])->getUses());
    $runner->assertEquals([ 'A' => '' ], TraitItem::fromArray([ 'name' => '\A' ])->getUses());
    $runner->assertEquals([ 'A\B' => '' ], TraitItem::fromArray([ 'name' => 'A\B' ])->getUses());
    $runner->assertEquals([ 'A\B' => '' ], TraitItem::fromArray([ 'name' => '\A\B' ])->getUses());
    $runner->assertEquals([ 'A\B' => 'C' ], TraitItem::fromArray([ 'name' => 'A\B', 'alias' => 'C' ])->getUses());
    $runner->assertEquals([ 'A\B' => 'C' ], TraitItem::fromArray([ 'name' => '\A\B', 'alias' => 'C' ])->getUses());

    //------------------------------------------------------------------------------
    // TraitItem::setName
    //------------------------------------------------------------------------------

    $tests = [
        [ 'A', '', 'A' ],
        [ '\A', '\\', 'A' ],
        [ '\A\B', 'A', 'B' ],
        [ 'A\B', '', 'A\B' ],
    ];
    foreach ($tests as [$name, $ns, $expected])
    {
        $sut = TraitItem::fromArray([ 'name' => $name ]);
        $runner->assertEquals($expected, $sut->getName());
        $runner->assertEquals($ns, $sut->namespace);
    }
};