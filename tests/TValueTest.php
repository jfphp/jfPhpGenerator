<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TValue;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TValueTest extends ABase
    {
        use TValue;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $config = [
        'properties' => [
            'hasValue' => [
                'nullable' => FALSE,
                'type'     => 'bool',
                'value'    => FALSE
            ],
            'value'    => 'mixed',
            'null'     => [
                'nullable' => FALSE,
                'type'     => 'string',
                'value'    => ''
            ]
        ]
    ];

    $runner->testClassDefinition(TValue::class, $config);

    //------------------------------------------------------------------------------
    // TValue::renderValue()
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TValueTest::new()->renderValue());
    $runner->assertEquals('', TValueTest::fromArray([ 'value' => NULL ])->renderValue());
    $runner->assertEquals('= NULL', TValueTest::fromArray([ 'null' => 'NULL', 'value' => NULL ])->renderValue());
    $runner->assertEquals('= FALSE', TValueTest::fromArray([ 'value' => FALSE ])->renderValue());
    $runner->assertEquals('= TRUE', TValueTest::fromArray([ 'value' => TRUE ])->renderValue());
    $runner->assertEquals('= 0', TValueTest::fromArray([ 'value' => 0 ])->renderValue());
    $runner->assertEquals('= 1', TValueTest::fromArray([ 'value' => 1 ])->renderValue());
    $runner->assertEquals('= 1.1', TValueTest::fromArray([ 'value' => 1.1 ])->renderValue());
    $runner->assertEquals('= \'\'', TValueTest::fromArray([ 'value' => '' ])->renderValue());
    $runner->assertEquals(
        '= ' . Runner::getVariable('config', __FILE__),
        TValueTest::fromArray([ 'value' => $config ])->renderValue()
    );
};