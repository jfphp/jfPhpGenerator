<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\collection\Methods;
use jf\php\generator\Method;
use jf\php\generator\TMethods;
use jf\php\generator\TName;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TMethodsTest extends ABase
    {
        use TName;
        use TMethods
        {
            _modifyMethod as private __modifyMethod;
        }

        public int     $calls  = 0;

        public ?Runner $runner = NULL;

        protected function _modifyMethod(Method $method) : bool
        {
            $this->__modifyMethod($method);
            $this->runner->assertEquals($this->methods[ $method->name ], $method);
            ++$this->calls;

            return TRUE;
        }

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TMethods::class,
        [
            'properties' => [
                'methods' => Methods::class,
                'name'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TMethods::addConstructor
    //------------------------------------------------------------------------------

    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'body'        => '',
                    'description' => 'Constructor de la clase ``',
                    'name'        => '__construct',
                    'override'    => TRUE,
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'runner' => $runner ])->addConstructor([])->buildMethods()
    );
    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'body'        => 'lorem ipsum',
                    'description' => 'Constructor de la clase `Abc\Def`',
                    'name'        => '__construct',
                    'override'    => TRUE,
                    'params'      => [ [ 'name' => 'a' ] ],
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'name' => 'Def', 'namespace' => 'Abc', 'runner' => $runner ])
            ->addConstructor([ 'body' => 'lorem ipsum', 'params' => [ [ 'name' => 'a' ] ] ])
            ->buildMethods()
    );
    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'body'        => 'lorem ipsum',
                    'description' => 'Constructor de la clase `Xy\Z`',
                    'name'        => '__construct',
                    'override'    => TRUE,
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'name' => 'Z', 'namespace' => 'Xy', 'runner' => $runner ])
            ->addConstructor('lorem ipsum')
            ->buildMethods()
    );

    //------------------------------------------------------------------------------
    // TMethods::addDestructor
    //------------------------------------------------------------------------------

    $body = Runner::getRandomMd5();
    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'description' => 'Destructor de la clase ``',
                    'name'        => '__destruct',
                    'override'    => TRUE,
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'runner' => $runner ])->addDestructor([])->buildMethods()
    );
    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'body'        => $body,
                    'description' => 'Destructor de la clase `Abc\Def`',
                    'name'        => '__destruct',
                    'override'    => TRUE,
                    'params'      => [ [ 'name' => 'a' ] ],
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'name' => 'Def', 'namespace' => 'Abc', 'runner' => $runner ])
            ->addDestructor([ 'body' => $body, 'params' => [ [ 'name' => 'a' ] ] ])
            ->buildMethods()
    );
    $runner->assertEquals(
        [
            (string) Method::fromArray(
                [
                    'body'        => $body,
                    'description' => 'Destructor de la clase `Xy\Z`',
                    'name'        => '__destruct',
                    'override'    => TRUE,
                    'type'        => NULL
                ]
            )
        ],
        TMethodsTest::fromArray([ 'name' => 'Z', 'namespace' => 'Xy', 'runner' => $runner ])
            ->addDestructor($body)
            ->buildMethods()
    );

    //------------------------------------------------------------------------------
    // TMethods::addMethods
    //------------------------------------------------------------------------------

    $runner->assertEquals(NULL, TMethodsTest::new()->addMethods([])->methods);

    //------------------------------------------------------------------------------
    // TMethods::buildMethods
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TMethodsTest::new()->buildMethods());
    $runner->assertEquals(
        [ 'abstract public function abc() : void;' ],
        TMethodsTest::fromArray([ 'runner' => $runner, 'methods' => [ [ 'abstract' => TRUE, 'name' => 'abc' ] ] ])->buildMethods()
    );
    $sut = TMethodsTest::fromArray(
        [
            'runner'  => $runner,
            'methods' => [
                [ 'name' => 'bcd', 'body' => '' ],
                [ 'name' => '_abc', 'body' => '' ],
                [ 'name' => '__bcd', 'body' => '' ],
                [ 'name' => '__abc', 'body' => '' ],
                [ 'name' => 'xyz', 'body' => 'return xyz;', 'static' => TRUE ]
            ]
        ]
    );
    $runner->assertEquals(
        [
            "public function __abc() : void\n{\n}",
            "public function __bcd() : void\n{\n}",
            "protected function _abc() : void\n{\n}",
            "public function bcd() : void\n{\n}",
            "public static function xyz() : void\n{\n    return xyz;\n}",
        ],
        $sut->buildMethods()
    );
    $runner->assertEquals(5, $sut->calls);
};