<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\collection\Classnames;
use jf\php\generator\Exception;
use jf\php\generator\TraitItem;
use jf\php\generator\TTranslations;
use jf\php\generator\TUses;
use jf\tests\Runner;

include_once __DIR__ . '/../../autoload.php';

return function (Runner $runner)
{
    class TUsesTest extends ABase
    {
        use TUses;

        public Classnames $items;

        public array      $uses = [];

        public function __construct(array $values = [])
        {
            $this->items = new Classnames();
            parent::__construct($values);
        }

        public function __toString() : string
        {
            return self::class;
        }

        public function addItems(array $items) : void
        {
            $this->items->addItems($items);
        }

        public function parseUse(string $use) : string
        {
            return $this->_parseUse($use);
        }
    }

    $runner->testClassDefinition(
        TUses::class,
        [
            'properties' => [
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ],
            'traits'     => [ TTranslations::class ]
        ]
    );

    $sut = new TUsesTest();
    $sut->addUses('A');
    $sut->addUses(
        [
            'a\b\X'  => 'X',
            'B'      => 'C',
            '\C',
            '\D'     => 'E',
            'F\G'    => 'H',
            '\I\J'   => 'K',
            'L\M\N'  => 'O',
            '\P\Q\R' => 'S',
            '\T\U',
            'V\W',
            '\X\Y\Z'
        ]
    );
    $runner->assertArrayResult(
        [
            'A'     => '',
            'a\b\X' => '',
            'B'     => 'C',
            'C'     => '',
            'D'     => 'E',
            'F\G'   => 'H',
            'I\J'   => 'K',
            'L\M\N' => 'O',
            'P\Q\R' => 'S',
            'T\U'   => '',
            'V\W'   => '',
            'X\Y\Z' => ''
        ],
        $sut->getUses()
    );
    $runner->assertArrayResult(
        [
            'use A;',
            'use a\b\X;',
            'use B as C;',
            'use C;',
            'use D as E;',
            'use F\G as H;',
            'use I\J as K;',
            'use L\M\N as O;',
            'use P\Q\R as S;',
            'use T\U;',
            'use V\W;',
            'use X\Y\Z;'
        ],
        $sut->renderUses()
    );

    //------------------------------------------------------------------------------
    // TUses::addUses
    //------------------------------------------------------------------------------

    $sut = new TUsesTest();
    $runner->assertEquals(
        [ 'jf\Collection\Collection<jf\php\generator\Property>' => 'jf\Collection\Collection<jf\php\generator\Property>' ],
        $sut->addUses('jf\Collection\Collection<jf\php\generator\Property>')
    );
    $runner->assertEquals(
        [
            'jf\Collection\Collection'                              => '',
            'jf\php\generator\Property'                             => '',
            '\jf\Collection\Collection<\jf\php\generator\Property>' => 'Collection<Property>'
        ],
        $sut->addUses('\jf\Collection\Collection<\jf\php\generator\Property>')
    );
    $runner->assertEquals([ 'jf\Collection\Collection' => '', 'jf\php\generator\Property' => '' ], $sut->getUses());

    $sut = new TUsesTest();
    $runner->assertEquals(
        [
            'jf\Collection\Collection'                                     => 'C',
            'jf\php\generator\Property'                                    => 'Prop',
            '\jf\Collection\Collection@C<\jf\php\generator\Property@Prop>' => 'C<Prop>'
        ],
        $sut->addUses('\jf\Collection\Collection@C<\jf\php\generator\Property@Prop>')
    );
    $runner->assertEquals([ 'jf\Collection\Collection' => 'C', 'jf\php\generator\Property' => 'Prop' ], $sut->getUses());

    try
    {
        $exception = NULL;
        TUsesTest::fromArray([ 'uses' => [ '\Abc\Def' => '' ] ])->addUses([ '\Abc\Def' => 'A' ]);
    }
    catch (Exception $e)
    {
        $exception = $e;
        $runner->assertEquals('Alias no coinciden: `` !== `A`', $e->getMessage());
    }
    $runner->assertInstanceOf(Exception::class, $exception);

    //------------------------------------------------------------------------------
    // TUses::extractUses
    //------------------------------------------------------------------------------

    $runner->assertArrayResult([], TUsesTest::extractUses([]));
    $runner->assertArrayResult([], TUsesTest::extractUses(TraitItem::fromArray([ 'name' => '\A\B' ])));
    $runner->assertArrayResult(
        [ 'B' => '', 'D\E' => '', 'F\G' => 'H' ],
        TUsesTest::extractUses(
            TUsesTest::fromArray(
                [
                    'name'  => '\A',
                    'items' => [
                        '\B',
                        'C',
                        '\D\E',
                        'F\G' => 'H'
                    ]
                ]
            )
        )
    );

    //------------------------------------------------------------------------------
    // TUses::parseUse
    //------------------------------------------------------------------------------

    $sut = new TUsesTest();
    $runner->assertEquals('Abc', $sut->parseUse('Abc'));
    $runner->assertEquals([], $sut->getUses());

    $sut = new TUsesTest();
    $runner->assertEquals('Abc\Def', $sut->parseUse('Abc\Def'));
    $runner->assertEquals([], $sut->getUses());

    $sut = new TUsesTest();
    $runner->assertEquals('Def', $sut->parseUse('\Abc\Def'));
    $runner->assertEquals([ 'Abc\Def' => '' ], $sut->getUses());

    //------------------------------------------------------------------------------
    // TUses::renderUses
    //------------------------------------------------------------------------------

    $sut = new TUsesTest();
    $sut->addUses(
        [
            'A',
            'B\C',
            'B\D\E',
            '\B\D\F',
            'B\D\G',
            '\B\D\H',
            'I\J[]',
        ]
    );
    $runner->assertEquals(
        [ 'use A;', 'use B\C;', 'use B\D\E;', 'use B\D\F;', 'use B\D\G;', 'use B\D\H;', 'use I\J;' ],
        $sut->renderUses()
    );
    $runner->assertEquals([ 'use A;', 'use B\D\E;', 'use B\D\F;', 'use B\D\G;', 'use B\D\H;', 'use I\J;' ], $sut->renderUses('B'));
    $runner->assertEquals([ 'use A;', 'use B\C;', 'use I\J;' ], $sut->renderUses('\B\D'));
};