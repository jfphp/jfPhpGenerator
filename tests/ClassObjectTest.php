<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\Classname;
use jf\php\generator\ClassObject;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Classes;
use jf\php\generator\collection\Classnames;
use jf\php\generator\collection\Methods;
use jf\php\generator\collection\Properties;
use jf\php\generator\collection\TraitItems;
use jf\php\generator\IAttributes;
use jf\php\generator\IExtends;
use jf\php\generator\IGenerator;
use jf\php\generator\IImplements;
use jf\php\generator\IMethods;
use jf\php\generator\IProperties;
use jf\php\generator\ITraits;
use jf\php\generator\IUses;
use jf\php\generator\TAttributes;
use jf\php\generator\TExtends;
use jf\php\generator\TGenerator;
use jf\php\generator\TImplements;
use jf\php\generator\TMethods;
use jf\php\generator\TProperties;
use jf\php\generator\TTraits;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        ClassObject::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IExtends::class,
                IGenerator::class,
                IImplements::class,
                IMethods::class,
                IProperties::class,
                IToArray::class,
                ITraits::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TExtends::class,
                TGenerator::class,
                TImplements::class,
                TMethods::class,
                TProperties::class,
                TTraits::class
            ],
            'properties' => [
                'abstract'     => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'arguments'    => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'attributes'   => Attributes::class,
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extends'      => Classnames::class,
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'final'        => 'bool',
                'implements'   => Classnames::class,
                'methods'      => Methods::class,
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'properties'   => Properties::class,
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'traits'       => TraitItems::class,
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // ClassObject::addImplements
    //------------------------------------------------------------------------------

    $sut = ClassObject::new();
    $runner->assertInstanceOf(ClassObject::class, $sut->addImplements(''));
    $sut->addImplements('');
    $runner->assertEquals(NULL, $sut->implements);
    $sut->addImplements([]);
    $runner->assertEquals(NULL, $sut->implements);
    $sut->addImplements('A\B');
    $runner->assertArrayCount(1, $sut->implements);
    $runner->assertInstanceOf(Classname::class, $sut->implements->first());
    $runner->assertEquals('A\B', $sut->implements->first()->getFqcn());

    //------------------------------------------------------------------------------
    // ClassObject::buildCode
    //------------------------------------------------------------------------------

    $tests = [
        [ [ 'class', '{', '}' ], [] ],
        [ [ 'class($a, $b)', '{', '}' ], [ 'arguments' => [ '$a', '$b' ] ] ],
        [ [ 'class Abc', '{', '}' ], [ 'name' => 'Abc' ] ],
        [ [ 'final class Abc', '{', '}' ], [ 'final' => TRUE, 'name' => 'Abc' ] ],
        [ [ 'abstract class Abc', '{', '}' ], [ 'abstract' => TRUE, 'name' => 'Abc' ] ],
        [ [ 'abstract class Abc', '{', '}' ], [ 'abstract' => TRUE, 'final' => TRUE, 'name' => 'Abc' ] ],
        [ [ 'class Abc extends Cde', '{', '}' ], [ 'name' => 'Abc', 'extends' => 'Cde' ] ],
        [ [ 'class Abc extends Cde implements Fghi', '{', '}' ], [ 'name' => 'Abc', 'extends' => 'Cde', 'implements' => 'Fghi' ] ],
    ];
    $items = [];
    $code  = [];
    foreach ($tests as [$expected, $values])
    {
        $class = ClassObject::fromArray($values);
        // Omitimos las clases anónimas
        if ($class->name)
        {
            $items[] = $values;

            $code[ $class->getCollectionKey() ] = implode(PHP_EOL, $expected);
        }
        $runner->assertEquals($expected, $class->buildCode());
    }
    $runner->assertEquals(array_values($code), Classes::fromItems($items)->render());

    //------------------------------------------------------------------------------
    // ClassObject::buildImplements
    //------------------------------------------------------------------------------

    $runner->assertEquals([], ClassObject::new()->buildImplements());
    $runner->assertArrayResult(
        [ 'A', 'A\B', 'Ab', 'C' ],
        ClassObject::fromArray([ 'implements' => [ [ 'name' => 'Ab' ], '\A', 'A\B', '\A\B' => 'C' ] ])->buildImplements()
    );

    //------------------------------------------------------------------------------
    // ClassObject::_modifyMethod
    //------------------------------------------------------------------------------

    $runner->assertEquals(
        "abstract class C\n{\n    abstract public function test() : void;\n}",
        ClassObject::fromArray([ 'abstract' => TRUE, 'name' => 'C', 'methods' => [ 'test' => [] ] ])->__toString()
    );
    $runner->assertEquals(
        "abstract class C\n{\n    public function test() : void\n    {\n    }\n}",
        ClassObject::fromArray([ 'abstract' => TRUE, 'name' => 'C', 'methods' => [ 'test' => [ 'body' => '' ] ] ])->__toString()
    );

    //------------------------------------------------------------------------------
    // ClassObject::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals("class\n{\n}", ClassObject::new()->__toString());

    $runner->assertStringEquals(
        <<<'PHP'
/**
 * Class description.
 */
class LoremIpsum extends ABase implements C, D, F
{
    /**
     * Lorem description.
     *
     * @var string
     */
    public string $a = '';

    /**
     * Description 2.
     *
     * @var float
     */
    public float $b = 1.23;

    /**
     * Description method 1.
     *
     * @return bool
     */
    public function meth1() : bool
    {
        return TRUE;
    }
}
PHP,
        (string) ClassObject::fromArray(
            [
                'description' => 'Class description',
                'name'        => 'LoremIpsum',
                'extends'     => 'ABase',
                'implements'  => [
                    '\A\B' => 'C',
                    'D',
                    '\E\F'
                ],
                'methods'     => [
                    'meth1' => [
                        'body'        => '',
                        'description' => 'description method 1',
                        'type'        => 'bool'
                    ]
                ],
                'properties'  => [
                    'a' => 'lorem description',
                    'b' => [
                        'description' => 'description 2',
                        'value'       => 1.23
                    ]
                ]
            ]
        )
    );
};