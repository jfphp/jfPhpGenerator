<?php

namespace jf\php\generator\tests;

use jf\php\generator\IGenerator;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IGenerator::class,
        [
            'methods' => [
                'buildCode' => [
                    'type' => 'array',
                    'value' => '[]'
                ]
            ]
        ]
    );
};