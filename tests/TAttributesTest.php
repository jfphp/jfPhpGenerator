<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\Attribute;
use jf\php\generator\collection\Attributes;
use jf\php\generator\TAttributes;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TAttributesTest extends ABase
    {
        use TAttributes;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TAttributes::class,
        [
            'properties' => [
                'attributes' => Attributes::class,
                'name'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TAttributes::addAttributes
    //------------------------------------------------------------------------------

    $data = [
        [ 'name' => 'a' ],
        [ 'name' => 'b' ],
        [ 'name' => 'c', 'arguments' => [ 'd' ] ],
        [ 'name' => 'e', 'arguments' => [ 'f', 'g' => 1 ] ],
        [ 'name' => 'h', 'arguments' => [ 'h', 'i' => 'A::class', 'j', 'k' => TRUE ] ],
    ];
    $sut  = TAttributesTest::fromArray([ 'attributes' => $data ]);
    $runner->assertArrayCount(count($data), $sut->attributes);
    foreach ($sut->attributes as $attribute)
    {
        $runner->assertInstanceOf(Attribute::class, $attribute);
        $config = current($data);
        $runner->assertArrayResult(
            $config + [
                'alias'        => '',
                'arguments'    => [],
                'name'         => '',
                'namespace'    => '',
                'package'      => '',
                'subnamespace' => ''
            ],
            $attribute->toArray()
        );
        next($data);
    }

    //------------------------------------------------------------------------------
    // TAttributes::buildAttributes
    //------------------------------------------------------------------------------

    $runner->assertArrayResult(
        [
            '#[a]',
            '#[b]',
            '#[c(\'d\')]',
            '#[e(\'f\', g: 1)]',
            '#[h(\'h\', \'j\', i: A::class, k: TRUE)]'
        ],
        $sut->buildAttributes()
    );
};