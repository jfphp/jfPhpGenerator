<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TDescription;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TDescriptionTest extends ABase
    {
        use TDescription;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TDescription::class,
        [
            'properties' => [
                'description' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TDescription::renderDescription
    //------------------------------------------------------------------------------

    $description = chr(rand(ord('a'), ord('z'))) . Runner::getRandomMd5();
    $expected    = ucfirst($description) . '.';
    $runner->assertEquals('', TDescriptionTest::new()->renderDescription());
    $runner->assertEquals(
        $expected,
        TDescriptionTest::fromArray([ 'description' => lcfirst($description) . '.' ])->renderDescription()
    );
    $runner->assertEquals(
        $expected,
        TDescriptionTest::fromArray([ 'description' => ucfirst($description) . '.' ])->renderDescription()
    );
    $runner->assertEquals($expected, TDescriptionTest::fromArray([ 'description' => lcfirst($description) ])->renderDescription());
    $runner->assertEquals($expected, TDescriptionTest::fromArray([ 'description' => ucfirst($description) ])->renderDescription());
    $runner->assertEquals($expected, TDescriptionTest::fromArray([ 'description' => "$description  ;:" ])->renderDescription());

    //------------------------------------------------------------------------------
    // TDescription::setDescription
    //------------------------------------------------------------------------------

    $runner->assertEquals($description, TDescriptionTest::fromArray([ 'description' => $description ])->description);
    $runner->assertEquals($description, TDescriptionTest::fromArray([ 'description' => [ $description ] ])->description);
    $runner->assertEquals(
        "$description\n$description\n$description",
        TDescriptionTest::fromArray([ 'description' => [ $description, $description, $description ] ])->description
    );
};