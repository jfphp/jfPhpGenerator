<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\Attribute;
use jf\php\generator\IUses;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Attribute::class,
        [
            'extends'    => 'jf\php\generator\Classname',
            'implements' => [
                IAssign::class,
                IItem::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'arguments'    => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    $cases = [
        //@formatter:off
        [ 'Abc'                     , 'Abc'                                        ],
        [ 'Abc'                     , 'Ns\Abc'                                     ],
        [ 'Abc'                     , '\Ns\Abc'                                    ],
        [ 'Abc(a: 1)'               , '\Ns\Abc', [ 'a' => 1                      ] ],
        [ 'Z(a: 1, b: 2)'           , 'X\Y\Z'  , [ 'a' => 1, 'b' => 2            ] ],
        [ 'Z(a: 1, b: 2)'           , '\X\Y\Z' , [ 'a' => 1, 'b' => 2            ] ],
        [ 'Z(123, a: 1, b: 2)'      , '\X\Y\Z' , [ 'a' => 1, 'b' => 2, 123       ] ],
        [ 'D(B::class, a: A::class)', '\B\C\D' , [ 'a' => 'A::class', 'B::class' ] ],
        //@formatter:on
    ];
    foreach ($cases as $config)
    {
        [ $expected, $name, $arguments ] = $config + [ '', '', [] ];
        $attribute = Attribute::fromArray([ 'name' => $name, 'arguments' => $arguments ]);
        $runner->assertEquals(sprintf('#[%s]', $expected), (string) $attribute);
    }
};