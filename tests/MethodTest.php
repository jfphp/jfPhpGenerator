<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Params;
use jf\php\generator\FunctionItem;
use jf\php\generator\IAttributes;
use jf\php\generator\IGenerator;
use jf\php\generator\IParams;
use jf\php\generator\IUses;
use jf\php\generator\Method;
use jf\php\generator\TModifiers;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Method::class,
        [
            'extends'    => FunctionItem::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IItem::class,
                IGenerator::class,
                IParams::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [ TModifiers::class ],
            'properties' => [
                'abstract'         => 'bool',
                'attributes'       => Attributes::class,
                'body'             => 'string',
                'const'            => 'bool',
                'copydoc'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'           => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'final'            => 'bool',
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'keyword'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => 'function'
                ],
                'name'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'override'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'params'           => Params::class,
                'return'           => [],
                'scope'            => [
                    'type'  => 'string',
                    'value' => ''
                ],
                'static'           => [
                    'type'  => 'bool',
                    'value' => FALSE
                ],
                'tags'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Method::__toString
    // Este método delega en el resultado de buildCode, solamente se comprueba
    // lo que se agrega a este resultado.
    //------------------------------------------------------------------------------

    $runner->assertEquals('abstract public function () : void;', Method::fromArray([ 'abstract' => TRUE ])->__toString());

    $sut                    = Method::fromArray(
        [
            'body'   => 'return $a + $b + $c;',
            'name'   => 'abc',
            'null'   => 'NULL',
            'static' => TRUE,
            'type'   => 'string'
        ]
    );
    $sut->getParams()->type = '';
    $sut->addParams([ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ]);
    $runner->assertEquals(
        <<<'PHP'
/**
 * @param $a
 * @param $b
 * @param $c
 *
 * @return string|NULL
 */
public static function abc($a, $b, $c) : ?string
{
    return $a + $b + $c;
}
PHP,
        $sut->__toString()
    );
    //------------------------------------------------------------------------------
    // Method::buildCode
    //------------------------------------------------------------------------------

    $runner->assertEquals([ 'public function () : void;' ], Method::new()->buildCode());
    $runner->assertEquals(
        [ 'abstract private function () : void;' ],
        Method::fromArray([ 'abstract' => TRUE, 'scope' => 'private' ])->buildCode()
    );
    $runner->assertEquals([ 'public function () : void', '{', '}' ], Method::fromArray([ 'body' => '' ])->buildCode());
    $runner->assertEquals(
        [ 'public static function () : void', '{', '    abc', '}' ],
        Method::fromArray([ 'body' => 'abc', 'static' => TRUE ])->buildCode()
    );
    // Inferiendo si es estático o no

    $runner->assertEquals(
        [ 'public static function () : void', '{', '    abc', '}' ],
        Method::fromArray([ 'body' => 'abc', 'static' => NULL ])->buildCode()
    );
    $runner->assertEquals(
        [ 'public function () : void', '{', '    $this->abc();', '}' ],
        Method::fromArray([ 'body' => '$this->abc();', 'static' => NULL ])->buildCode()
    );

    // Si es abstract no puede ser final
    $runner->assertEquals(
        [ 'abstract public function () : void;' ],
        Method::fromArray([ 'abstract' => TRUE, 'final' => TRUE ])->buildCode()
    );
    $runner->assertEquals(
        [ 'final public function () : void', '{', '}' ],
        Method::fromArray([ 'abstract' => FALSE, 'body' => '', 'final' => TRUE ])->buildCode()
    );

    $values = [
        'bool'   => 'TRUE',
        'TRUE'   => 'TRUE',
        'FALSE'  => 'FALSE',
        'int'    => '0',
        'float'  => '0.0',
        'void'   => NULL,
        // 'static'       => $this->static ? 'return static;' : 'return $this;',
        'string' => "''",
    ];
    foreach ($values as $type => $expected)
    {
        $name = 'format' . ucfirst(strtolower($type));
        $runner->assertEquals(
            array_values(
                array_filter(
                    [
                        sprintf('public function %s() : %s', $name, $type),
                        '{',
                        $expected === NULL ? NULL : sprintf('    return %s;', $expected),
                        '}'
                    ]
                )
            ),
            Method::fromArray([ 'body' => '', 'name' => $name, 'type' => $type ])->buildCode()
        );
    }
    $runner->assertEquals(
        [ 'public function x() : static', '{', '    return $this;', '}' ],
        Method::fromArray([ 'body' => '', 'name' => 'x', 'type' => 'static' ])->buildCode()
    );
    $runner->assertEquals(
        [ 'public static function y() : static', '{', '    return static;', '}' ],
        Method::fromArray([ 'body' => '', 'name' => 'y', 'static' => TRUE, 'type' => 'static' ])->buildCode()
    );
    $runner->assertEquals(
        [ 'public function z() : NULL', '{', '    return NULL;', '}' ],
        Method::fromArray([ 'body' => '', 'name' => 'z', 'null' => 'NULL' ])->buildCode()
    );

    $runner->assertEquals(
        [ 'public function abc(string $a, string $b, string $c) : ?string', '{', '    return NULL;', '}' ],
        Method::fromArray(
            [
                'body'   => '',
                'name'   => 'abc',
                'null'   => 'NULL',
                'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ],
                'type'   => 'string'
            ]
        )
            ->buildCode()
    );

    //------------------------------------------------------------------------------
    // Method::renderTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Method::new()->renderTags());
    $runner->assertEquals(
        [ '@inheritdoc' ],
        Method::fromArray([ 'override' => TRUE, 'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ] ])
            ->renderTags()
    );
    $runner->assertEquals(
        [ '@inheritdoc', '', '@throws Abc' ],
        Method::fromArray([ 'override' => TRUE, 'tags' => [ [ 'name' => 'throws', 'value' => 'Abc' ] ] ])->renderTags()
    );
    $runner->assertEquals(
        [ '@see A::a()' ],
        Method::fromArray([ 'copydoc' => 'A::a()', 'name' => 'c' ])->renderTags()
    );
    $runner->assertEquals(
        [ '@throws C', '', '@see A::a()' ],
        Method::fromArray([ 'copydoc' => 'A::a()', 'name' => 'c', 'tags' => [ [ 'name' => 'throws', 'value' => 'C' ] ] ])
            ->renderTags()
    );
    $runner->assertEquals(
        [ '@param string $a', '@param string $b', '@param string $c' ],
        Method::fromArray([ 'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ] ])->renderTags()
    );
    $runner->assertEquals(
        [ '@param string $a', '@param string $b', '@param string $c', '', '@throws RuntimeException' ],
        Method::fromArray(
            [
                'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ],
                'tags'   => [ [ 'name' => 'throws', 'value' => 'RuntimeException' ] ]
            ]
        )->renderTags()
    );

    //------------------------------------------------------------------------------
    // Method::setBody
    //------------------------------------------------------------------------------

    $runner->assertEquals(NULL, Method::new()->body);
    $runner->assertEquals(NULL, Method::new()->setBody(NULL)->body);
    $runner->assertEquals('', Method::new()->setBody('')->body);
    $runner->assertEquals('    a', Method::new()->setBody('a')->body);
    $runner->assertEquals(' a', Method::new()->setBody(' a')->body);
    $runner->assertEquals("    a\n    b", Method::new()->setBody([ 'a', 'b' ])->body);

    $sut = Method::fromArray([ 'body' => 'return new \DateTime();' ]);
    $runner->assertEquals('    return new DateTime();', $sut->body);
    $runner->assertArrayResult([ 'use DateTime;' ], $sut->renderUses());

    $sut = Method::fromArray([ 'body' => 'return new \DateTime@PhpDateTime();' ]);
    $runner->assertEquals('    return new PhpDateTime();', $sut->body);
    $runner->assertArrayResult([ 'use DateTime as PhpDateTime;' ], $sut->renderUses());

    //------------------------------------------------------------------------------
    // Method::setName
    //------------------------------------------------------------------------------

    foreach ([
                 Runner::getRandomMd5()        => '',
                 '_' . Runner::getRandomMd5()  => 'protected',
                 '__' . Runner::getRandomMd5() => ''
             ] as $name => $scope)
    {
        $property = Method::fromArray([ 'name' => $name ]);
        $runner->assertEquals($name, $property->name);
        $runner->assertEquals($scope, $property->scope);
    }
};