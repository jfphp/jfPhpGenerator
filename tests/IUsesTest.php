<?php

namespace jf\php\generator\tests;

use jf\php\generator\IUses;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IUses::class,
        [
            'methods' => [
                'getUses' => [
                    'type' => 'array',
                    'value' => '[]'
                ]
            ]
        ]
    );
};