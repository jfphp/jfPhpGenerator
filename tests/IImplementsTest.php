<?php

namespace jf\php\generator\tests;

use jf\php\generator\IImplements;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IImplements::class,
        [
            'methods' => [
                'addImplements'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IImplements::class',
                    'params' => [
                        'implements' => [
                            'type' => 'array|string'
                        ]
                    ]
                ],
                'buildImplements' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getImplements'   => [
                    'type'  => 'jf\php\generator\collection\Classnames',
                    'value' => "''"
                ]
            ]
        ]
    );
};