<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TDocTags;
use jf\php\generator\TTags;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TDocTagsTest extends ABase
    {
        use TDocTags;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TDocTags::class,
        [
            'traits'     => [ TTags::class ],
            'properties' => [
                'copydoc' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'    => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TDocTags::renderDoc
    //------------------------------------------------------------------------------

    $tags = TDocTagsTest::new();
    $runner->assertEquals([], $tags->renderDoc());
    $tags->addTags([ [ 'name' => 'override' ] ]);
    $runner->assertEquals([ '/**', ' * @override', ' */' ], $tags->renderDoc());
    $tags->addTags([ [ 'name' => 'throws', 'value' => 'Abc' ] ]);
    $runner->assertEquals([ '/**', ' * @override', ' * @throws Abc', ' */' ], $tags->renderDoc());
};