<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\Classname;
use jf\php\generator\collection\Classnames;
use jf\php\generator\TImplements;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TImplementsTest extends ABase
    {
        use TImplements;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TImplements::class,
        [
            'properties' => [
                'implements' => Classnames::class,
                'name'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TImplements::addImplements
    //------------------------------------------------------------------------------

    $runner->assertInstanceOf(TImplementsTest::class, TImplementsTest::new()->addImplements([]));
    $sut = new Classname();
    $runner->assertEquals([ $sut ], TImplementsTest::new()->addImplements([ $sut ])->implements->values());

    //------------------------------------------------------------------------------
    // TImplements::buildImplements
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TImplementsTest::new()->addImplements([])->buildImplements());
    $runner->assertEquals([ 'A\B' ], TImplementsTest::new()->addImplements('A\B')->buildImplements());
    $runner->assertEquals([ 'C' ], TImplementsTest::new()->addImplements('\A\B\C')->buildImplements());

    $runner->assertArrayResult(
        [ 'A', 'A\B', 'Ab', 'C' ],
        TImplementsTest::new()->addImplements([ [ 'name' => 'Ab' ], '\A', 'A\B', '\A\B' => 'C' ])->buildImplements()
    );

    //------------------------------------------------------------------------------
    // TImplements::renderImplements
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TImplementsTest::new()->renderImplements());
    $runner->assertEquals('implements A\B', TImplementsTest::new()->addImplements('A\B')->renderImplements());
    $runner->assertEquals('implements C', TImplementsTest::new()->addImplements('\A\B\C')->renderImplements());

    $runner->assertEquals(
        'implements A, A\B, Ab, C',
        TImplementsTest::new()->addImplements([ [ 'name' => 'Ab' ], '\A', 'A\B', '\A\B' => 'C' ])->renderImplements()
    );
};