<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TDescription;
use jf\php\generator\TDocDescription;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TDocDescriptionTest extends ABase
    {
        use TDocDescription;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TDocDescription::class,
        [
            'traits'     => [
                TDescription::class
            ],
            'properties' => [
                'description' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TDocDescription::addDocDescription
    //------------------------------------------------------------------------------

    $tags = [];
    $runner->assertEquals(NULL, TDocDescriptionTest::new()->addDocDescription($tags));
    $runner->assertEquals([], $tags);
    $runner->assertEquals(
        'Lorem ipsum.',
        (string) TDocDescriptionTest::fromArray([ 'description' => 'lorem ipsum' ])->addDocDescription($tags)
    );
    $runner->assertEquals($tags['description'], TDocDescriptionTest::new()->addDocDescription($tags));
    $runner->assertEquals(
        'Abcdefg hijklmno pqrstu vwxyz.',
        (string) TDocDescriptionTest::fromArray([ 'description' => 'abcdefg hijklmno pqrstu vwxyz  ' ])->addDocDescription($tags)
    );
};