<?php

namespace jf\php\generator\tests;

use jf\log\Logger;
use jf\log\writer\Output;
use jf\php\generator\ABase;
use jf\php\generator\Exception;
use jf\php\generator\TFile;
use jf\php\generator\TTranslations;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TFileTest extends ABase
    {
        use TFile;
    }

    $runner->testClassDefinition(
        TFile::class,
        [
            'properties' => [
                'content'      => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'debug'        => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => TRUE
                ],
                'filename'     => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'footer'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'header'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'logger'       => 'Psr\Log\LoggerInterface',
                'overwrite'    => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ],
            'traits'     => [ TTranslations::class ]
        ]
    );

    function calcSprintfLen(string $text) : int
    {
        return 3 * (mb_strlen($text) + 5); // 3 líneas con 2 espacios, 2 rayas verticales y 1 retorno de carro cada una
    }

    //------------------------------------------------------------------------------
    // TTFileTest::clear
    //------------------------------------------------------------------------------

    $content = [ 'n' => random_bytes(16) ];
    $sut     = TFileTest::fromArray([ 'content' => $content ]);
    $runner->assertEquals($content, $sut->content);
    $runner->assertEquals([], $sut->clear()->content);

    //------------------------------------------------------------------------------
    // TFileTest::rect
    //------------------------------------------------------------------------------

    $runner->assertEquals(calcSprintfLen(__FILE__), mb_strlen(TFileTest::rect(__FILE__)));

    //------------------------------------------------------------------------------
    // TFileTest::getBasename
    //------------------------------------------------------------------------------

    $runner->assertEquals(basename(__FILE__), TFileTest::fromArray([ 'filename' => __FILE__ ])->getBasename());

    //------------------------------------------------------------------------------
    // TFileTest::getDirname
    //------------------------------------------------------------------------------

    $runner->assertEquals(dirname(__FILE__), TFileTest::fromArray([ 'filename' => __FILE__ ])->getDirname());

    //------------------------------------------------------------------------------
    // TFileTest::relativePath
    //------------------------------------------------------------------------------

    $runner->assertEquals('../src/TFile.php', TFileTest::relativePath(__FILE__, __DIR__ . '/../src/TFile.php'));
    $runner->assertEquals('../src/TFile.php', TFileTest::relativePath(__FILE__, dirname(__DIR__) . '/src/TFile.php'));
    for ($i = 1; $i < 5; ++$i)
    {
        $runner->assertEquals(implode('/', array_fill(0, $i, '..')), TFileTest::relativePath(__DIR__, dirname(__DIR__, $i)));
    }

    //------------------------------------------------------------------------------
    // TFileTest::save
    //------------------------------------------------------------------------------

    ob_start();
    $footer = bin2hex(random_bytes(16));
    $header = bin2hex(random_bytes(16));
    $runner->assertEquals('', TFileTest::fromArray([ 'header' => '', 'footer' => '' ])->save());
    $sut = TFileTest::fromArray([ 'footer' => $footer, 'header' => $header, 'overwrite' => TRUE ]);
    $runner->assertEquals($header . $footer, $sut->save());
    $runner->assertEquals('', ob_get_clean()); // Sin logger no debe salir nada por pantalla

    ob_start();
    $logger         = new Logger('TFile', new Output());
    $logger->format = '{message}';
    $sut            = TFileTest::fromArray([ 'footer' => $footer, 'header' => $header, 'logger' => $logger, 'overwrite' => TRUE ]);
    $runner->assertEquals($header . $footer, $sut->save());
    $content = explode(PHP_EOL, ob_get_clean());
    $runner->assertEquals('Código generado: 64 bytes', $content[0]);
    $runner->assertEquals('│ php://stdout │', $content[2]);
    $runner->assertEquals($header . $footer, $content[4]);

    ob_start();
    $sut->logger = NULL;
    $sut->debug  = FALSE;
    $sut->setFilename('php://output');
    $actual = $sut->save();
    ob_end_clean();
    $runner->assertEquals($header . $footer, $actual);

    ob_start();
    $filename      = tempnam(sys_get_temp_dir(), 'test-');
    $sut->filename = $filename;
    $actual        = $sut->save();
    ob_end_clean();

    $runner->assertEquals($header . $footer, $actual);
    $content = file_get_contents($filename);
    unlink($filename);
    $runner->assertEquals($header . $footer, $content);
    $content = file_get_contents(__FILE__);
    file_put_contents($filename, $content);

    ob_start();
    $sut->overwrite = FALSE;
    $actual         = $sut->save();
    ob_end_clean();

    $runner->assertEquals($header . $footer, $actual);
    $runner->assertEquals($content, file_get_contents($filename));
    unlink($filename);

    $runner->assertException(Exception::class, fn() => $sut->setFilename('/root/abc')->save());
    $runner->assertException(Exception::class, fn() => $sut->setFilename('/root/abc/d')->save());

    TFileTest::fromArray([ 'footer' => $footer, 'header' => $header, 'overwrite' => TRUE ]);

    //------------------------------------------------------------------------------
    // TFileTest::setContent
    //------------------------------------------------------------------------------

    $runner->assertArrayResult([ 'a', 'b', 'c' ], TFileTest::fromArray([ 'content' => [ 'a', 'b', 'c' ] ])->content);
    $runner->assertArrayResult([ 'a', 'b', 'c' ], TFileTest::fromArray([ 'content' => "a\nb\nc" ])->content);

    //------------------------------------------------------------------------------
    // TFileTest::setFilename + TFileTest::clear
    //------------------------------------------------------------------------------

    $runner->assertInstanceOf(TFileTest::class, TFileTest::new()->setFilename(''));
    $runner->assertEquals(__FILE__, TFileTest::fromArray([ 'filename' => __FILE__ ])->filename);
    $runner->assertArrayResult([ file_get_contents(__FILE__) ], TFileTest::fromArray([ 'filename' => __FILE__ ])->content);
    $runner->assertEquals([], TFileTest::fromArray([ 'filename' => __FILE__ ])->clear()->content);

    //------------------------------------------------------------------------------
    // TFileTest::splitPath
    //------------------------------------------------------------------------------

    $runner->assertEquals([ 'a', 'b', 'c' ], TFileTest::splitPath('////a///b//c/'));

    //------------------------------------------------------------------------------
    // TFileTest::__toString
    //------------------------------------------------------------------------------

    $sut = TFileTest::new();
    $runner->assertEquals('', (string) $sut);
    $content = file_get_contents(__FILE__);
    $sut->setContent($content);
    $runner->assertEquals($content, (string) $sut);
};