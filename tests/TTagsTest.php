<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\Tag;
use jf\php\generator\Tags;
use jf\php\generator\TTags;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TTagsTest extends ABase
    {
        use TTags;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TTags::class,
        [
            'properties' => [
                'copydoc' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'    => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    $desc1     = bin2hex(random_bytes(16));
    $desc2     = bin2hex(random_bytes(16));
    $name1     = bin2hex(random_bytes(16));
    $name2     = bin2hex(random_bytes(16));
    $var1      = bin2hex(random_bytes(16));
    $var2      = bin2hex(random_bytes(16));
    $tags      = [
        [
            'name'        => $name1,
            'description' => $desc1
        ],
        [
            [ 'name' => $name2, 'variable' => $var1 ],
            [ 'name' => $name2, 'variable' => $var2 ],
        ],
        [
            'name'        => $name1,
            'description' => $desc2
        ]
    ];
    $tags1     = [ NULL, NULL, NULL ];
    $tags2     = [
        [
            [ 'name' => $name2, 'variable' => $var1 ],
            [ 'name' => $name2, 'variable' => $var2 ],
        ],
        [
            'name'        => $name1,
            'description' => $desc1
        ],
        [
            [ 'name' => $name1, 'variable' => $var1 ],
            [ 'name' => $name1, 'variable' => $var1 ],
        ],
        [
            [ 'name' => $name2, 'variable' => $var2 ],
            [ 'name' => $name2, 'variable' => $var2 ],
        ],
        [
            'name'        => $name2,
            'description' => $desc2
        ],
        NULL,
        [
            'name'        => $name1,
            'description' => $desc1
        ]
    ];
    $tags3     = [
        [
            'name'        => $name1,
            'description' => $desc1
        ],
        [
            'name'        => $name2,
            'description' => $desc2
        ],
        [
            'name'        => $name1,
            'description' => $desc1
        ]
    ];
    $tags4     = [
        [
            [ 'name' => $name2, 'variable' => $var1 ],
            [ 'name' => $name2, 'variable' => $var2 ],
        ],
        [
            [ 'name' => $name1, 'variable' => $var1 ],
            [ 'name' => $name1, 'variable' => $var1 ],
        ],
        [
            [ 'name' => $name2, 'variable' => $var2 ],
            [ 'name' => $name2, 'variable' => $var2 ],
        ]
    ];
    $expected  = [
        '@' . $name1 . ' ' . ucfirst($desc1) . '.',
        '',
        '@' . $name2 . ' $' . $var1,
        '@' . $name2 . ' $' . $var2,
        '',
        '@' . $name1 . ' ' . ucfirst($desc2) . '.'
    ];
    $expected1 = [ '', '', '' ];
    $expected2 = [
        '@' . $name2 . ' $' . $var1,
        '@' . $name2 . ' $' . $var2,
        '',
        '@' . $name1 . ' ' . ucfirst($desc1) . '.',
        '',
        '@' . $name1 . ' $' . $var1,
        '@' . $name1 . ' $' . $var1,
        '',
        '@' . $name2 . ' $' . $var2,
        '@' . $name2 . ' $' . $var2,
        '',
        '@' . $name2 . ' ' . ucfirst($desc2) . '.',
        '',
        '@' . $name1 . ' ' . ucfirst($desc1) . '.',
    ];
    $expected3 = [
        '@' . $name1 . ' ' . ucfirst($desc1) . '.',
        '@' . $name2 . ' ' . ucfirst($desc2) . '.',
        '@' . $name1 . ' ' . ucfirst($desc1) . '.',
    ];
    $expected4 = [
        '@' . $name2 . ' $' . $var1,
        '@' . $name2 . ' $' . $var2,
        '',
        '@' . $name1 . ' $' . $var1,
        '@' . $name1 . ' $' . $var1,
        '',
        '@' . $name2 . ' $' . $var2,
        '@' . $name2 . ' $' . $var2,
    ];

    //------------------------------------------------------------------------------
    // TTags::addTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TTagsTest::fromArray([ 'tags' => [] ])->tags);
    $tag = new Tag([ 'name' => 'test' ]);
    $runner->assertEquals([ $tag ], TTagsTest::fromArray([ 'tags' => [ $tag ] ])->tags);
    $test = TTagsTest::fromArray([ 'tags' => [ [ 'name' => $name1, 'variable' => $var1 ] ] ]);
    $runner->assertEquals(1, count($test->tags));
    $runner->assertEquals(sprintf('@%s $%s', $name1, $var1), (string) $test->tags[0]);
    $test = TTagsTest::fromArray([ 'tags' => $tags ]);
    $runner->assertEquals(3, count($test->tags));
    $runner->assertTrue($test->tags[0] instanceof Tag, 'Se esperaba una instancia de ' . Tag::class);
    $runner->assertTrue($test->tags[1] instanceof Tags, 'Se esperaba una instancia de ' . Tags::class);
    $runner->assertTrue($test->tags[2] instanceof Tag, 'Se esperaba una instancia de ' . Tag::class);

    //------------------------------------------------------------------------------
    // TTags::renderTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TTagsTest::new()->renderTags());
    $runner->assertArrayResult($expected, TTagsTest::fromArray([ 'tags' => $tags ])->renderTags());
    $runner->assertArrayResult($expected1, TTagsTest::fromArray([ 'tags' => $tags1 ])->renderTags());
    $runner->assertArrayResult($expected2, TTagsTest::fromArray([ 'tags' => $tags2 ])->renderTags());
    $runner->assertArrayResult($expected3, TTagsTest::fromArray([ 'tags' => $tags3 ])->renderTags());
    $runner->assertArrayResult($expected4, TTagsTest::fromArray([ 'tags' => $tags4 ])->renderTags());
    $runner->assertArrayResult(
        $expected,
        TTagsTest::fromArray(
            [
                'tags' => [
                    Tag::fromArray($tags[0]),
                    Tags::fromArray([ 'tags' => $tags[1] ]),
                    Tag::fromArray($tags[2])
                ]
            ]
        )->renderTags()
    );
    // copydoc
    $runner->assertArrayResult([ '@see ' . $desc1 ], TTagsTest::fromArray([ 'copydoc' => $desc1 ])->renderTags());
    $runner->assertArrayResult(
        [ ...$expected, '', '@see ' . $desc2 ],
        TTagsTest::fromArray([ 'copydoc' => $desc2, 'tags' => $tags ])->renderTags()
    );
    // Ejemplo con PHPDoc
    $runner->assertEquals(
        <<<'DOC'
@method assertEquals

@param array  $expected Valor esperado.
@param array  $actual   Valor a comparar.
@param string $message  Mensaje a mostrar si no se cumple la condición.
@param mixed  ...$args  Valores a usar para formatear el mensaje.

@return bool Resultado de la comparación.

@throws RuntimeException
DOC,
        implode(
            PHP_EOL,
            TTagsTest::fromArray(
                yaml_parse(
                    <<<'YAML'
tags:
    -   name: method
        value: assertEquals
    -
        -   description : Valor esperado
            name        : param
            value       : array
            variable    : expected
        -   description : Valor a comparar
            name        : param
            value       : array
            variable    : actual
        -   description : Mensaje a mostrar si no se cumple la condición
            name        : param
            value       : string
            variable    : message
        -   description : Valores a usar para formatear el mensaje
            name        : param
            value       : mixed
            variable    : ...$args
    -   description : Resultado de la comparación
        name        : return
        value       : bool
    - ~
    -   name        : throws
        value       : RuntimeException
YAML

                )
            )->renderTags()
        )
    );
};