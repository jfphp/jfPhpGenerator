<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\Classname;
use jf\php\generator\collection\Classnames;
use jf\php\generator\TExtends;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TExtendsTest extends ABase
    {
        use TExtends;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TExtends::class,
        [
            'properties' => [
                'extends' => [
                    'type'  => Classnames::class,
                    'value' => NULL
                ],
                'name'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TExtends::addExtends
    //------------------------------------------------------------------------------

    $runner->assertInstanceOf(TExtendsTest::class, TExtendsTest::new()->addExtends([]));

    $sut = new Classname();
    $runner->assertEquals([ $sut ], TExtendsTest::new()->addExtends([ $sut ])->extends->values());

    //------------------------------------------------------------------------------
    // TExtends::buildExtends
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TExtendsTest::new()->addExtends([])->buildExtends());
    $runner->assertEquals([ 'A\B' ], TExtendsTest::new()->addExtends('A\B')->buildExtends());
    $runner->assertEquals([ 'C' ], TExtendsTest::new()->addExtends('\A\B\C')->buildExtends());

    $runner->assertArrayResult(
        [ 'A', 'A\B', 'Ab', 'C', 'D', 'E' ],
        TExtendsTest::new()->addExtends([ [ 'name' => 'Ab' ], '\A', 'A\B', '\A\B' => 'C', '\D' => NULL, '\E' => '' ])->buildExtends()
    );

    //------------------------------------------------------------------------------
    // TExtends::renderExtends
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TExtendsTest::new()->renderExtends());
    $runner->assertEquals('extends A\B', TExtendsTest::new()->addExtends('A\B')->renderExtends());
    $runner->assertEquals('extends C', TExtendsTest::new()->addExtends('\A\B\C')->renderExtends());

    $runner->assertEquals(
        'extends A, A\B, Ab, C',
        TExtendsTest::new()->addExtends([ [ 'name' => 'Ab' ], '\A', 'A\B', '\A\B' => 'C' ])->renderExtends()
    );
};
