<?php

namespace jf\php\generator\tests;

use jf\php\generator\Exception;
use jf\php\generator\Exception as jfException;
use jf\php\generator\Helpers;
use jf\tests\Runner;

return function (Runner $runner)
{
    class FakeHelpers
    {
        /**
         * Método fake para comprobar algunas utilidades de la clase Helpers.
         * Línea de descripción adicional.
         *
         * @param string $p1 Parámetro 1.
         *                   Otra línea de comentario adicional.
         * @param string $p2 Otro parámetro.
         *
         * @return void
         *
         * @throws jfException
         * @throws jf\php\generator\Exception
         * @throws \ReflectionException
         *
         * @noinspection PhpDocRedundantThrowsInspection
         * @noinspection PhpDocSignatureInspection
         * @noinspection PhpFullyQualifiedNameUsageInspection
         * @noinspection PhpUndefinedClassInspection
         * @noinspection PhpUndefinedNamespaceInspection
         */
        public function fake() : void
        {
        }
    }

    $runner->testClassDefinition(
        Helpers::class,
        [
            'properties' => [
                'forks' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ]
            ]
        ]
    );

    $comment = (new \ReflectionClass(FakeHelpers::class))->getMethod('fake')->getDocComment();

    //------------------------------------------------------------------------------
    // Helpers::extractMethodThrows
    //------------------------------------------------------------------------------
    $runner->assertException(Exception::class, fn() => Helpers::extractMethodThrows('', ''));
    $runner->assertException(Exception::class, fn() => Helpers::extractMethodThrows('Abc', ''));
    $runner->assertException(Exception::class, fn() => Helpers::extractMethodThrows('Abc', 'method'));
    $runner->assertException(Exception::class, fn() => Helpers::extractMethodThrows(Helpers::class, ''));
    $runner->assertException(Exception::class, fn() => Helpers::extractMethodThrows(Helpers::class, 'unknown'));
    $runner->assertArrayResult(
        [
            [
                'name'  => 'throws',
                'value' => '\ReflectionException'
            ],
            [
                'name'  => 'throws',
                'value' => '\\' . Exception::class
            ]
        ],
        Helpers::extractMethodThrows(FakeHelpers::class, 'fake')
    );

    //------------------------------------------------------------------------------
    // Helpers::getCommentTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Helpers::getCommentTags(''));
    $runner->assertArrayResult(
        [
            '@description'  => [
                'Método fake para comprobar algunas utilidades de la clase Helpers.',
                'Línea de descripción adicional.'
            ],
            '@noinspection' => [
                'PhpDocRedundantThrowsInspection',
                'PhpDocSignatureInspection',
                'PhpFullyQualifiedNameUsageInspection',
                'PhpUndefinedClassInspection',
                'PhpUndefinedNamespaceInspection'
            ],
            '@param'        => [
                "string \$p1 Parámetro 1.\nOtra línea de comentario adicional.",
                'string $p2 Otro parámetro.'
            ],
            '@return'       => [ 'void' ],
            '@throws'       => [ 'jfException', Exception::class, '\ReflectionException' ]
        ],
        Helpers::getCommentTags($comment)
    );

    //------------------------------------------------------------------------------
    // Helpers::resolve
    //------------------------------------------------------------------------------

    $runner->assertEquals('\\' . Helpers::class, Helpers::resolve(__FILE__, 'Helpers'));
    $runner->assertEquals('\\' . Runner::class, Helpers::resolve(__FILE__, 'Runner'));
};