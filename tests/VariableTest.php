<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\IGenerator;
use jf\php\generator\IUses;
use jf\php\generator\TDocDescription;
use jf\php\generator\TDocTags;
use jf\php\generator\TDocType;
use jf\php\generator\Variable;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Variable::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IGenerator::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TDocDescription::class,
                TDocTags::class,
                TDocType::class
            ],
            'properties' => [
                'copydoc'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'           => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'name'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'prefix'           => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => '$'
                ],
                'tags'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Variable::__toString
    // Este método delega en buildCode la mayor parte del trabajo.
    //------------------------------------------------------------------------------

    $runner->assertEquals('', (string) Variable::new());
    $runner->assertEquals('$abc;', (string) Variable::fromArray([ 'name' => 'abc' ]));
    $runner->assertEquals([ '@var int' ], Variable::fromArray([ 'name' => 'abc', 'value' => 100 ])->renderTags());
    $runner->assertEquals(
        "/**\n * @var int\n */\n\$abc = 100;",
        (string) Variable::fromArray([ 'name' => 'abc', 'value' => 100 ])
    );
    $runner->assertEquals(
        <<<'DOC'
/**
 * Lorem ipsum dolor sit amet.
 *
 * @var int
 *
 * @deprecated
 */
$abc = 100;
DOC,
        (string) Variable::fromArray(
            [
                'description' => 'lorem ipsum dolor sit amet',
                'name'        => 'abc',
                'value'       => 100,
                'tags'        => [ [ 'name' => 'deprecated' ] ]
            ]
        )
    );

    //------------------------------------------------------------------------------
    // Variable::buildCode
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Variable::new()->buildCode());
    $sut = new Variable([ 'name' => 'abc' ]);
    $runner->assertArrayResult([ '$abc' ], Variable::fromArray([ 'name' => 'abc' ])->buildCode());
    $runner->assertArrayResult([ '#abc' ], Variable::fromArray([ 'name' => 'abc', 'prefix' => '#' ])->buildCode());
    $runner->assertArrayResult([ '$xyz', '= 1.1' ], Variable::fromArray([ 'name' => 'xyz', 'value' => 1.1 ])->buildCode());
    $runner->assertArrayResult(
        [ '$xyz', '= FALSE' ],
        Variable::fromArray([ 'name' => 'xyz', 'value' => FALSE ])->buildCode()
    );
    $runner->assertArrayResult(
        [ '$xyz', "= 'test'" ],
        Variable::fromArray([ 'name' => 'xyz', 'value' => 'test' ])->buildCode()
    );
    $runner->assertArrayResult([ '$xyz', '= NULL' ], Variable::fromArray([ 'name' => 'xyz', 'value' => NULL ])->buildCode());
    $runner->assertArrayResult(
        [ '$xyz', '= null' ],
        Variable::fromArray([ 'name' => 'xyz', 'null' => 'null', 'value' => NULL ])->buildCode()
    );
    $runner->assertArrayResult(
        [ '$xyz', '= FALSE' ],
        Variable::fromArray([ 'name' => 'xyz', 'null' => 'NULL', 'value' => FALSE ])->buildCode()
    );

    //------------------------------------------------------------------------------
    // Variable::renderTags
    //------------------------------------------------------------------------------

    $text = Runner::getRandomMd5();
    $runner->assertEquals([], Variable::new()->renderTags());
    $runner->assertEquals([ "@see $text::\$var" ], Variable::fromArray([ 'copydoc' => "$text::\$var" ])->renderTags());
    $runner->assertEquals(
        [ ucfirst($text) . '.', '', '@var int' ],
        Variable::fromArray([ 'description' => $text, 'type' => 'int' ])->renderTags()
    );
    $runner->assertEquals(
        [ ucfirst($text) . '.', '', '@var int', '', '@deprecated' ],
        Variable::fromArray([ 'description' => $text, 'type' => 'int', 'tags' => [ [ 'name' => 'deprecated' ] ] ])->renderTags()
    );
};