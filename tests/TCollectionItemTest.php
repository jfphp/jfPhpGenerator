<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TCollectionItem;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TCollectionItemTest extends ABase
    {
        use TCollectionItem;

        public string $name = '';

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TCollectionItem::class,
        [
            'properties' => [
                'name' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TCollectionItem::getCollectionKey
    //------------------------------------------------------------------------------

    $name = random_bytes(16);
    $runner->assertEquals($name, TCollectionItemTest::fromArray([ 'name' => $name ])->getCollectionKey());
};