<?php

namespace jf\php\generator\tests;

use jf\php\generator\TModifiers;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        TModifiers::class,
        [
            'properties' => [
                'abstract' => 'bool',
                'const'    => 'bool',
                'final'    => 'bool',
                'override' => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'scope'    => [
                    'type'  => 'string',
                    'value' => ''
                ],
                'static'   => [
                    'type'  => 'bool',
                    'value' => FALSE
                ]
            ]
        ]
    );
};