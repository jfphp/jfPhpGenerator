<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Methods;
use jf\php\generator\collection\Properties;
use jf\php\generator\collection\TraitItems;
use jf\php\generator\IAttributes;
use jf\php\generator\IGenerator;
use jf\php\generator\IMethods;
use jf\php\generator\IProperties;
use jf\php\generator\ITraits;
use jf\php\generator\IUses;
use jf\php\generator\TAttributes;
use jf\php\generator\TGenerator;
use jf\php\generator\TMethods;
use jf\php\generator\TProperties;
use jf\php\generator\TraitObject;
use jf\php\generator\TTraits;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        TraitObject::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IGenerator::class,
                IMethods::class,
                IProperties::class,
                ITraits::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TGenerator::class,
                TMethods::class,
                TProperties::class,
                TTraits::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'attributes'   => Attributes::class,
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'methods'      => Methods::class,
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'properties'   => Properties::class,
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'traits'       => TraitItems::class,
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TraitObject::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TraitObject::new()->__toString());
    $runner->assertEquals("trait Def\n{\n}", TraitObject::fromArray([ 'name' => 'Abc\Def' ])->__toString());

    //------------------------------------------------------------------------------
    // TraitObject::buildCode
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TraitObject::new()->buildCode());
    $runner->assertEquals([ 'trait Def', '{', '}' ], TraitObject::fromArray([ 'name' => 'Abc\Def' ])->buildCode());

    //------------------------------------------------------------------------------
    // Ejemplo completo
    //------------------------------------------------------------------------------

    $sut = TraitObject::fromArray(
        [
            'description' => 'lorem trait ipsum description',
            'name'        => 'Abc\Def',
            'attributes'  => [
                [
                    'name'      => 'MyAttribute',
                    'arguments' => [ 'a' => 1 ]
                ]
            ],
            'methods'     => [
                'lorem' => [
                    'description' => 'lorem ipsum dolor sit amet',
                    'body'        => ''
                ]
            ],
            'properties'  => [
                'name'  => 'lorem ipsum',
                'value' => [
                    'description' => 'lorem ipsum',
                    'value'       => 1.1
                ]
            ],
            'traits'      => [
                '\XYZ\TUses',
                'TTraits'
            ]
        ]
    );
    $runner->assertEquals(
        <<<'PHP'
/**
 * Lorem trait ipsum description.
 */
#[MyAttribute(a: 1)]
trait Def
{
    use TTraits;
    use TUses;

    /**
     * Lorem ipsum.
     *
     * @var string
     */
    public string $name = '';

    /**
     * Lorem ipsum.
     *
     * @var float
     */
    public float $value = 1.1;

    /**
     * Lorem ipsum dolor sit amet.
     */
    public function lorem() : void
    {
    }
}
PHP,
        (string) $sut
    );

    $runner->assertArrayResult([ 'XYZ\TUses' => '' ], $sut->getUses());
};
