<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\collection\Params;
use jf\php\generator\Param;
use jf\php\generator\TParams;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TParamsTest extends ABase
    {
        use TParams;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TParams::class,
        [
            'properties' => [
                'name'   => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'params' => Params::class
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TParams::addParams
    //------------------------------------------------------------------------------

    $int   = random_int(0, PHP_INT_MAX);
    $float = microtime(TRUE);
    $data  = [
        [ 'name' => 'a' ],
        [ 'name' => 'b', 'type' => 'string' ],
        [ 'name' => 'c', 'type' => 'int', 'value' => $int ],
        [ 'name' => 'd', 'type' => 'float', 'value' => $float, 'null' => 'NULL' ]
    ];
    $sut   = TParamsTest::new();

    $sut->getParams()->type = '';
    $sut->addParams($data);
    $runner->assertArrayCount(count($data), $sut->params);
    foreach ($sut->params as $param)
    {
        $runner->assertInstanceOf(Param::class, $param);
        $config = current($data);
        $runner->assertArrayResult((array) new Param($config), (array) $param);
        next($data);
    }

    //------------------------------------------------------------------------------
    // TParams::buildParams
    // Las pruebas de construcción de un parámetro se verifican en ParamTest.
    //------------------------------------------------------------------------------

    $runner->assertArrayResult(
        [
            '$a',
            'string $b',
            'int $c = ' . $int,
            '?float $d = ' . $float,
        ],
        $sut->buildParams()
    );
};