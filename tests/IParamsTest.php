<?php

namespace jf\php\generator\tests;

use jf\php\generator\IParams;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IParams::class,
        [
            'methods' => [
                'addParams'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IParams::class',
                    'params' => [
                        'params' => [
                            'type' => 'array|string'
                        ]
                    ]
                ],
                'buildParams' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getParams'   => [
                    'type'  => 'jf\php\generator\collection\Params',
                    'value' => "''"
                ]
            ]
        ]
    );
};