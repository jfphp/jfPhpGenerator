<?php

namespace jf\php\generator\tests;

use jf\php\generator\IAttributes;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IAttributes::class,
        [
            'methods' => [
                'addAttributes'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IAttributes::class',
                    'params' => [
                        'attributes' => [
                            'type' => 'array'
                        ]
                    ]
                ],
                'buildAttributes' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getAttributes'   => [
                    'type'  => 'jf\php\generator\collection\Attributes',
                    'value' => "''"
                ]
            ]
        ]
    );
};