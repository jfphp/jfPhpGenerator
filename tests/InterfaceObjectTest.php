<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Classnames;
use jf\php\generator\collection\Methods;
use jf\php\generator\collection\Properties;
use jf\php\generator\IAttributes;
use jf\php\generator\IExtends;
use jf\php\generator\IGenerator;
use jf\php\generator\IMethods;
use jf\php\generator\InterfaceObject;
use jf\php\generator\IProperties;
use jf\php\generator\IUses;
use jf\php\generator\TAttributes;
use jf\php\generator\TExtends;
use jf\php\generator\TGenerator;
use jf\php\generator\TMethods;
use jf\php\generator\TProperties;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        InterfaceObject::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IExtends::class,
                IGenerator::class,
                IMethods::class,
                IProperties::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TExtends::class,
                TGenerator::class,
                TMethods::class,
                TProperties::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'attributes'   => Attributes::class,
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extends'      => Classnames::class,
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'final'        => 'bool',
                'methods'      => Methods::class,
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'properties'   => Properties::class,
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // InterfaceObject::buildCode
    //------------------------------------------------------------------------------

    $runner->assertEquals([], InterfaceObject::new()->buildCode());
    $runner->assertEquals(
        [ 'interface LoremIpsum', '{', '}' ],
        InterfaceObject::fromArray([ 'name' => 'LoremIpsum' ])->buildCode()
    );
    $runner->assertEquals(
        [ 'final interface LoremIpsum2', '{', '}' ],
        InterfaceObject::fromArray([ 'name' => 'LoremIpsum2', 'final' => TRUE ])->buildCode()
    );
    $runner->assertEquals(
        [ 'interface Abc extends C, D', '{', '}' ],
        InterfaceObject::fromArray([ 'name' => 'Abc', 'extends' => [ 'C', 'D' ] ])->buildCode()
    );

    //------------------------------------------------------------------------------
    // InterfaceObject::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals(
        <<<'PHP'
/**
 * Interface description.
 */
interface LoremIpsum extends C, D, F
{
    /**
     * Lorem description.
     *
     * @var string
     */
    public const A = '';

    /**
     * Description 2.
     *
     * @var float
     */
    public const B = 1.23;

    /**
     * Description method 1.
     *
     * @return bool
     */
    public function meth1() : bool;
}
PHP,
        (string) InterfaceObject::fromArray(
            [
                'description' => 'Interface description',
                'name'        => 'LoremIpsum',
                'extends'     => [
                    '\A\B' => 'C',
                    'D',
                    '\E\F'
                ],
                'methods'     => [
                    'meth1' => [
                        'description' => 'description method 1',
                        'type'        => 'bool'
                    ]
                ],
                'properties'  => [
                    'a' => 'lorem description',
                    'b' => [
                        'description' => 'description 2',
                        'value'       => 1.23
                    ]
                ]
            ]
        )
    );
};