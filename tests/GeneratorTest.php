<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ClassObject;
use jf\php\generator\EnumObject;
use jf\php\generator\Exception;
use jf\php\generator\File;
use jf\php\generator\Generator;
use jf\php\generator\InterfaceObject;
use jf\php\generator\IUses;
use jf\php\generator\TraitObject;
use jf\php\generator\TUses;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;
use Throwable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Generator::class,
        [
            'extends'    => File::class,
            'implements' => [
                IAssign::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'properties' => [
                'content'      => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'debug'        => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => TRUE
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'filename'     => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'footer'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => PHP_EOL
                ],
                'header'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => "<?php\n\n"
                ],
                'logger'       => 'Psr\Log\LoggerInterface',
                'overwrite'    => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ],
            'traits'     => [
                TUses::class
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Generator::__toString
    //------------------------------------------------------------------------------

    $sut = Generator::new();
    $runner->assertEquals("<?php\n\n\n", (string) $sut);
    $class1 = $sut->addClass(new ClassObject([ 'name' => 'Abc\Def' ]));
    $class2 = $sut->addClass(new ClassObject([ 'name' => 'F\Gh' ]));
    $runner->assertException(Exception::class, fn() => (string) $sut);
    $class2->namespace = $class1->namespace = 'Xyz';
    $runner->assertEquals(
        <<<'PHP'
<?php

namespace Xyz;

class Def
{
}

class Gh
{
}

PHP,
        (string) $sut
    );

    //------------------------------------------------------------------------------
    // Generator::addClass
    //------------------------------------------------------------------------------

    $runner->assertEquals("class ClassAbc\n{\n}", (string) Generator::new()->addClass([ 'name' => 'ClassAbc' ]));
    $runner->assertEquals("class Def\n{\n}", (string) Generator::new()->addClass(new ClassObject([ 'name' => 'Def' ])));

    //------------------------------------------------------------------------------
    // Generator::addEnum
    //------------------------------------------------------------------------------

    $runner->assertEquals("enum EnumCde\n{\n}", (string) Generator::new()->addEnum([ 'name' => 'EnumCde' ]));
    $runner->assertEquals("enum Cde\n{\n}", (string) Generator::new()->addEnum(new EnumObject([ 'name' => 'Cde' ])));

    //------------------------------------------------------------------------------
    // Generator::addInterface
    //------------------------------------------------------------------------------

    $runner->assertEquals(
        "interface InterfaceOpq\n{\n}",
        (string) Generator::new()->addInterface([ 'name' => 'InterfaceOpq' ])
    );
    $runner->assertEquals(
        "interface Opq\n{\n}",
        (string) Generator::new()->addInterface(new InterfaceObject([ 'name' => 'Opq' ]))
    );

    //------------------------------------------------------------------------------
    // Generator::addTrait
    //------------------------------------------------------------------------------

    $runner->assertEquals("trait TraitXyz\n{\n}", (string) Generator::new()->addTrait([ 'name' => 'TraitXyz' ]));
    $runner->assertEquals("trait Xyz\n{\n}", (string) Generator::new()->addTrait(new TraitObject([ 'name' => 'Xyz' ])));

    //------------------------------------------------------------------------------
    // Generator::generate
    // La prueba más extensa e importante ya que permite verificar que el código
    // generado coincida con el almacenado en este repositorio.
    //------------------------------------------------------------------------------

    $root    = dirname(__DIR__);
    $started = ob_start();
    try
    {
        $config           = getGeneratorContent($root . '/generator.yaml');
        $config['logger'] = NULL;
        $files            = Generator::generate($root . '/src', $config);
        $started          = $started && !ob_end_clean();
        uksort(
            $files,
            function ($file1, $file2)
            {
                $name1 = basename($file1);
                $name2 = basename($file2);
                $dir1  = basename(dirname($file1));
                $dir2  = basename(dirname($file2));

                return match (TRUE)
                {
                    $dir1 === 'src' => $dir2 === 'src' ? strcasecmp($name1, $name2) : 1,
                    $dir2 === 'src' => -1,
                    default         => strcasecmp($dir1, $dir2)
                };
            }
        );
        foreach ($files as $file => $content)
        {
            printf("\tVerificando %s\n", basename($file));
            $started = ob_start();
            $runner->assertStringEquals(
                file_get_contents($file),
                $content,
                'Error al comparar el archivo %s',
                File::relativePath(__FILE__, $file)
            );
            $started = $started && !ob_end_clean();
        }
        // Verificamos una enumeración que no existe en el archivo generator.yaml
        $started = ob_start();
        $files   = Generator::generate('/', [ 'enumerations' => [ 'MyEnum' => [] ] ]);
        $started = $started && !ob_end_clean();
        $runner->assertArrayHasKey($files, '//MyEnum.php');
        $runner->assertStringEquals(
            <<<'PHP'
<?php

enum MyEnum
{
}
PHP,
            current($files)
        );
    }
    catch (Throwable $e)
    {
        if ($started)
        {
            ob_end_clean();
        }
        printf("ERROR: %s\n", $e->getMessage());
        echo $e->getTraceAsString() . PHP_EOL;
        die;
    }

    //------------------------------------------------------------------------------
    // Generator::getUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Generator::new()->getUses());
};