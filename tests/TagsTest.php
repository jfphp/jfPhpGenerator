<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\Tags;
use jf\php\generator\TTags;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Tags::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IToArray::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TTags::class
            ],
            'properties' => [
                'copydoc' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'  => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'tags'    => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Tags::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', (string) Tags::new());
    $runner->assertEquals('', (string) Tags::fromArray([ 'tags' => [ 'a', 'b' ] ]));
    $runner->assertEquals(
        "@a\n@b",
        (string) Tags::fromArray(
            [
                'tags' => [
                    [ 'name' => 'a' ],
                    [ 'name' => 'b' ]
                ]
            ]
        )
    );
    $runner->assertEquals(
        "@a   valA\n@bcd valBCD",
        (string) Tags::fromArray(
            [
                'tags' => [
                    [ 'name' => 'a', 'value' => 'valA' ],
                    [ 'name' => 'bcd', 'value' => 'valBCD' ]
                ]
            ]
        )
    );
    $runner->assertEquals(
        <<<'DOC'
@a   valA   $var1
@bcd valBCD $variable2
DOC,
        (string) Tags::fromArray(
            [
                'tags' => [
                    [ 'name' => 'a', 'value' => 'valA', 'variable' => 'var1' ],
                    [ 'name' => 'bcd', 'value' => 'valBCD', 'variable' => 'variable2' ]
                ]
            ]
        )
    );
    $runner->assertEquals(
        <<<'DOC'
@a   valA   $var1      Description 1.
@bcd valBCD $variable2 Description 2.
DOC,
        (string) Tags::fromArray(
            [
                'tags' => [
                    [ 'description' => 'description 1', 'name' => 'a', 'value' => 'valA', 'variable' => 'var1' ],
                    [
                        'description' => 'description 2',
                        'name'        => 'bcd',
                        'value'       => 'valBCD',
                        'variable'    => 'variable2'
                    ]
                ]
            ]
        )
    );
};
