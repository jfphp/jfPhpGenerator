<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\collection\TraitItems;
use jf\php\generator\IUses;
use jf\php\generator\TTraits;
use jf\tests\Runner;

include_once __DIR__ . '/../../autoload.php';

return function (Runner $runner)
{
    class TTraitsTest extends ABase implements IUses
    {
        use TTraits;

        public function __toString() : string
        {
            return self::class;
        }

        public function getUses() : array
        {
            $_traits = $this->traits;
            $_uses   = [];
            if ($_traits)
            {
                foreach ($_traits as $_trait)
                {
                    $_tuses = $_trait->getUses();
                    if ($_tuses)
                    {
                        $_uses = [ ...$_uses, ...$_tuses ];
                    }
                }
            }

            return $_uses;
        }
    }

    $runner->testClassDefinition(
        TTraits::class,
        [
            'properties' => [
                'name'   => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'traits' => TraitItems::class
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TTraits::addTraits
    //------------------------------------------------------------------------------

    $runner->assertEquals(NULL, TTraitsTest::fromArray([ 'traits' => '' ])->traits);

    $runner->assertArrayResult([], TTraitsTest::fromArray([ 'traits' => '' ])->getUses());
    $runner->assertArrayResult([], TTraitsTest::fromArray([ 'traits' => 'Abc' ])->getUses());
    $runner->assertArrayResult([], TTraitsTest::fromArray([ 'traits' => [ 'Abc' ] ])->getUses());

    $runner->assertArrayResult([ 'Abc' => '' ], TTraitsTest::fromArray([ 'traits' => '\Abc' ])->getUses());
    $runner->assertArrayResult([ 'Abc' => '' ], TTraitsTest::fromArray([ 'traits' => [ '\Abc' ] ])->getUses());

    $runner->assertArrayResult([ 'use Abc;' ], TTraitsTest::fromArray([ 'traits' => 'Abc' ])->buildTraits());
    $runner->assertArrayResult([ 'use Abc;' ], TTraitsTest::fromArray([ 'traits' => [ 'Abc' ] ])->buildTraits());
    $runner->assertArrayResult(
        [ 'use X;' ],
        TTraitsTest::fromArray([ 'traits' => [ [ 'alias' => 'X', 'name' => 'Abc' ] ] ])->buildTraits()
    );

    //------------------------------------------------------------------------------
    // TTraits::addTraits
    // TTraits::renderTraits
    //------------------------------------------------------------------------------

    $test = new TTraitsTest();
    $test->addTraits('A');
    $test->addTraits(
        [
            'B'      => 'C',
            '\c'     => '',
            '\C',
            '\D'     => 'E',
            'F\G'    => 'H',
            '\I\J'   => 'K',
            'L\M\N'  => 'O',
            '\P\Q\R' => 'S',
            '\T\U',
            'V\W',
            '\X\Y\Z'
        ]
    );
    $runner->assertArrayResult(
        [
            'use A;',
            'use C;',
            'use c;',
            'use E;',
            'use H;',
            'use K;',
            'use O;',
            'use S;',
            'use U;',
            'use V\W;',
            'use Z;'
        ],
        $test->buildTraits()
    );

    $runner->assertArrayResult(
        [
            'B'     => 'C',
            'c'     => '',
            'C'     => '',
            'D'     => 'E',
            'F\G'   => 'H',
            'I\J'   => 'K',
            'L\M\N' => 'O',
            'P\Q\R' => 'S',
            'T\U'   => '',
            'V\W'   => '',
            'X\Y\Z' => ''
        ],
        $test->getUses()
    );

    //------------------------------------------------------------------------------
    // TTraits::sortTraits
    //------------------------------------------------------------------------------

    $runner->assertArrayResult(
        [
            'use B;',
            'use D;',
            "use A\n{\n    A::name as private _name;\n}",
            "use C\n{\n    C::name as private _name;\n}"
        ],
        TTraitsTest::fromArray(
            [
                'traits' => [
                    'D',
                    [
                        'name'    => 'C',
                        'methods' => [
                            'name' => 'C1'
                        ]
                    ],
                    'B',
                    [
                        'name'    => 'A',
                        'methods' => [
                            'name' => 'A1'
                        ]
                    ]

                ]
            ]
        )->buildTraits()
    );
};