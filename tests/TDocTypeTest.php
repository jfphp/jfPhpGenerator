<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TDocType;
use jf\php\generator\TType;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TDocTypeTest extends ABase
    {
        use TDocType;

        public function __toString() : string
        {
            return self::class;
        }
    }

    $runner->testClassDefinition(
        TDocType::class,
        [
            'traits'     => [ TType::class ],
            'properties' => [
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TDocType::addDocType
    //------------------------------------------------------------------------------

    $tags = [];
    $runner->assertEquals(NULL, TDocTypeTest::new()->addDocType($tags));
    $runner->assertEquals([], $tags);
    $runner->assertEquals('@var bool', (string) TDocTypeTest::fromArray([ 'type' => 'bool' ])->addDocType($tags));
    $tag = TDocTypeTest::fromArray([ 'type' => 'int' ])->addDocType($tags);
    $runner->assertEquals('@var int', (string) $tag);
    $runner->assertEquals($tags['var'], $tag);

    //------------------------------------------------------------------------------
    // TDocType::getDocType
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TDocTypeTest::new()->getDocType());
    $runner->assertEquals('NULL', TDocTypeTest::fromArray([ 'null' => 'NULL' ])->getDocType());
    $runner->assertEquals('bool', TDocTypeTest::fromArray([ 'type' => 'bool' ])->getDocType());
    $runner->assertEquals('bool|NULL', TDocTypeTest::fromArray([ 'type' => '?bool' ])->getDocType());
    $runner->assertEquals('resource', TDocTypeTest::fromArray([ 'type' => 'resource' ])->getDocType());
    $runner->assertEquals('resource|string', TDocTypeTest::fromArray([ 'type' => 'string|resource' ])->getDocType());
    $runner->assertEquals('resource[]|string', TDocTypeTest::fromArray([ 'type' => 'string|resource[]' ])->getDocType());
    $runner->assertEquals('Abc&Def', TDocTypeTest::fromArray([ 'type' => 'Def&Abc' ])->getDocType());

    // Tipos compuestos y además verificamos el orden
    $types = [ 'Abc', 'array', 'bool', 'float', 'int', 'object', 'string', 'NULL' ];
    $type  = implode('|', $types);
    shuffle($types);
    $runner->assertEquals($type, TDocTypeTest::fromArray([ 'type' => $types ])->getDocType());
    $types = [ 'Abc', 'array', 'float', 'int', 'object', 'string', 'false', 'NULL' ];
    $type  = str_replace('|false|', '|FALSE|', implode('|', $types));
    shuffle($types);
    $runner->assertEquals($type, TDocTypeTest::fromArray([ 'type' => $types ])->getDocType());
    $types = [ 'Abc', 'array', 'float', 'int', 'object', 'string', 'true', 'NULL' ];
    $type  = str_replace('|true|', '|TRUE|', implode('|', $types));
    shuffle($types);
    $runner->assertEquals($type, TDocTypeTest::fromArray([ 'type' => $types ])->getDocType());

    // Tipos complejos
    $type = 'array{a:1,b:2,c:3}';
    $sut  = TDocTypeTest::fromArray([ 'type' => $type ]);
    $runner->assertEquals('array', $sut->getPhpType());
    $runner->assertEquals($type, $sut->getDocType());
};