<?php

namespace jf\php\generator\tests;

use jf\php\generator\IMethods;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IMethods::class,
        [
            'methods' => [
                'addMethods'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IMethods::class',
                    'params' => [
                        'methods' => [
                            'type' => 'array|string'
                        ]
                    ]
                ],
                'buildMethods' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getMethods'   => [
                    'type'  => 'jf\php\generator\collection\Methods',
                    'value' => "''"
                ]
            ]
        ]
    );
};