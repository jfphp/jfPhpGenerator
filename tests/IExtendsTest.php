<?php

namespace jf\php\generator\tests;

use jf\php\generator\IExtends;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IExtends::class,
        [
            'methods' => [
                'addExtends'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IExtends::class',
                    'params' => [
                        'extends' => [
                            'type' => 'array|string'
                        ]
                    ]
                ],
                'buildExtends' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getExtends'   => [
                    'type'  => 'jf\php\generator\collection\Classnames',
                    'value' => "''"
                ]
            ]
        ]
    );
};