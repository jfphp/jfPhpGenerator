<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TDocDescription;
use jf\php\generator\TDocTags;
use jf\php\generator\TGenerator;
use jf\php\generator\TName;
use jf\php\generator\TUses;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TGeneratorTest extends ABase
    {
        use TGenerator;

        public int   $calls    = 0;

        public mixed $callback = NULL;

        public function buildCode() : array
        {
            return [ ++$this->calls ];
        }

        public function callback() : mixed
        {
            return $this->callback;
        }
    }

    $runner->testClassDefinition(
        TGenerator::class,
        [
            'traits'     => [
                TDocDescription::class,
                TDocTags::class,
                TName::class,
                TUses::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TGenerator::__toString
    //------------------------------------------------------------------------------

    $sut = TGeneratorTest::new();
    $runner->assertEquals('1', $sut->__toString());
    $runner->assertEquals('2', $sut->__toString());
    $runner->assertEquals('3', $sut->__toString());

    //------------------------------------------------------------------------------
    // TGenerator::appendItems
    //------------------------------------------------------------------------------

    $data = [];
    $sut  = TGeneratorTest::new();
    $sut->appendItems($data, 'buildCode');
    $runner->assertEquals([ '1' ], $data);
    $sut->appendItems($data, 'buildCode');
    $runner->assertEquals([ '1', '2' ], $data);
    $sut->callback = [ 4, 5 ];
    $sut->appendItems($data, 'callback', '::');
    $runner->assertEquals([ '1', '2', '4::5' ], $data);
    $sut->callback = 'lorem';
    $sut->appendItems($data, 'callback');
    $runner->assertEquals([ '1', '2', '4::5', 'lorem' ], $data);

    //------------------------------------------------------------------------------
    // TGenerator::buildCode
    //------------------------------------------------------------------------------

    $sut = TGeneratorTest::new();
    $runner->assertEquals([ 1 ], $sut->buildCode());
    $runner->assertEquals([ 2 ], $sut->buildCode());
    $runner->assertEquals([ 3 ], $sut->buildCode());

    //------------------------------------------------------------------------------
    // TGenerator::getUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TGeneratorTest::new()->getUses());

    //------------------------------------------------------------------------------
    // TGenerator::renderTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TGeneratorTest::new()->renderTags());

    //------------------------------------------------------------------------------
    // TName::setName
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TGeneratorTest::fromArray([ 'name' => '' ])->getNameUses());
    $runner->assertEquals([ '\\' => 'Abc' ], TGeneratorTest::fromArray([ 'name' => 'Abc' ])->getNameUses());
    $runner->assertEquals([ 'Abc' => 'Def' ], TGeneratorTest::fromArray([ 'name' => 'Abc\Def' ])->getNameUses());
    $runner->assertEquals([ 'Gh' => 'Ijkl' ], TGeneratorTest::fromArray([ 'name' => '\Gh\Ijkl' ])->getNameUses());

    // Llamando a setName haciendo hecho setPackage
    $sut = TGeneratorTest::fromArray([ 'package' => 'a\b' ]);
    $sut->assign([ 'name' => '\Gh' ]);
    $runner->assertEquals('a\b\Gh', $sut->getFqcn());
};