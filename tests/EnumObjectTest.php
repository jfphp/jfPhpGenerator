<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Classnames;
use jf\php\generator\collection\Methods;
use jf\php\generator\collection\Properties;
use jf\php\generator\collection\TraitItems;
use jf\php\generator\EnumObject;
use jf\php\generator\Exception;
use jf\php\generator\IAttributes;
use jf\php\generator\IGenerator;
use jf\php\generator\IImplements;
use jf\php\generator\IMethods;
use jf\php\generator\IProperties;
use jf\php\generator\ITraits;
use jf\php\generator\IUses;
use jf\php\generator\TAttributes;
use jf\php\generator\TGenerator;
use jf\php\generator\TImplements;
use jf\php\generator\TMethods;
use jf\php\generator\TProperties;
use jf\php\generator\TTraits;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        EnumObject::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IGenerator::class,
                IImplements::class,
                IMethods::class,
                IProperties::class,
                IToArray::class,
                ITraits::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TGenerator::class,
                TImplements::class,
                TMethods::class,
                TProperties::class,
                TTraits::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'attributes'   => Attributes::class,
                'copydoc'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'implements'   => Classnames::class,
                'methods'      => Methods::class,
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'properties'   => Properties::class,
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tags'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'traits'       => TraitItems::class,
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // EnumObject::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', (string) EnumObject::new());
    $runner->assertEquals(
        <<<PHP
/**
 * Lorem ipsum description.
 */
enum Suit
{
    /**
     * Clubs description.
     */
    case Clubs;

    /**
     * Diamonds description.
     */
    case Diamonds;

    /**
     * Hearts description.
     */
    case Hearts;

    /**
     * Spades description.
     */
    case Spades;
}
PHP,
        (string) EnumObject::fromArray(
            [
                'description' => 'Lorem ipsum description',
                'name'        => 'Suit',
                'properties'  => [
                    [ 'description' => 'Clubs description', 'name' => 'Clubs' ],
                    [ 'description' => 'Diamonds description', 'name' => 'Diamonds' ],
                    [ 'description' => 'Hearts description', 'name' => 'Hearts' ],
                    [ 'description' => 'Spades description', 'name' => 'Spades' ]
                ]
            ]
        )
    );
    $runner->assertEquals(
        <<<PHP
/**
 * Lorem ipsum description.
 */
enum Suit : int implements Abc
{
    /**
     * Clubs description.
     */
    case Clubs = 1;

    /**
     * Diamonds description.
     */
    case Diamonds = 2;

    /**
     * Hearts description.
     */
    case Hearts = 30;

    /**
     * Spades description.
     */
    case Spades = 50;
}
PHP,
        (string) EnumObject::fromArray(
            [
                'description' => 'Lorem ipsum description',
                'name'        => 'Suit',
                'implements'  => 'Abc',
                'properties'  => [
                    [ 'description' => 'Clubs description', 'name' => 'Clubs', 'value' => 1 ],
                    [ 'description' => 'Diamonds description', 'name' => 'Diamonds', 'value' => 2 ],
                    [ 'description' => 'Hearts description', 'name' => 'Hearts', 'value' => 30 ],
                    [ 'description' => 'Spades description', 'name' => 'Spades', 'value' => 50 ]
                ]
            ]
        )
    );

    //------------------------------------------------------------------------------
    // EnumObject::buildCode
    //------------------------------------------------------------------------------

    $runner->assertEquals([], EnumObject::new()->buildCode());
    $runner->assertEquals([ 'enum Abc', '{', '}' ], EnumObject::fromArray([ 'name' => 'Abc' ])->buildCode());
    $runner->assertEquals(
        [ 'enum Cde : int', '{', '}' ],
        EnumObject::fromArray([ 'name' => 'Cde', 'type' => 'int' ])->buildCode()
    );
    $runner->assertEquals(
        [ 'enum Fg : string', '{', "    case Ipsum = 'i';\n    \n    case Lorem = 'l';", '}' ],
        EnumObject::fromArray(
            [
                'name'       => 'Fg',
                'properties' => [
                    [ 'name' => 'Lorem', 'value' => 'l' ],
                    [ 'name' => 'Ipsum', 'value' => 'i' ]
                ]
            ]
        )->buildCode()
    );

    //------------------------------------------------------------------------------
    // EnumObject::buildSetter
    //------------------------------------------------------------------------------

    $runner->assertException(
        new Exception('La ruta `abc.yaml` no es un archivo'),
        fn() => EnumObject::new()->buildSetter('abc', tpl: 'abc.yaml')
    );

    $runner->assertEquals(
        <<<'PHP'
/**
 * @param string $abc Valor a asignar.
 *
 * @return MyEnum
 */
public function setAbc(string $abc = '') : MyEnum
{
    return ($this->abc = MyEnum::tryFrom($abc));
}
PHP,
        (string) EnumObject::fromArray([ 'description' => 'con tpl', 'name' => 'MyEnum', 'type' => 'string' ])
            ->buildSetter(
                'abc',
                [],
                <<<'YAML'
body: return ($this->[[name]] = [[enum]]::tryFrom($[[name]]));
name: set[[Name]]
type: '[[enum]]'
params:
-   description: Valor a asignar
    name       : '[[name]]'
    value      : '[[value]]'
    type       : '[[type]]'
YAML
            )
    );

    $runner->assertEquals(
        <<<'PHP'
/**
 * Asigna el valor de la propiedad `country` (país de nacimiento).
 *
 * @param Country|string $country Valor a asignar.
 *
 * @return static
 */
public function setCountry(Country|string $country = '') : static
{
    $this->country = $country instanceof Country
        ? $country
        : Country::tryFrom($country);

    return $this;
}
PHP,
        (string) EnumObject::fromArray([ 'description' => 'País de nacimiento', 'name' => 'Country', 'type' => 'string' ])
            ->buildSetter(
                'country',
                [ 'params' => [ [ 'type' => 'Country|string', 'value' => '' ] ] ],
                __DIR__ . '/../tpl/enum-setter.yaml'
            )
    );

    $runner->assertEquals(
        <<<'PHP'
/**
 * Asigna el valor de la propiedad `enumName` (lorem ipsum).
 *
 * @param EnumName|string|NULL $enumName Valor a asignar.
 *
 * @return static
 */
public function setEnumName(EnumName|string|NULL $enumName = NULL) : static
{
    if ($enumName === NULL)
    {
        $this->enumName = NULL;
    }
    elseif ($enumName instanceof EnumName)
    {
        $this->enumName = $enumName;
    }
    else
    {
        $this->enumName = EnumName::tryFrom($enumName);
    }

    return $this;
}
PHP,
        (string) EnumObject::fromArray([ 'description' => 'lorem ipsum', 'name' => 'EnumName', 'type' => 'string' ])
            ->buildSetter()
    );

    //------------------------------------------------------------------------------
    // EnumObject::getType
    //------------------------------------------------------------------------------

    $runner->assertEquals('', EnumObject::new()->getType());
    $runner->assertEquals('string', EnumObject::fromArray([ 'type' => 'string' ])->getType());
    $runner->assertEquals(
        'int',
        EnumObject::fromArray([ 'properties' => [ [ 'value' => 1 ], [ 'value' => 2 ], ] ])->getType()
    );
    $runner->assertEquals(
        'string',
        EnumObject::fromArray([ 'properties' => [ [ 'value' => 'a' ], [ 'value' => 'b' ], ] ])->getType()
    );
    // No se pueden mezclar tipos
    $runner->assertException(
        Exception::class,
        fn() => EnumObject::fromArray(
            [ 'properties' => [ [ 'name' => 'a', 'value' => 'a' ], [ 'name' => 'b', 'value' => 1 ] ] ]
        )
            ->getType()
    );
};