<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\TName;
use jf\tests\Runner;

return function (Runner $runner)
{
    class TNameTest extends ABase
    {
        use TName;
    }

    $runner->testClassDefinition(
        TName::class,
        [
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TNameTest::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', (string) TNameTest::new());
    $runner->assertEquals('Ab', (string) TNameTest::fromArray([ 'name' => 'Ab' ]));
    $runner->assertEquals('A', (string) TNameTest::fromArray([ 'name' => '\A' ]));
    $runner->assertEquals('A\B', (string) TNameTest::fromArray([ 'name' => 'A\B' ]));
    $runner->assertEquals('B', (string) TNameTest::fromArray([ 'name' => '\A\B' ]));
    $runner->assertEquals('C', (string) TNameTest::fromArray([ 'alias' => 'C', 'name' => '\A\B' ]));

    //------------------------------------------------------------------------------
    // TNameTest::getFqcn
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TNameTest::new()->getFqcn());
    $runner->assertEquals('Ab', TNameTest::fromArray([ 'name' => 'Ab' ])->getFqcn());
    $runner->assertEquals('A\Ab', TNameTest::fromArray([ 'name' => 'A\Ab' ])->getFqcn());
    $runner->assertEquals('A\Ab', TNameTest::fromArray([ 'name' => '\A\Ab' ])->getFqcn());
    $runner->assertEquals('Abc\Xyz', TNameTest::fromArray([ 'namespace' => 'Abc', 'name' => 'Xyz' ])->getFqcn());
    $runner->assertEquals('Abc\Def\Xyz', TNameTest::fromArray([ 'namespace' => '\Abc\Def', 'name' => 'Xyz' ])->getFqcn());
    $runner->assertEquals(
        'Abc\Def\Xyz',
        TNameTest::fromArray([ 'namespace' => 'Def', 'name' => 'Xyz', 'package' => 'Abc' ])->getFqcn()
    );
    $runner->assertEquals(
        'Abc\Def\Hi\Xyz',
        TNameTest::fromArray([ 'namespace' => 'Def', 'name' => 'Xyz', 'package' => 'Abc', 'subnamespace' => 'Hi' ])->getFqcn()
    );
    $runner->assertEquals(
        'Abc\Def\Hi\Xyz',
        TNameTest::fromArray([ 'namespace' => 'Def', 'name' => 'Hi\Xyz', 'package' => 'Abc' ])->getFqcn()
    );

    //------------------------------------------------------------------------------
    // TNameTest::getNamespaceUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TNameTest::new()->getNamespaceUses());
    $runner->assertEquals([], TNameTest::fromArray([ 'name' => 'Ab' ])->getNamespaceUses());
    $runner->assertEquals([ 'Ab' => '' ], TNameTest::fromArray([ 'name' => '\Ab' ])->getNamespaceUses());
    $runner->assertEquals([ 'A' => '' ], TNameTest::fromArray([ 'name' => '\A' ])->getNamespaceUses());
    $runner->assertEquals([ 'A\B' => '' ], TNameTest::fromArray([ 'name' => 'A\B' ])->getNamespaceUses());
    $runner->assertEquals([ 'A\B' => '' ], TNameTest::fromArray([ 'name' => '\A\B' ])->getNamespaceUses());

    //------------------------------------------------------------------------------
    // TNameTest::getNameAndAliasUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TNameTest::new()->getNameAndAliasUses());
    $runner->assertEquals([ 'Ab' => '' ], TNameTest::fromArray([ 'name' => 'Ab' ])->getNameAndAliasUses());
    $runner->assertEquals([ 'Ab' => '' ], TNameTest::fromArray([ 'name' => '\Ab' ])->getNameAndAliasUses());
    $runner->assertEquals([ 'A' => '' ], TNameTest::fromArray([ 'name' => '\A' ])->getNameAndAliasUses());
    $runner->assertEquals([ 'A\B' => '' ], TNameTest::fromArray([ 'name' => 'A\B' ])->getNameAndAliasUses());
    $runner->assertEquals([ 'B' => '' ], TNameTest::fromArray([ 'name' => '\A\B' ])->getNameAndAliasUses());

    //------------------------------------------------------------------------------
    // TNameTest::getNameUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], TNameTest::new()->getNameUses());
    $runner->assertEquals([ '' => 'Ab' ], TNameTest::fromArray([ 'name' => 'Ab' ])->getNameUses());
    $runner->assertEquals([ '\\' => 'A' ], TNameTest::fromArray([ 'name' => '\A' ])->getNameUses());
    $runner->assertEquals([ 'A' => 'B' ], TNameTest::fromArray([ 'name' => 'A\B' ])->getNameUses());
    $runner->assertEquals([ 'A' => 'B' ], TNameTest::fromArray([ 'name' => '\A\B' ])->getNameUses());

    //------------------------------------------------------------------------------
    // TName::setName
    //------------------------------------------------------------------------------

    $common = [
        'alias'        => '',
        'name'         => '',
        'namespace'    => '',
        'package'      => '',
        'subnamespace' => ''
    ];
    $runner->assertEquals([ ...$common, 'name' => 'Abc' ], TNameTest::fromArray([ 'name' => 'Abc' ])->toArray());
    $runner->assertEquals([ ...$common, 'name' => 'Abc' ], TNameTest::fromArray([ 'name' => 'Abc', 'namespace' => '' ])->toArray());
    $runner->assertEquals(
        [ ...$common, 'name' => 'Def', 'subnamespace' => 'Abc' ],
        TNameTest::fromArray([ 'name' => 'Abc\Def' ])->toArray()
    );
    $runner->assertEquals(
        [ ...$common, 'name' => 'Ijkl', 'namespace' => 'Gh' ],
        TNameTest::fromArray([ 'name' => '\Gh\Ijkl' ])->toArray()
    );
};