<?php

namespace jf\php\generator\tests;

use jf\php\generator\ITraits;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        ITraits::class,
        [
            'methods' => [
                'addTraits'   => [
                    'type'   => 'static',
                    'params' => [
                        'traits' => 'array|string'
                    ],
                    'value'  => 'jf\php\generator\ITraits::class'
                ],
                'buildTraits' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getTraits'   => [
                    'type'  => 'jf\php\generator\collection\TraitItems',
                    'value' => "''"
                ]
            ]
        ]
    );
};