<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\File;
use jf\php\generator\TFile;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    /** @noinspection DuplicatedCode */
    $runner->testClassDefinition(
        File::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IToArray::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'properties' => [
                'content'      => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'debug'        => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => TRUE
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'filename'     => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'footer'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'header'       => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'logger'       => 'Psr\Log\LoggerInterface',
                'overwrite'    => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ],
            'traits'     => [
                TFile::class
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // File::fromFilename
    //------------------------------------------------------------------------------

    $runner->assertInstanceOf(File::class, File::fromFilename(''));
    $runner->assertEquals('', File::fromFilename('')->filename);
    $runner->assertEquals([ 'abc' ], File::fromFilename('', [ 'content' => [ 'abc' ] ])->content);
    $runner->assertEquals(__FILE__, File::fromFilename(__FILE__, [ 'content' => [ 'abc' ] ])->filename);
    $runner->assertEquals([ file_get_contents(__FILE__) ], File::fromFilename(__FILE__)->content);
};