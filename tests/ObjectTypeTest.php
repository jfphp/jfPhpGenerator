<?php

namespace jf\php\generator\tests;

use BackedEnum;
use DateTime;
use jf\php\generator\ClassObject;
use jf\php\generator\EnumObject;
use jf\php\generator\InterfaceObject;
use jf\php\generator\ObjectType;
use jf\php\generator\TraitObject;
use jf\tests\Runner;
use stdClass;
use UnitEnum;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        ObjectType::class,
        [
            'implements' => [
                BackedEnum::class,
                UnitEnum::class
            ],
            'properties' => [
                'name'  => [
                    'nullable' => FALSE,
                    'type'     => 'string'
                ],
                'value' => [
                    'nullable' => FALSE,
                    'type'     => 'string'
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // ObjectType::tryFromObject
    //------------------------------------------------------------------------------

    $runner->assertEquals(ObjectType::ClassObject, ObjectType::tryFromObject(new ClassObject()));
    $runner->assertEquals(ObjectType::ClassObject, ObjectType::tryFromObject(new class extends ClassObject { }));
    $runner->assertEquals(ObjectType::EnumObject, ObjectType::tryFromObject(new EnumObject()));
    $runner->assertEquals(ObjectType::EnumObject, ObjectType::tryFromObject(new class extends EnumObject { }));
    $runner->assertEquals(ObjectType::InterfaceObject, ObjectType::tryFromObject(new InterfaceObject()));
    $runner->assertEquals(ObjectType::InterfaceObject, ObjectType::tryFromObject(new class extends InterfaceObject { }));
    $runner->assertEquals(ObjectType::TraitObject, ObjectType::tryFromObject(new TraitObject()));
    $runner->assertEquals(ObjectType::TraitObject, ObjectType::tryFromObject(new class extends TraitObject { }));
    $runner->assertEquals(NULL, ObjectType::tryFromObject(new stdClass()));
    $runner->assertEquals(NULL, ObjectType::tryFromObject(new DateTime()));
};