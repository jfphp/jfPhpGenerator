<?php

namespace jf\php\generator\tests;

use jf\php\generator\ABase;
use jf\php\generator\Exception;
use jf\php\generator\TTranslations;
use jf\php\generator\TType;
use jf\php\generator\TUses;
use jf\php\generator\TValue;
use jf\tests\Runner;
use stdClass;

return function (Runner $runner)
{
    class TTypeTest extends ABase
    {
        use TType;

        public function __toString() : string
        {
            return static::class;
        }

        public function testSortTypes() : string
        {
            $_types = explode('|', $this->getPhpType());
            self::sortTypes($_types);

            return implode('|', $_types);
        }
    }

    $runner->testClassDefinition(
        TType::class,
        [
            'traits'     => [
                TTranslations::class,
                TUses::class,
                TValue::class
            ],
            'properties' => [
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    $types = [
        'B'     => 'B',
        'C'     => 'C',
        'E\F'   => 'E\F',
        'H'     => 'H',
        'J\K\L' => 'J\K\L',
        'R'     => 'R'
    ];

    //------------------------------------------------------------------------------
    // addIntersectionType(array|string|null $type)
    //------------------------------------------------------------------------------
    // Agregando de uno en uno y verificando de uno en uno
    //------------------------------------------------------------------------------

    foreach ($types as $type => $expected)
    {
        $test = new TTypeTest();
        $test->addIntersectionType($type);
        $runner->assertArrayResult([ $expected ], array_values($test->intersectionType));
        $runner->assertArrayResult($type[0] === '' ? [ substr($type, 1) => '' ] : [], $test->getUses());
    }
    $test = new TTypeTest();
    $test->addIntersectionType('A');
    $runner->assertArrayResult([ 'A' ], array_values($test->intersectionType));
    $runner->assertEquals([], $test->type);
    $test->addIntersectionType(NULL);
    $runner->assertEquals(NULL, $test->intersectionType);
    $runner->assertEquals([], $test->type);

    //------------------------------------------------------------------------------
    // addType(array|string|null $type)
    //------------------------------------------------------------------------------
    // Agregando de uno en uno y verificando de uno en uno
    //------------------------------------------------------------------------------

    foreach ($types as $type => $expected)
    {
        $test = new TTypeTest();
        $test->addType($type);
        $runner->assertArrayResult([ $expected ], array_values($test->type));
        $runner->assertArrayResult($type[0] === '' ? [ substr($type, 1) => '' ] : [], $test->getUses());
    }

    //------------------------------------------------------------------------------
    // Agregando de uno en uno y verificando todos
    //------------------------------------------------------------------------------

    $test  = new TTypeTest();
    $testA = new TTypeTest();
    $runner->assertEquals([], $testA->type, 'El tipo por defecto debe se un array');
    $testA->addType(NULL);
    $runner->assertEquals(NULL, $testA->type, 'El tipo debe ser nulo');
    foreach ($types as $type => $expected)
    {
        $test->addType($type);
        $testA->addType([ $type ]);
    }
    $expected     = array_values($types);
    $expectedType = array_combine($expected, $expected);
    $runner->assertArrayResult($expectedType, $test->type);
    $runner->assertArrayResult($expectedType, $testA->type);
    $expected     = array_filter(array_map(fn($type) => $type[0] === '' ? substr($type, 1) : FALSE, array_keys($types)));
    $expectedUses = array_combine($expected, array_fill(0, count($expected), ''));
    $runner->assertArrayResult($expectedUses, $test->getUses());
    $runner->assertArrayResult($expectedUses, $testA->getUses());

    //------------------------------------------------------------------------------
    // Agregando todos y verificando todos
    //------------------------------------------------------------------------------

    $test = new TTypeTest();
    $test->addType(array_keys($types));
    $runner->assertArrayResult($expectedType, $test->type);
    $runner->assertArrayResult($expectedUses, $test->getUses());

    //------------------------------------------------------------------------------
    // getPhpType()
    //------------------------------------------------------------------------------

    $test = new TTypeTest();
    $runner->assertEquals('', $test->getPhpType());
    $test->addType('int');
    $runner->assertEquals('int', $test->getPhpType());
    $test->addType('int');  // Verificamos que si existe no se agregue de nuevo
    $runner->assertEquals('int', $test->getPhpType());
    $test->addType('string');
    $runner->assertEquals('int|string', $test->getPhpType());
    $test->addType('float');
    $runner->assertEquals('float|int|string', $test->getPhpType());
    $test->addType('mixed'); // mixed reemplaza cualquier valor que haya
    $runner->assertEquals('mixed', $test->getPhpType());

    $runner->assertEquals('', TTypeTest::fromArray([ 'type' => 'callable' ])->getPhpType());
    $runner->assertEquals('string', TTypeTest::fromArray([ 'type' => 'string|callable' ])->getPhpType());

    // Tipo compuesto y además verificamos el orden

    $types = [ 'Abc', 'array', 'bool', 'float', 'int', 'object', 'string', 'NULL' ];
    $type  = implode('|', $types);
    shuffle($types);
    $runner->assertEquals($type, TTypeTest::fromArray([ 'type' => $types ])->getPhpType());
    $types = [ 'Abc', 'array', 'float', 'int', 'object', 'string', 'FALSE', 'NULL' ];
    $type  = implode('|', $types);
    shuffle($types);
    $runner->assertEquals($type, TTypeTest::fromArray([ 'type' => $types ])->getPhpType());
    $types = [ 'Abc', 'array', 'float', 'int', 'object', 'string', 'TRUE', 'NULL' ];
    $type  = implode('|', $types);
    shuffle($types);
    $runner->assertEquals($type, TTypeTest::fromArray([ 'type' => $types ])->getPhpType());

    $runner->assertEquals('float|int|string', TTypeTest::fromArray([ 'type' => 'int|string|float' ])->getPhpType());
    $runner->assertEquals('float|int|string', TTypeTest::fromArray([ 'type' => 'string' ])->addType('int|float')->getPhpType());

    // Tipos nulos

    $test = TTypeTest::fromArray([ 'type' => 'null' ]);
    $runner->assertEquals('null', $test->getPhpType());
    $runner->assertEquals('null', $test->null);
    $runner->assertEquals('', TTypeTest::fromArray([ 'type' => NULL ])->getPhpType());

    // Tipos resource

    $test = TTypeTest::fromArray([ 'type' => 'resource' ]);
    $runner->assertEquals('mixed', $test->getPhpType());

    // Asignar `test->null` si estaba vacía

    $test = TTypeTest::fromArray([ 'type' => 'NULL' ]);
    $runner->assertEquals('NULL', $test->getPhpType());
    $runner->assertEquals('NULL', $test->null);

    $test = TTypeTest::fromArray([ 'type' => 'string|NULL' ]);
    $runner->assertEquals('?string', $test->getPhpType());
    $runner->assertEquals('NULL', $test->null);

    $test = TTypeTest::fromArray([ 'null' => 'NULL', 'type' => 'string|null' ]);
    $runner->assertEquals('?string', $test->getPhpType());
    $runner->assertEquals('NULL', $test->null);

    $test = TTypeTest::fromArray([ 'type' => '?string' ]);
    $runner->assertEquals('?string', $test->getPhpType());
    $runner->assertEquals('NULL', $test->null);

    $test = TTypeTest::fromArray([ 'null' => 'null', 'type' => '?string' ]);
    $runner->assertEquals('?string', $test->getPhpType());
    $runner->assertEquals('null', $test->null);

    // Respetar el valor de `test->null` si tenía valor

    $test = TTypeTest::fromArray([ 'null' => 'null', 'type' => 'string|NULL' ]);
    $runner->assertEquals('?string', $test->getPhpType());
    $runner->assertEquals('null', $test->null);

    $test = TTypeTest::fromArray([ 'type' => 'string|bool|null' ]);
    $runner->assertEquals('bool|string|null', $test->getPhpType());
    $runner->assertEquals('null', $test->null);

    $test = TTypeTest::fromArray([ 'type' => '?string' ])->addType('bool');
    $runner->assertEquals('bool|string|NULL', $test->getPhpType());
    $runner->assertEquals('NULL', $test->null);

    // Arrays

    $runner->assertEquals('array', TTypeTest::fromArray([ 'type' => 'array[]' ])->getPhpType());
    $runner->assertEquals('array|string', TTypeTest::fromArray([ 'type' => 'string|string[]' ])->getPhpType());
    $runner->assertEquals('A|B|array', TTypeTest::fromArray([ 'type' => 'A|B|C[]' ])->getPhpType());
    $runner->assertEquals('A|B|array', TTypeTest::fromArray([ 'type' => 'A|B|C[]|array' ])->getPhpType());
    $runner->assertEquals('A|B|C[]', implode('|', TTypeTest::fromArray([ 'type' => 'A|B|C[]|array' ])->type));

    // Genéricos

    $runner->assertEquals('Collection', TTypeTest::fromArray([ 'type' => 'Collection<Item>' ])->getPhpType());

    // true & false (PHP 8.2)

    $runner->assertEquals('string|FALSE', TTypeTest::fromArray([ 'type' => 'false|string' ])->getPhpType());
    $runner->assertEquals('string|TRUE', TTypeTest::fromArray([ 'type' => 'string|true' ])->getPhpType());
    $runner->assertEquals('bool|string', TTypeTest::fromArray([ 'type' => 'true|false|string' ])->getPhpType());

    // Intersección de tipos (PHP 8.2).
    // Prueba rápida ya que por debajo usa los mismos métodos, solamente se cambia el | por &.

    $runner->assertEquals('A&B', TTypeTest::fromArray([ 'intersectionType' => 'A&B' ])->getPhpType());
    $runner->assertEquals('A&B', TTypeTest::fromArray([ 'intersectionType' => 'A&B&null' ])->getPhpType());
    $runner->assertEquals('A&B', TTypeTest::fromArray([ 'type' => 'A&B&null' ])->getPhpType());

    // No se pueden mezclar uniones e intersecciones
    $runner->assertException(Exception::class, fn() => TTypeTest::fromArray([ 'type' => 'A&B|C' ]));

    //------------------------------------------------------------------------------
    // inferType()
    //------------------------------------------------------------------------------

    $types = [
        [ 'bool', FALSE ],
        [ 'bool', TRUE ],
        [
            'callable',
            function ()
            {
                return NULL;
            }
        ],
        [
            'callable',
            new class
            {
                public function __invoke() : void
                {
                }
            }
        ],
        [ 'float', 0.1 ],
        [ 'float', 1.2 ],
        [ 'int', 0 ],
        [ 'int', 1 ],
        [ 'object', new stdClass() ],
        [ 'string', 'a' ],
        [ 'string', 'bc' ],
    ];
    foreach ($types as [$expected, $value])
    {
        $runner->assertEquals($expected, TTypeTest::inferType($value));
    }

    //------------------------------------------------------------------------------
    // _sortTypes()
    //------------------------------------------------------------------------------

    $runner->assertEquals('', TTypeTest::new()->testSortTypes());
    $runner->assertEquals('a|b|null', TTypeTest::fromArray([ 'type' => 'null|b|a' ])->testSortTypes());
    $runner->assertEquals('a|b|null', TTypeTest::fromArray([ 'type' => 'a|null|b' ])->testSortTypes());
    $runner->assertEquals('A|a|NULL', TTypeTest::fromArray([ 'type' => 'NULL|a|A' ])->testSortTypes());
    $runner->assertEquals('a|b', TTypeTest::fromArray([ 'type' => 'b|a' ])->testSortTypes());
    $runner->assertEquals('A|a', TTypeTest::fromArray([ 'type' => 'a|A' ])->testSortTypes());
    // Para cubrir el 100% se necesitan comparar 2 NULL.
    $types = [ 'NULL', 'A', 'a', 'b', 'B', 'NULL' ];
    TTypeTest::sortTypes($types);
    $runner->assertEquals([ 'A', 'B', 'a', 'b', 'NULL', 'NULL' ], $types);

    //------------------------------------------------------------------------------
    // setValue()
    //------------------------------------------------------------------------------

    $runner->assertEquals('bool', TTypeTest::fromArray([ 'value' => TRUE ])->getPhpType());
    $runner->assertEquals('int', TTypeTest::fromArray([ 'value' => 1 ])->getPhpType());
    $runner->assertEquals('float', TTypeTest::fromArray([ 'value' => 1.1 ])->getPhpType());
    $runner->assertEquals('NULL', TTypeTest::fromArray([ 'value' => NULL ])->getPhpType());
    $runner->assertEquals('string', TTypeTest::fromArray([ 'type' => 'string' ])->setValue(1.1)->getPhpType());
};