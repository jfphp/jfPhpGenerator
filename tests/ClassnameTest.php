<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\ABase;
use jf\php\generator\Classname;
use jf\php\generator\IUses;
use jf\php\generator\TName;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Classname::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IItem::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TName::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Classname::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Classname::new()->__toString());
    $runner->assertEquals('Ab', Classname::fromArray([ 'name' => 'Ab' ])->__toString());
    $runner->assertEquals('A', Classname::fromArray([ 'name' => '\A' ])->__toString());
    $runner->assertEquals('A\B', Classname::fromArray([ 'name' => 'A\B' ])->__toString());
    $runner->assertEquals('B', Classname::fromArray([ 'name' => '\A\B' ])->__toString());

    //------------------------------------------------------------------------------
    // Classname::getUses
    //------------------------------------------------------------------------------

    $runner->assertEquals([], Classname::new()->getUses());
    $runner->assertEquals([], Classname::fromArray([ 'name' => 'Ab' ])->getUses());
    $runner->assertEquals([ 'Ab' => '' ], Classname::fromArray([ 'name' => '\Ab' ])->getUses());
    $runner->assertEquals([ 'A' => '' ], Classname::fromArray([ 'name' => '\A' ])->getUses());
    $runner->assertEquals([ 'A\B' => '' ], Classname::fromArray([ 'name' => 'A\B' ])->getUses());
    $runner->assertEquals([ 'A\B' => '' ], Classname::fromArray([ 'name' => '\A\B' ])->getUses());

    //------------------------------------------------------------------------------
    // Classname::setName
    //------------------------------------------------------------------------------

    $sut = Classname::fromArray([ 'name' => 'Ab@C' ]);
    $runner->assertEquals('Ab', $sut->name);
    $runner->assertEquals('C', $sut->alias);
};