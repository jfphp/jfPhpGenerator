<?php

namespace jf\php\generator\tests;

use jf\assert\Assert;
use jf\php\generator\ABase;
use jf\php\generator\collection\Properties;
use jf\php\generator\Property;
use jf\php\generator\TProperties;
use jf\php\generator\TTranslations;
use jf\tests\Runner;

return function (Runner $runner)
{
    class PropertyExtended extends Property
    {

    }

    class TPropertiesTest extends ABase
    {
        use TProperties
        {
            getProperties as private _getProperties;
        }

        public array  $methods       = [];

        public string $propertyClass = '';

        public function __toString() : string
        {
            return self::class;
        }

        public function addMethods(array $methods) : static
        {
            foreach ($methods as $_method)
            {
                $this->methods[] = $_method;
            }

            return $this;
        }

        public function getFqcn() : string
        {
            return static::class;
        }

        public function getProperties() : Properties
        {
            if (!$this->properties)
            {
                $class = $this->propertyClass;
                if ($class)
                {
                    $this->properties = new Properties($class);
                }
                $this->_getProperties()->type = '';
            }

            return $this->properties;
        }
    }

    $runner->testClassDefinition(
        TProperties::class,
        [
            'traits'     => [
                TTranslations::class
            ],
            'properties' => [
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'properties'   => Properties::class,
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TProperties::addProperties
    //------------------------------------------------------------------------------

    $data                           = [ [ 'name' => 'floatprop' ] ];
    $sut                            = TPropertiesTest::new();
    $sut->getProperties()->defaults = [ 'type' => 'float' ];
    $sut->addProperties($data);
    $runner->assertArrayCount(count($data), $sut->properties);
    $property = $sut->getProperties()->first();
    $runner->assertInstanceOf(Property::class, $property);
    $config = (new Property([ ...$data[0], 'type' => [ 'float' => 'float' ] ]))->toArray();
    $runner->assertArrayResult($config, $property->toArray());

    $int   = random_int(0, PHP_INT_MAX);
    $float = microtime(TRUE);
    $data  = [
        [ 'name' => 'a' ],
        [ 'name' => 'b', 'scope' => 'protected', 'type' => 'string' ],
        [ 'name' => 'c', 'scope' => 'private', 'type' => 'int', 'value' => $int ],
        [ 'name' => 'd', 'scope' => 'public', 'type' => 'float', 'value' => $float, 'null' => 'NULL' ]
    ];
    $sut   = TPropertiesTest::fromArray([ 'properties' => $data ]);
    $runner->assertArrayCount(count($data), $sut->properties);
    foreach ($sut->properties as $property)
    {
        $runner->assertInstanceOf(Property::class, $property);
        $config = current($data);
        $config = (new Property($config))->toArray();
        $runner->assertArrayResult($config, $property->toArray());
        next($data);
    }

    //------------------------------------------------------------------------------
    // TProperties::__clone
    //------------------------------------------------------------------------------

    $clone = $sut;
    foreach ($sut->properties as $name => $property)
    {
        $runner->assertTrue($clone->properties[ $name ] === $property, 'La propiedad debería ser la misma instancia');
    }

    $clone = clone $sut;
    foreach ($sut->properties as $name => $property)
    {
        $runner->assertTrue($clone->properties[ $name ] !== $property, 'La propiedad debería ser una instancia diferente');
    }

    //------------------------------------------------------------------------------
    // TProperties::buildProperties
    // Las pruebas de construcción de una propiedad se verifican en PropertyTest.
    //------------------------------------------------------------------------------

    $runner->assertArrayResult(
        [
            'public $a;',
            "/**\n * @var string\n */\nprotected string \$b = '';",
            "/**\n * @var int\n */\nprivate int \$c = $int;",
            "/**\n * @var float|NULL\n */\npublic ?float \$d = $float;"
        ],
        $sut->buildProperties()
    );

    //------------------------------------------------------------------------------
    // getter
    //------------------------------------------------------------------------------

    $property = [
        'getter' => TRUE,
        'name'   => 'abc'
    ];
    $sut      = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $runner->assertEquals([], $sut->methods);
    $sut->buildProperties();
    $runner->assertArrayResult(
        [
            [
                'body'        => 'return $this->abc;',
                'description' => 'Devuelve el valor de la propiedad `abc`',
                'name'        => 'getAbc',
                'null'        => '',
                'type'        => ''
            ]
        ],
        $sut->methods
    );

    $property = [
        'getter' => [
            'abc'         => 1,
            'def'         => 2,
            'description' => 'lorem ipsum'
        ],
        'name'   => 'abc'
    ];
    $sut      = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $runner->assertEquals([], $sut->methods);
    $sut->buildProperties();
    $runner->assertArrayResult(
        [
            [
                'abc'         => 1,
                'def'         => 2,
                'body'        => 'return $this->abc;',
                'description' => 'lorem ipsum',
                'name'        => 'getAbc',
                'null'        => '',
                'type'        => ''
            ]
        ],
        $sut->methods
    );

    // Verificamos que se acepte una clase que extienda de Property.
    $runner->assertInstanceOf(
        Properties::class,
        TPropertiesTest::fromArray([ 'propertyClass' => PropertyExtended::class, 'properties' => [ $property ] ])->getProperties()
    );

    // Verificamos que se lance una excepción si la clase no extiende de Property.
    $runner->assertException(
        new Assert('Se esperaba un objeto subclase de `jf\php\generator\Property`'),
        fn() => TPropertiesTest::fromArray([ 'propertyClass' => ABase::class, 'properties' => [ $property ] ])->getProperties()
    );

    //------------------------------------------------------------------------------
    // setter
    //------------------------------------------------------------------------------

    $property = [
        'setter' => TRUE,
        'name'   => 'item',
        'null'   => 'NULL',
        'type'   => 'float',
        'value'  => $float,
    ];
    $sut      = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $runner->assertEquals([], $sut->methods);
    $sut->buildProperties();
    $runner->assertArrayResult(
        [
            [
                'body'        => "\$this->item = \$item;\n\nreturn \$this;",
                'description' => 'Asigna el valor de la propiedad `item`',
                'name'        => 'setItem',
                'type'        => 'static',
                'params'      => [
                    [
                        'description' => 'Valor a asignar',
                        'name'        => 'item',
                        'null'        => 'NULL',
                        'type'        => 'float'
                    ]
                ]
            ]
        ],
        $sut->methods
    );

    $property = [
        'setter' => [
            'abc'         => 'zyx',
            'def'         => $int,
            'description' => 'lorem ipsum 2',
            'params'      => [ [ 'xyz' => 'cba' ] ]
        ],
        'name'   => 'item',
        'null'   => 'NULL',
        'type'   => 'float',
        'value'  => $float,
    ];
    $sut      = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $runner->assertEquals([], $sut->methods);
    $sut->buildProperties();
    $runner->assertArrayResult(
        [
            [
                'abc'         => 'zyx',
                'def'         => $int,
                'body'        => "\$this->item = \$item;\n\nreturn \$this;",
                'description' => 'lorem ipsum 2',
                'name'        => 'setItem',
                'type'        => 'static',
                'params'      => [
                    [
                        'description' => 'Valor a asignar',
                        'name'        => 'item',
                        'null'        => 'NULL',
                        'type'        => 'float',
                        'xyz'         => 'cba'
                    ]
                ]
            ]
        ],
        $sut->methods
    );

    //------------------------------------------------------------------------------
    // getter & setter
    //------------------------------------------------------------------------------

    $name     = 'value';
    $property = [ 'getter' => TRUE, 'setter' => TRUE, 'name' => $name ];
    $sut      = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $runner->assertEquals([], $sut->methods);
    $runner->assertEquals('', $sut->properties[ $name ]->scope);
    $sut->buildProperties();
    $runner->assertEquals('protected', $sut->properties[ $name ]->scope);
    $runner->assertArrayResult(
        [
            [
                'body'        => 'return $this->value;',
                'description' => 'Devuelve el valor de la propiedad `value`',
                'name'        => 'getValue',
                'null'        => '',
                'type'        => ''
            ],
            [
                'body'        => "\$this->value = \$value;\n\nreturn \$this;",
                'description' => 'Asigna el valor de la propiedad `value`',
                'name'        => 'setValue',
                'type'        => 'static',
                'params'      => [
                    [
                        'description' => 'Valor a asignar',
                        'name'        => 'value',
                        'null'        => '',
                        'type'        => ''
                    ]
                ]
            ]
        ],
        $sut->methods
    );

    $sut                             = TPropertiesTest::fromArray([ 'properties' => [ $property ] ]);
    $sut->properties[ $name ]->scope = 'public';
    $sut->buildProperties();
    $runner->assertEquals('public', $sut->properties[ $name ]->scope);

    //------------------------------------------------------------------------------
    // sort
    //------------------------------------------------------------------------------

    $sut = TPropertiesTest::fromArray(
        [
            'properties' => [
                [ 'name' => 'x' ],
                [ 'name' => 'c', 'const' => TRUE ],
                [ 'name' => 'z', 'const' => TRUE ],
                [ 'name' => 'a' ],
                [ 'name' => 'y' ],
                [ 'name' => 'b', 'const' => TRUE ],
            ]
        ]
    );
    $runner->assertEquals([ 'b', 'c', 'z', 'a', 'x', 'y' ], $sut->properties->sort()->keys());
};
