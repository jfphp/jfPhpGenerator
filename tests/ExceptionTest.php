<?php

namespace jf\php\generator\tests;

use jf\assert\Assert;
use jf\php\generator\Exception;
use jf\tests\Runner;
use Stringable;
use Throwable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Exception::class,
        [
            'extends'    => Assert::class,
            'implements' => [
                Stringable::class,
                Throwable::class
            ]
        ]
    );
};