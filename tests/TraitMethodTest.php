<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\ABase;
use jf\php\generator\TName;
use jf\php\generator\TraitMethod;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        TraitMethod::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IItem::class,
                IToArray::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TName::class
            ],
            'properties' => [
                'alias'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'insteadof'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'namespace'    => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'package'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'scope'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'subnamespace' => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // TraitMethod::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', (string) TraitMethod::new());
    $runner->assertEquals(
        'Abc::method insteadof Xyz;',
        (string) TraitMethod::fromArray([ 'name' => 'method', 'namespace' => 'Abc', 'insteadof' => 'Xyz' ])
    );
    $runner->assertEquals(
        'Abc::method as private _method;',
        (string) TraitMethod::fromArray([ 'name' => 'method', 'namespace' => 'Abc' ])
    );
    $runner->assertEquals(
        'Abc::method as newname;',
        (string) TraitMethod::fromArray([ 'alias' => 'newname', 'name' => 'method', 'namespace' => 'Abc' ])
    );
    $runner->assertEquals(
        'method as private other;',
        (string) TraitMethod::fromArray([ 'alias' => 'other', 'name' => 'method', 'scope' => 'private' ])
    );
    $runner->assertEquals(
        'method as private _method;',
        (string) TraitMethod::fromArray([ 'name' => 'method' ])
    );
};