<?php

namespace jf\php\generator\tests;

use jf\php\generator\IProperties;
use jf\tests\Runner;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        IProperties::class,
        [
            'methods' => [
                'addProperties'   => [
                    'type'   => 'static',
                    'value'  => 'jf\php\generator\IProperties::class',
                    'params' => [
                        'properties' => [
                            'type' => 'array|string'
                        ]
                    ]
                ],
                'buildProperties' => [
                    'type'  => 'array',
                    'value' => '[]'
                ],
                'getProperties'   => [
                    'type'  => 'jf\php\generator\collection\Properties',
                    'value' => "''"
                ]
            ]
        ]
    );
};