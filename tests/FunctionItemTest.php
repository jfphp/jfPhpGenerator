<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\ABase;
use jf\php\generator\collection\Attributes;
use jf\php\generator\collection\Functions;
use jf\php\generator\collection\Params;
use jf\php\generator\FunctionItem;
use jf\php\generator\IAttributes;
use jf\php\generator\IGenerator;
use jf\php\generator\IParams;
use jf\php\generator\IUses;
use jf\php\generator\TAttributes;
use jf\php\generator\TCollectionItem;
use jf\php\generator\TDocDescription;
use jf\php\generator\TDocTags;
use jf\php\generator\TDocType;
use jf\php\generator\TParams;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        FunctionItem::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IItem::class,
                IGenerator::class,
                IParams::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TCollectionItem::class,
                TDocDescription::class,
                TDocTags::class,
                TDocType::class,
                TParams::class
            ],
            'properties' => [
                'attributes'       => Attributes::class,
                'body'             => 'string',
                'copydoc'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'           => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'keyword'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => 'function'
                ],
                'name'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'params'           => Params::class,
                'return'           => 'string',
                'static'           => [
                    'type'  => 'bool',
                    'value' => FALSE
                ],
                'tags'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // FunctionItem::__toString
    // Este método delega en el resultado de buildCode, solamente se comprueba
    // lo que se agrega a este resultado.
    //------------------------------------------------------------------------------

    $sut = FunctionItem::fromArray(
        [
            'body' => 'return $a + $b + $c;',
            'name' => 'abc',
            'null' => 'NULL',
            'type' => 'string'
        ]
    );

    $sut->getParams()->type = '';
    $sut->addParams([ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ]);
    $runner->assertEquals(
        <<<'PHP'
/**
 * @param $a
 * @param $b
 * @param $c
 *
 * @return string|NULL
 */
function abc($a, $b, $c) : ?string
{
    return $a + $b + $c;
}
PHP,
        $sut->__toString()
    );
    //------------------------------------------------------------------------------
    // FunctionItem::buildCode
    //------------------------------------------------------------------------------

    $tests = [
        [ [ 'function () : void' ], [] ],
        [ [ 'function () : void', '{', '}' ], [ 'body' => '' ] ],
        [ [ 'function z() : NULL' ], [ 'name' => 'z', 'null' => 'NULL' ] ],
        [ [ 'static function () : void', '{', '    abc', '}' ], [ 'body' => 'abc', 'static' => TRUE ] ],
        // Inferiendo si es estático o no
        [ [ 'static function () : void', '{', '    abc', '}' ], [ 'body' => 'abc', 'static' => NULL ] ],
        [ [ 'function () : void', '{', '    $this->abc();', '}' ], [ 'body' => '$this->abc();', 'static' => NULL ] ],
        [ [ 'function x() : static', '{', '    return $this;', '}' ], [ 'body' => '', 'name' => 'x', 'type' => 'static' ] ],
        [
            [ 'static function y() : static', '{', '    return static;', '}' ],
            [ 'body' => '', 'name' => 'y', 'static' => TRUE, 'type' => 'static' ]
        ],
        // Inline
        [ [ 'fn () : int => abc();' ], [ 'body' => ' abc(); ', 'keyword' => 'fn', 'type' => 'int' ] ],
        [
            [ 'fn () : void =>', '{', "    abc();\n    d();", '}' ],
            [ 'body' => [ 'abc();', 'd();' ], 'keyword' => 'fn' ]
        ]
    ];

    $items = [];
    $code  = [];
    foreach ($tests as [$expected, $values])
    {
        $fn = FunctionItem::fromArray($values);
        // Omitimos las funciones anónimas
        if ($fn->name)
        {
            $items[] = $values;
            $type    = $values['type'] ?? 'NULL';

            $code[ $fn->getCollectionKey() ] = "/**\n * @return $type\n */\n" . implode(PHP_EOL, $expected);
        }
        $runner->assertEquals($expected, $fn->buildCode());
    }
    ksort($code);
    $runner->assertEquals(array_values($code), Functions::fromItems($items)->render());

    $values = [
        'bool'   => 'TRUE',
        'TRUE'   => 'TRUE',
        'FALSE'  => 'FALSE',
        'int'    => '0',
        'float'  => '0.0',
        'void'   => NULL,
        'string' => "''",
    ];
    foreach ($values as $type => $expected)
    {
        $name = 'format' . ucfirst(strtolower($type));
        $runner->assertEquals(
            array_values(
                array_filter(
                    [
                        sprintf('function %s() : %s', $name, $type),
                        '{',
                        $expected === NULL ? NULL : sprintf('    return %s;', $expected),
                        '}'
                    ]
                )
            ),
            FunctionItem::fromArray([ 'body' => '', 'name' => $name, 'type' => $type ])->buildCode()
        );
    }
    $runner->assertEquals(
        [ 'function abc(string $a, string $b, string $c) : ?string', '{', '    return NULL;', '}' ],
        FunctionItem::fromArray(
            [
                'body'   => '',
                'name'   => 'abc',
                'null'   => 'NULL',
                'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ],
                'type'   => 'string'
            ]
        )
            ->buildCode()
    );

    //------------------------------------------------------------------------------
    // FunctionItem::renderTags
    //------------------------------------------------------------------------------

    $runner->assertEquals([], FunctionItem::new()->renderTags());
    $runner->assertEquals(
        [ '@see A::a()' ],
        FunctionItem::fromArray([ 'copydoc' => 'A::a()', 'name' => 'c' ])->renderTags()
    );
    $runner->assertEquals(
        [ '@throws C', '', '@see A::a()' ],
        FunctionItem::fromArray([ 'copydoc' => 'A::a()', 'name' => 'c', 'tags' => [ [ 'name' => 'throws', 'value' => 'C' ] ] ])
            ->renderTags()
    );
    $runner->assertEquals(
        [ '@param string $a', '@param string $b', '@param string $c' ],
        FunctionItem::fromArray([ 'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ] ])->renderTags()
    );
    $runner->assertEquals(
        [ '@param string $a', '@param string $b', '@param string $c', '', '@throws RuntimeException' ],
        FunctionItem::fromArray(
            [
                'params' => [ [ 'name' => 'a' ], [ 'name' => 'b' ], [ 'name' => 'c' ] ],
                'tags'   => [ [ 'name' => 'throws', 'value' => 'RuntimeException' ] ]
            ]
        )->renderTags()
    );
    $runner->assertEquals(
        [ '@return string' ],
        FunctionItem::fromArray([ 'type' => 'string' ])->renderTags()
    );
    $text = ucfirst(bin2hex(random_bytes(32)));
    $runner->assertEquals(
        [ "@return string $text." ],
        FunctionItem::fromArray([ 'return' => lcfirst($text), 'type' => 'string' ])->renderTags()
    );

    //------------------------------------------------------------------------------
    // FunctionItem::setBody
    //------------------------------------------------------------------------------

    $runner->assertEquals(NULL, FunctionItem::new()->body);
    $runner->assertEquals(NULL, FunctionItem::new()->setBody(NULL)->body);
    $runner->assertEquals('', FunctionItem::new()->setBody('')->body);
    $runner->assertEquals('    a', FunctionItem::new()->setBody('a')->body);
    $runner->assertEquals(' a', FunctionItem::new()->setBody(' a')->body);
    $runner->assertEquals("    a\n    b", FunctionItem::new()->setBody([ 'a', 'b' ])->body);

    $sut = FunctionItem::fromArray([ 'body' => 'return new \DateTime();' ]);
    $runner->assertEquals('    return new DateTime();', $sut->body);
    $runner->assertArrayResult([ 'use DateTime;' ], $sut->renderUses());

    $sut = FunctionItem::fromArray([ 'body' => 'return new \DateTime@PhpDateTime();' ]);
    $runner->assertEquals('    return new PhpDateTime();', $sut->body);
    $runner->assertArrayResult([ 'use DateTime as PhpDateTime;' ], $sut->renderUses());
};