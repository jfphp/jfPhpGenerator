<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\php\generator\ABase;
use jf\php\generator\IUses;
use jf\php\generator\Tag;
use jf\php\generator\TDescription;
use jf\php\generator\TUses;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Tag::class,
        [
            'extends'    => ABase::class,
            'implements' => [
                IAssign::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TDescription::class,
                TUses::class
            ],
            'properties' => [
                'description'  => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'       => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'name'         => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'tagWithUses'  => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => [ 'extends', 'implements', 'mixin', 'see', 'throws', 'use' ]

                ],
                'translations' => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'uses'         => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'        => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'variable'     => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ]
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Tag::getName
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Tag::new()->getName());
    $runner->assertEquals('@return', Tag::fromArray([ 'name' => 'return' ])->getName());
    $runner->assertEquals('@return', Tag::fromArray([ 'name' => '@return' ])->getName());

    //------------------------------------------------------------------------------
    // Tag::getVariable
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Tag::new()->getVariable());
    $runner->assertEquals('$var', Tag::fromArray([ 'variable' => 'var' ])->getVariable());
    $runner->assertEquals('$var', Tag::fromArray([ 'variable' => '$var' ])->getVariable());
    $runner->assertEquals('...$var', Tag::fromArray([ 'variable' => '...var' ])->getVariable());

    //------------------------------------------------------------------------------
    // Tag::setValue
    //------------------------------------------------------------------------------

    $runner->assertEquals('@extends Cde', Tag::fromArray([ 'name' => 'extends', 'value' => '\Abc\Cde' ])->__toString());
    $sut = Tag::fromArray([ 'name' => 'extends', 'value' => '\Abc\Cde<\X\Yz>' ]);
    $runner->assertEquals('@extends Cde<Yz>', $sut->__toString());
    $runner->assertEquals([ 'Abc\Cde' => '', 'X\Yz' => '', ], $sut->getUses());

    //------------------------------------------------------------------------------
    // Tag::__toString
    //------------------------------------------------------------------------------

    $runner->assertEquals('', Tag::new()->__toString());
    $runner->assertEquals('@return', Tag::fromArray([ 'name' => 'return' ])->__toString());
    $runner->assertEquals('@note Abc.', Tag::fromArray([ 'description' => 'abc', 'name' => 'note' ])->__toString());
    $runner->assertEquals('$var', Tag::fromArray([ 'variable' => 'var' ])->__toString());
    $runner->assertEquals(
        '$var Var description.',
        Tag::fromArray([ 'description' => 'var description', 'variable' => 'var' ])->__toString()
    );
    $runner->assertEquals('@param $name', Tag::fromArray([ 'name' => 'param', 'variable' => 'name' ])->__toString());
    $runner->assertEquals(
        '@param $name Desc param.',
        Tag::fromArray([ 'description' => 'desc param', 'name' => 'param', 'variable' => 'name' ])->__toString()
    );
    $sut = Tag::fromArray([ 'name' => 'throws', 'value' => '\Abc\De' ]);
    $runner->assertEquals('@throws De', $sut->__toString());
    $runner->assertTrue(($sut->uses['Abc\De'] ?? NULL) === '', 'No se importó la clase');
};
