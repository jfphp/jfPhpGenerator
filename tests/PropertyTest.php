<?php

namespace jf\php\generator\tests;

use jf\Base\IAssign;
use jf\Base\IToArray;
use jf\Collection\IItem;
use jf\php\generator\collection\Attributes;
use jf\php\generator\IAttributes;
use jf\php\generator\IGenerator;
use jf\php\generator\IUses;
use jf\php\generator\Property;
use jf\php\generator\TAttributes;
use jf\php\generator\TCollectionItem;
use jf\php\generator\TModifiers;
use jf\php\generator\Variable;
use jf\tests\Runner;
use JsonSerializable;
use Serializable;
use Stringable;

return function (Runner $runner)
{
    $runner->testClassDefinition(
        Property::class,
        [
            'extends'    => Variable::class,
            'implements' => [
                IAssign::class,
                IAttributes::class,
                IGenerator::class,
                IItem::class,
                IToArray::class,
                IUses::class,
                JsonSerializable::class,
                Serializable::class,
                Stringable::class
            ],
            'traits'     => [
                TAttributes::class,
                TCollectionItem::class,
                TModifiers::class
            ],
            'properties' => [
                'abstract'         => 'bool',
                'attributes'       => Attributes::class,
                'const'            => 'bool',
                'copydoc'          => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'description'      => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'extras'           => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'final'            => 'bool',
                'getter'           => [
                    'nullable' => FALSE,
                    'type'     => [
                        'array',
                        'bool',
                        'string'
                    ],
                    'value'    => FALSE
                ],
                'hasValue'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => TRUE
                ],
                'init'             => [
                    'nullable' => TRUE,
                    'type'     => [
                        'array',
                        'bool',
                        'string'
                    ]
                ],
                'intersectionType' => [
                    'type'  => 'array',
                    'value' => []
                ],
                'name'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'null'             => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => ''
                ],
                'override'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'prefix'           => [
                    'nullable' => FALSE,
                    'type'     => 'string',
                    'value'    => '$'
                ],
                'readonly'         => [
                    'nullable' => FALSE,
                    'type'     => 'bool',
                    'value'    => FALSE
                ],
                'scope'            => [
                    'type'  => 'string',
                    'value' => ''
                ],
                'setter'           => [
                    'nullable' => FALSE,
                    'type'     => [
                        'array',
                        'bool',
                        'string'
                    ],
                    'value'    => FALSE
                ],
                'static'           => [
                    'type'  => 'bool',
                    'value' => FALSE
                ],
                'tags'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'translations'     => [
                    'nullable' => FALSE,
                    'static'   => TRUE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'type'             => [
                    'type'  => 'array',
                    'value' => []
                ],
                'uses'             => [
                    'nullable' => FALSE,
                    'type'     => 'array',
                    'value'    => []
                ],
                'value'            => 'mixed'
            ]
        ]
    );

    //------------------------------------------------------------------------------
    // Property::buildCode
    //------------------------------------------------------------------------------

    $tests = [
        [ [], [] ],
        [ [ 'public', '$abc' ], [ 'name' => 'abc' ] ],
        [ [ '$abc' ], [ 'name' => 'abc', 'scope' => NULL ] ],
        [ [ 'final', 'public', 'int', '$abc', '= 1' ], [ 'final' => TRUE, 'name' => 'abc', 'value' => 1 ] ],
        [ [ 'public', 'int', '$abc', '= 1' ], [ 'name' => 'abc', 'value' => 1 ] ],
        [ [ 'public', 'readonly', '$abc' ], [ 'name' => 'abc', 'readonly' => TRUE ] ],
        [ [ 'public', 'const', 'ABC' ], [ 'name' => 'abc', 'const' => TRUE ] ],
        [ [ 'public', 'const', 'ABC', '= TRUE' ], [ 'name' => 'abc', 'const' => TRUE, 'value' => TRUE ] ],
        [ [ 'public', 'static', '$abc' ], [ 'name' => 'abc', 'static' => TRUE ] ],
        [
            [ 'public', 'readonly', '$abc' ],
            [ 'name' => 'abc', 'const' => TRUE, 'readonly' => TRUE, 'static' => TRUE ]
        ],
        [ [ 'public', 'const', 'ABC' ], [ 'name' => 'abc', 'const' => TRUE, 'static' => TRUE ] ],

    ];
    foreach ($tests as [$expected, $config])
    {
        $runner->assertEquals($expected, Property::fromArray($config)->buildCode());
    }

    //------------------------------------------------------------------------------
    // Property::renderTags
    //------------------------------------------------------------------------------

    $tests = [
        [ [], [] ],
        [ [], [ 'name' => 'abc' ] ],
        [ [ '@var int' ], [ 'name' => 'abc', 'value' => 1 ] ],
        [ [ '@var bool' ], [ 'name' => 'abc', 'value' => TRUE ] ],
        [ [ '@inheritdoc' ], [ 'description' => 'lorem ipsum', 'name' => 'abc', 'value' => TRUE, 'override' => TRUE ] ],
        [
            [ '@see A\B::c()' ],
            [ 'copydoc' => 'A\B::c()', 'description' => 'lorem ipsum', 'name' => 'abc', 'value' => TRUE ]
        ],
    ];
    foreach ($tests as [$expected, $config])
    {
        $runner->assertEquals($expected, Property::fromArray($config)->renderTags());
    }

    //------------------------------------------------------------------------------
    // Property::setName
    //------------------------------------------------------------------------------

    foreach ([
                 Runner::getRandomMd5()        => '',
                 '_' . Runner::getRandomMd5()  => 'protected',
                 '__' . Runner::getRandomMd5() => ''
             ] as $name => $scope)
    {
        $property = Property::fromArray([ 'name' => $name ]);
        $runner->assertEquals($name, $property->name);
        $runner->assertEquals($scope, $property->scope);
    }

    //------------------------------------------------------------------------------
    // Property::__toString
    //------------------------------------------------------------------------------

    $tests = [
        [ '', [] ],
        [ 'public $abc;', [ 'name' => 'abc' ] ],
        [ "/**\n * @var int\n */\npublic int \$abc = 1;", [ 'name' => 'abc', 'value' => 1 ] ],
    ];
    foreach ($tests as [$expected, $config])
    {
        $runner->assertEquals($expected, (string) Property::fromArray($config));
    }
    $runner->assertEquals(
        <<<'PHP'
/**
 * Lorem ipsum dolor sit amet.
 *
 * @var A[]|NULL
 *
 * @see http://www.joaquinfernandez.net
 */
#[Column(name: 'items')]
public ?array $items = [];
PHP,
        (string) Property::fromArray(
            [
                'attributes'  => [
                    [
                        'arguments' => [ 'name' => 'items' ],
                        'name'      => 'Column'
                    ]
                ],
                'description' => 'lorem ipsum dolor sit amet',
                'name'        => 'items',
                'null'        => 'NULL',
                'tags'        => [
                    [
                        [
                            'name'  => 'see',
                            'value' => 'http://www.joaquinfernandez.net'
                        ]
                    ]
                ],
                'type'        => 'A[]',
                'value'       => []
            ]
        )
    );
};
