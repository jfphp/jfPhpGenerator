#!/usr/bin/env php
<?php

/**
 * Funciones comunes para otros scripts.
 *
 * @internal
 */

use jf\assert\Assert;
use jf\assert\http\NotFound;
use jf\assert\php\UnexpectedValueException;
use jf\Base\File\Yaml;
use jf\php\generator\Formatter;
use jf\php\generator\Generator;
use jf\Reflector\ReflectionClass;
use jf\Serializer\Sorter\ScalarFirst;

require_once __DIR__ . '/../../autoload.php';

/**
 * Agrega la configuración al archivo `generator.yaml` existente.
 *
 * @param array  $config   Configuración a agregar. Debe estar agrupada por tipos (`classes`, `traits`, etc).
 * @param string $filename Ruta del archivo con el que se ha generado la configuración.
 *
 * @return void
 */
function addToGenerator(array $config, string $filename) : void
{
    $_generator = findGeneratorFile($filename);
    $_current   = getGeneratorContent($_generator);
    foreach ($config as $_type => $_items)
    {
        foreach ($_items as $_name => $_item)
        {
            $_current[ $_type ][ $_name ] = $_item;
        }
    }
    sortConfiguration($_current);
    printf("Guardando archivo `%s`: %d bytes\n", $_generator, Yaml::create()->saveYaml($_generator, $_current) ?: -1);
}

/**
 * Devuelve la configuración del elemento generador a partir del
 * nombre de un archivo que contiene una clase, enum, interfaz o trait.
 *
 * @param string $filename Ruta completa del archivo a analizar.
 *
 * @return array|null
 *
 * @throws ReflectionException
 */
function createConfigFromFilename(string $filename) : ?array
{
    $filename = findGeneratorFile($filename);

    return getReflector(ReflectionClass::getClassFromFile($filename))?->getConfig();
}

/**
 * Devuelve la ruta del archivo `generator.yaml` subiendo de directorio a partir de la ruta inicial.
 * Si no lo encuentra lanza una excepción.
 *
 * @param string $path
 *
 * @return string
 */
function findGeneratorFile(string $path) : string
{
    if (is_file($path))
    {
        $path = dirname($path);
    }
    elseif (!is_dir($path))
    {
        UnexpectedValueException::isTrue(ReflectionClass::isDefined($path), 'Elemento `{0}` desconocido');
        $path = dirname(ReflectionClass::fromClassname($path)->getFilename());
    }
    $_file = NULL;
    while ($path !== '/')
    {
        $_tmp = "$path/generator.yaml";
        if (is_file($_tmp))
        {
            $_file = $_tmp;
            break;
        }
        $path = dirname($path);
    }
    NotFound::notIsNull($_file, 'No se encontró el archivo `generator.yaml`');

    return $_file;
}

/**
 * Devuelve un generador que permite generar el código con la configuración especificada.
 *
 * @param array  $item      Configuración del elemento a generar.
 * @param string $type      Tipo de elemento a generar.
 * @param array  $generator Configuración del generador.
 *
 * @return Generator
 */
function getGenerator(array $item, string $type = '', array $generator = []) : Generator
{
    $_generator = Generator::fromArray($generator);
    $_type      = $type ?: $item['$type'] ?? NULL;
    if (!$_type && preg_match('/^([TI])[A-Z]/', $item['name'] ?? '', $_matches))
    {
        // Intentamos usar el sígil para detectar el tipo.
        $_type = $_matches[1] === 'T' ? 'trait' : 'interface';
    }
    switch ($_type)
    {
        case 'enum':
            $_generator->addEnum($item);
            break;
        case 'interface':
            $_generator->addInterface($item);
            break;
        case 'trait':
            $_generator->addTrait($item);
            break;
        default:
            $_generator->addClass($item);
            break;
    }

    return $_generator;
}

/**
 * Devuelve el contenido actual del archivo `generator.yaml`.
 *
 * @param string $filename Ruta inicial donde buscar el archivo.
 *
 * @return array
 */
function getGeneratorContent(string $filename) : array
{
    if (!str_ends_with($filename, '.yaml') || !is_file($filename))
    {
        $filename = findGeneratorFile($filename);
    }
    $_config = Yaml::new()->loadYaml($filename) ?? [];
    if (!empty($_config['templates']))
    {
        resolveTpl($_config, $_config['templates']);
    }
    foreach ($_config as $_key => &$_items)
    {
        if ($_key !== 'traits' && is_array($_items))
        {
            foreach ($_items as &$_item)
            {
                if (!empty($_item['traits']))
                {
                    foreach ($_item['traits'] as $_name => $_trait)
                    {
                        if (is_int($_name))
                        {
                            $_name = is_string($_trait)
                                ? $_trait
                                : $_trait['name'];
                        }
                        $_name = Formatter::extractName($_name);
                        if ($_name[0] === 'T')
                        {
                            $_name  = preg_replace('|^T|', 'I', Formatter::extractName($_name));
                            $_iface = __DIR__ . "/../src/$_name.php";
                            if (is_file($_iface))
                            {
                                $_item['implements'][] = $_name;
                            }
                        }
                    }
                }
            }
        }
    }

    return $_config;
}

/**
 * Devuelve el reflector para la clase, enum, trait o interfaz especificada.
 *
 * @param string $fqcn Nombre del elemento a usar para devolver el reflector.
 *
 * @return ReflectionClass|null
 */
function getReflector(string $fqcn) : ?ReflectionClass
{
    if (ReflectionClass::isDefined($fqcn))
    {
        printf("Procesando %s\n", $fqcn);
        $_reflector            = ReflectionClass::fromClassname($fqcn);
        $_reflector->addSlash  = TRUE;
        $_reflector->inherited = FALSE;
    }
    else
    {
        $_reflector = NULL;
        printf("No se encontró la clase `%s`\n", $fqcn);
    }

    return $_reflector;
}

/**
 * Resuelve las plantillas presentes en la configuración.
 *
 * @param array $config    Configuración del generador.
 * @param array $templates Listado de plantillas.
 *
 * @return void
 */
function resolveTpl(array &$config, array $templates) : void
{
    if (isset($config['$template']))
    {
        $_params = $config['$template'];
        $_tpl    = $templates[ $_params['id'] ] ?? NULL;
        Assert::notEmpty($_tpl);
        unset($config['$template']);
        $_replaces = [];
        foreach ($_params as $_k => $_v)
        {
            $_replaces["{{$_k}}"] = $_v;
        }
        $config = array_replace_recursive(yaml_parse(strtr($_tpl, $_replaces)), $config);
    }
    foreach ($config as &$_v)
    {
        if (is_array($_v))
        {
            resolveTpl($_v, $templates);
        }
    }
}

/**
 * Ordena la configuración del generador según el algoritmo de la clase `ScalarFirst`.
 *
 * @param array $config Configuración a ordenar.
 *
 * @return array
 */
function sortConfiguration(array &$config) : array
{
    return ScalarFirst::new($config['sort'] ?? [ 'ignore' => [ 'params', 'tags' ] ])->sort($config);
}