#!/usr/bin/env php
<?php

/**
 * Construye el archivo `generator.yaml` analizando las clases
 * de un directorio que todavía no lo tenga creado.
 *
 * @internal
 */

use jf\Base\File\Yaml;
use jf\generator\config\parser\php\Config;
use jf\php\generator\Generator;
use jf\Reflector\ReflectionClass;
use jf\tests\Runner;

require_once __DIR__ . '/../../autoload.php';

$rootdir = dirname(__DIR__);
$genfile = $rootdir . '/generator.yaml';
$yaml    = Yaml::create();
$config  = $yaml->loadYaml($genfile);
$ns      = 'jf\php\generator';
$names   = $argc > 1 ? [ $argv[1] ] : array_map(fn($file) => basename($file, '.php'), glob($rootdir . '/src/collection/*.php'));
sort($names, SORT_FLAG_CASE | SORT_STRING);
foreach ($names as $arg)
{
    $class = ReflectionClass::getClassFromFile($arg);
    if (ReflectionClass::isDefined($class))
    {
        printf("Procesando %s\n", $class);
        $definition = [];
        try
        {
            $reflector            = ReflectionClass::fromClassname($class);
            $reflector->inherited = FALSE;
            $definition           = $reflector->getConfig();
            $file                 = new Generator();
            switch ($definition['$type'])
            {
                case 'enum':
                    $key = 'enumerations';
                    $file->addEnum($definition);
                    break;
                case 'interface':
                    $key = 'interfaces';
                    $file->addInterface($definition);
                    break;
                case 'trait':
                    $key = 'traits';
                    $file->addTrait($definition);
                    break;
                default:
                    $key = 'classes';
                    $file->addClass($definition);
                    break;
            }
            if (($definition['namespace'] ?? NULL) === $ns)
            {
                unset($definition['namespace']);
            }
            $config[ $key ][ $arg ] = $definition;
            echo $file . PHP_EOL;
            (new Runner())->assertStringEquals($reflector->getSourceFile(), (string) $file);
        }
        catch (Throwable $e)
        {
            printf(
                "El contenido generado es diferente: %s\n\nResultado:\n%s\nConfiguración:\n%s\n",
                $class,
                $e->getMessage(),
                Yaml::create()->toYaml($definition)
            );
            die(-1);
        }
    }
    else
    {
        printf("No se encontró la clase `%s`\n", $arg);
    }
}

if (!isset($config['namespace']))
{
    $config['namespace'] = $ns;
}
Config::sortConfig($config);
echo $yaml->toYaml($config);
// $yaml->saveYaml($genfile, $config);
