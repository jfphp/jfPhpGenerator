# jf/php-generator

Generador de código PHP.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/php-generator` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/php-generator
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete       | Versión |
|:--------------|:--------|
| jf/assert     | ^3.1    |
| jf/base       | ^4.0    |
| jf/collection | ^2.0    |
| psr/log       | ^3.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfPhpGenerator.git
cd jfPhpGenerator
composer install
```

## Archivos disponibles

### Clases

| Nombre                                                                      | Descripción                                                                                                                       |
|:----------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------|
| [jf\php\generator\ABase](src/ABase.php)                                     | Clase base para el resto de clases del proyecto.                                                                                  |
| [jf\php\generator\Attribute](src/Attribute.php)                             | Representa un atributo de PHP.                                                                                                    |
| [jf\php\generator\ClassObject](src/ClassObject.php)                         | Genera una clase.                                                                                                                 |
| [jf\php\generator\Classname](src/Classname.php)                             | Renderiza cada uno de los items que representan un nombre de clase.                                                               |
| [jf\php\generator\EnumObject](src/EnumObject.php)                           | Genera una enumeración.                                                                                                           |
| [jf\php\generator\Exception](src/Exception.php)                             | Excepciones lanzadas por la aplicación.                                                                                           |
| [jf\php\generator\File](src/File.php)                                       | Clase para manipular archivos.                                                                                                    |
| [jf\php\generator\Formatter](src/Formatter.php)                             | Clase con utilidades para manipular textos.                                                                                       |
| [jf\php\generator\FunctionItem](src/FunctionItem.php)                       | Genera una función de PHP.                                                                                                        |
| [jf\php\generator\Generator](src/Generator.php)                             | Clase para manipular archivos.                                                                                                    |
| [jf\php\generator\Helpers](src/Helpers.php)                                 | Clase con métodos de ayuda para realizar diversas operaciones.                                                                    |
| [jf\php\generator\InterfaceObject](src/InterfaceObject.php)                 | Genera una interfaz.                                                                                                              |
| [jf\php\generator\Method](src/Method.php)                                   | Método de una clase.                                                                                                              |
| [jf\php\generator\Param](src/Param.php)                                     | Parámetro de un método.                                                                                                           |
| [jf\php\generator\Property](src/Property.php)                               | Propiedad de una clase.                                                                                                           |
| [jf\php\generator\Tag](src/Tag.php)                                         | Representa una etiqueta para documentar un elemento.                                                                              |
| [jf\php\generator\Tags](src/Tags.php)                                       | Representa un grupo de etiquetas para documentar un elemento.                                                                     |
| [jf\php\generator\TraitItem](src/TraitItem.php)                             | Renderiza cada uno de los items que representan un trait y que generan las sentencias `use XXXX;` en la definición de las clases. |
| [jf\php\generator\TraitMethod](src/TraitMethod.php)                         | Representa un método de un trait que se usa y que debe ser renombrado para evitar una colisión.                                   |
| [jf\php\generator\TraitObject](src/TraitObject.php)                         | Genera un trait.                                                                                                                  |
| [jf\php\generator\Variable](src/Variable.php)                               | Representa una variable PHP.                                                                                                      |
| [jf\php\generator\collection\ACollection](src/collection/ACollection.php)   | Clase base de las colecciones.                                                                                                    |
| [jf\php\generator\collection\Attributes](src/collection/Attributes.php)     | Colección de atributos.                                                                                                           |
| [jf\php\generator\collection\Classes](src/collection/Classes.php)           | Colección de clases.                                                                                                              |
| [jf\php\generator\collection\Classnames](src/collection/Classnames.php)     | Colección de nombres de clases.                                                                                                   |
| [jf\php\generator\collection\Functions](src/collection/Functions.php)       | Colección de funciones.                                                                                                           |
| [jf\php\generator\collection\Methods](src/collection/Methods.php)           | Colección de métodos de una clase.                                                                                                |
| [jf\php\generator\collection\Params](src/collection/Params.php)             | Colección de parámetros de un método o función.                                                                                   |
| [jf\php\generator\collection\Properties](src/collection/Properties.php)     | Colección de propiedades de una clase.                                                                                            |
| [jf\php\generator\collection\TraitItems](src/collection/TraitItems.php)     | Colección de traits que se agregan a un elemento.                                                                                 |
| [jf\php\generator\collection\TraitMethods](src/collection/TraitMethods.php) | Colección de traits que se agregan a una clases o trait.                                                                          |

### Enumeraciones

| Nombre                                            | Descripción                                         |
|:--------------------------------------------------|:----------------------------------------------------|
| [jf\php\generator\ObjectType](src/ObjectType.php) | Enumera los tipos de elementos que se pueden crear. |

### Interfaces

| Nombre                                              | Descripción                                                                                                              |
|:----------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------|
| [jf\php\generator\IAttributes](src/IAttributes.php) | Interfaz para las clases que requieren gestionar los atributos.                                                          |
| [jf\php\generator\IExtends](src/IExtends.php)       | Interfaz para las clases que requieren gestionar los elementos de los que extiende la clase o interfaz.                  |
| [jf\php\generator\IGenerator](src/IGenerator.php)   | Interfaz para los elementos que generan código tales como clases, enumeraciones, interfaces y traits.                    |
| [jf\php\generator\IImplements](src/IImplements.php) | Interfaz para las clases que requieren gestionar los elementos que implementa la clase.                                  |
| [jf\php\generator\IMethods](src/IMethods.php)       | Interfaz para las clases que requieren gestionar los métodos.                                                            |
| [jf\php\generator\IParams](src/IParams.php)         | Interfaz para las clases que requieren gestionar los parámetros del método.                                              |
| [jf\php\generator\IProperties](src/IProperties.php) | Interfaz para las clases que requieren gestionar las propiedades.                                                        |
| [jf\php\generator\ITraits](src/ITraits.php)         | Interfaz para las clases que requieren gestionar los traits.                                                             |
| [jf\php\generator\IUses](src/IUses.php)             | Interfaz para aquellas clases cuyo contenido que pueden hacer referencia a otros elementos en otros espacios de nombres. |

### Traits

| Nombre                                                        | Descripción                                                                                                  |
|:--------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------|
| [jf\php\generator\TAttributes](src/TAttributes.php)           | Gestiona los atributos del elemento.                                                                         |
| [jf\php\generator\TCollectionItem](src/TCollectionItem.php)   | Trait para los elementos que se gestionan mediante las colecciones.                                          |
| [jf\php\generator\TDescription](src/TDescription.php)         | Gestiona la descripción del elemento.                                                                        |
| [jf\php\generator\TDocDescription](src/TDocDescription.php)   | Documenta el elemento agregando su descripción.                                                              |
| [jf\php\generator\TDocTags](src/TDocTags.php)                 | Gestiona las etiquetas PHPDoc del elemento.                                                                  |
| [jf\php\generator\TDocType](src/TDocType.php)                 | Documenta el tipo de datos de un elemento.                                                                   |
| [jf\php\generator\TExtends](src/TExtends.php)                 | Gestiona los elementos de los que extiende la clase o interfaz del elemento.                                 |
| [jf\php\generator\TFile](src/TFile.php)                       | Trait para manipular archivos.                                                                               |
| [jf\php\generator\TGenerator](src/TGenerator.php)             | Trait para las clases que generan código de elementos tales como clases, enumeraciones, interfaces y traits. |
| [jf\php\generator\TImplements](src/TImplements.php)           | Gestiona los elementos que implementa la clase del elemento.                                                 |
| [jf\php\generator\TMethods](src/TMethods.php)                 | Gestiona los métodos del elemento.                                                                           |
| [jf\php\generator\TModifiers](src/TModifiers.php)             | Modificadores de un elemento.                                                                                |
| [jf\php\generator\TName](src/TName.php)                       | Gestiona un nombre que puede pertenecer a un espacio de nombres y tener un alias.                            |
| [jf\php\generator\TParams](src/TParams.php)                   | Gestiona los parámetros del método del elemento.                                                             |
| [jf\php\generator\TProperties](src/TProperties.php)           | Gestiona las propiedades del elemento.                                                                       |
| [jf\php\generator\TTags](src/TTags.php)                       | Gestiona las etiquetas PHPDoc del elemento.                                                                  |
| [jf\php\generator\TTraits](src/TTraits.php)                   | Gestiona los traits del elemento.                                                                            |
| [jf\php\generator\TTranslations](src/TTranslations.php)       | Trait para gestiona las traducciones de los textos.                                                          |
| [jf\php\generator\TType](src/TType.php)                       | Gestiona el tipo de una variable, propieadad o parámetro.                                                    |
| [jf\php\generator\TUses](src/TUses.php)                       | Gestiona los elementos a importar.                                                                           |
| [jf\php\generator\TValue](src/TValue.php)                     | Trait que gestiona el valor del elemento.                                                                    |
| [jf\php\generator\collection\TType](src/collection/TType.php) | Trait para agregar un tipo por defecto en los elementos de algunas colecciones.                              |

## Scripts

### scripts/_common.php

Funciones comunes para otros scripts.

Ver archivo  [scripts/_common.php](scripts/_common.php).

#### scripts/add-to-generator.php

Agrega la configuración al archivo `generator.yaml`.

Ver archivo  [scripts/add-to-generator.php](scripts/add-to-generator.php).

##### scripts/build-config.php

Construye el archivo `generator.yaml` analizando las clases de un directorio que todavía no lo tenga creado.

Ver archivo  [scripts/build-config.php](scripts/build-config.php).

###### scripts/coverage

Muestra por pantalla la cobertura de aquellas clases que no llegan al 100% usando la opción -p del visor.

Ver archivo  [scripts/coverage](scripts/coverage).

####### scripts/create-test

Genera los tests básicos a partir del análisis de una clase.

Ver archivo  [scripts/create-test](scripts/create-test).

######## scripts/generate

Genera el código fuente de todo los archivos del repositorio dentro de directorio `src`.

Ver archivo  [scripts/generate](scripts/generate).

######### scripts/tests

Realiza pruebas exhaustivas cubriendo el 100% del código.
La prueba más completa es la generación del código de todos los archivos del repositorio y comparando el resultado
con el contenido del archivo actual.

Ver archivo  [scripts/tests](scripts/tests).
