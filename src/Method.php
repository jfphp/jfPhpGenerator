<?php

namespace jf\php\generator;

/**
 * Método de una clase.
 */
class Method extends FunctionItem
{
    use TModifiers;

    /**
     * @inheritdoc
     */
    public function buildCode() : array
    {
        $_abstract = $this->abstract;
        $_body     = $this->body;
        $_code     = parent::buildCode();
        $_code[0]  = ($this->scope ?: 'public') . ' ' . $_code[0];
        if ($_abstract === TRUE)
        {
            $_code[0] = 'abstract ' . $_code[0];
        }
        elseif ($this->final)
        {
            $_code[0] = 'final ' . $_code[0];
        }
        if ($_abstract === TRUE || ($_abstract === NULL && $_body === NULL))
        {
            // `FunctionItem::buildCode` pudo haber inferido un `body` de acuerdo al `type`.
            $_code = [ $_code[0] . ';' ];
        }

        return $_code;
    }

    /**
     * @inheritdoc
     */
    public function renderTags() : array
    {
        if ($this->override)
        {
            $_tags = $this->tags;
            array_unshift($_tags, new Tags([ 'tags' => [ [ 'name' => 'inheritdoc' ] ] ]));
            $_tags = $this->_renderTags($_tags);
        }
        else
        {
            $_tags = parent::renderTags();
        }

        return $_tags;
    }

    /**
     * Asigna el valor para indicar que el método es abstracto.
     *
     * @param bool|NULL $abstract Valor a asignar.
     *
     * @return static
     */
    public function setAbstract(?bool $abstract) : static
    {
        $this->abstract = $abstract;
        if ($abstract)
        {
            $this->setBody('');
        }

        return $this;
    }

    /**
     * Asigna el nombre del método.
     *
     * @param string $name Valor a asignar.
     *
     * @return static
     */
    public function setName(string $name) : static
    {
        $this->name = $name;
        if ($name && $name[0] === '_' && ($name[1] ?? NULL) !== '_' && !$this->scope)
        {
            $this->scope = 'protected';
        }

        return $this;
    }
}
