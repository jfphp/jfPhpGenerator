<?php

namespace jf\php\generator;

/**
 * Representa una variable PHP.
 */
class Variable extends ABase implements IGenerator, IUses
{
    use TDocDescription;
    use TDocTags
    {
        TDocTags::renderTags as protected _renderTags;
    }
    use TDocType
    {
        TDocType::renderValue as protected _renderValue;
    }

    /**
     * Nombre de la variable.
     *
     * @var string
     */
    public string $name = '';

    /**
     * Prefijo a anteponer en el nombre de la variable.
     *
     * @var string
     */
    public string $prefix = '$';

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_code = array_filter([ ...$this->renderDoc(), implode(' ', $this->buildCode()) ]);

        return $_code
            ? implode(PHP_EOL, $_code) . ';'
            : '';
    }

    /**
     * Genera los elementos para construir la sentencia PHP para definir la variable.
     *
     * @return string[]
     */
    public function buildCode() : array
    {
        $_code = [];
        $_name = $this->name;
        if ($_name)
        {
            $_code[] = $this->prefix . $_name;
            $_value  = $this->renderValue();
            if ($_value)
            {
                $_code[] = $_value;
            }
        }

        return $_code;
    }

    /**
     * @see TUses::getUses()
     */
    public function getUses() : array
    {
        return [ ...$this->uses, ...static::extractUses($this) ];
    }

    /**
     * @see TDocTags::renderTags()
     */
    public function renderTags() : array
    {
        $_tags = $this->tags;
        if (!$this->copydoc)
        {
            $_mtags = [];
            $this->addDocType($_mtags);
            $this->addDocDescription($_mtags);
            foreach ($_mtags as $_name => $_tag)
            {
                unset($_tags[ $_name ]);
                array_unshift($_tags, $_tag);
            }
        }

        return $this->_renderTags($_tags);
    }

    /**
     * @see TDocType::renderValue()
     */
    public function renderValue() : string
    {
        $_value = $this->_renderValue();
        if (!$_value && !$this->null && $this->type)
        {
            $_value = match (current($this->type))
            {
                'float'  => '= 0.0',
                'int'    => '= 0',
                'string' => "= ''",
                default  => ''
            };
        }

        return $_value;
    }
}
