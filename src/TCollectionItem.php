<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Trait para los elementos que se gestionan mediante las colecciones.
 *
 * @mixin IItem
 */
trait TCollectionItem
{
    /**
     * Nombre del elemento.
     *
     * @var string
     */
    public string $name = '';

    /**
     * @inheritdoc
     */
    public function getCollectionKey() : string
    {
        return $this->name ?: spl_object_hash($this);
    }
}
