<?php

namespace jf\php\generator;

use jf\php\generator\collection\Classnames;

/**
 * Interfaz para las clases que requieren gestionar los elementos que implementa la clase.
 */
interface IImplements
{
    /**
     * Agrega a la colección los elementos que implementa la clase.
     *
     * @param array|string $implements Listado de los elementos que implementa la clase que serán agregados.
     *
     * @return static
     */
    public function addImplements(array|string $implements) : static;

    /**
     * Construye el código necesario para cada uno de los elementos que implementa la clase.
     *
     * @return array
     */
    public function buildImplements() : array;

    /**
     * Devuelve la colección de los elementos que implementa la clase.
     *
     * @return Classnames
     */
    public function getImplements() : Classnames;
}
