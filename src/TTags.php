<?php

namespace jf\php\generator;

/**
 * Gestiona las etiquetas PHPDoc del elemento.
 */
trait TTags
{
    /**
     * Elemento del cual se debe copiar la documentación.
     *
     * @var string
     */
    public string $copydoc = '';

    /**
     * Etiquetas del elemento.
     *
     * @var Tag[]|Tags[]
     */
    public array $tags = [];

    /**
     * Agrega las etiquetas a la lista.
     *
     * @param Tag[]|Tags[]|array[] $tags Listado de etiquetas a agregar.
     *
     * @return static
     */
    public function addTags(array $tags) : static
    {
        foreach ($tags as $_tag)
        {
            if (is_array($_tag))
            {
                $_tag = array_is_list($_tag)
                    ? new Tags([ 'tags' => $_tag ])
                    : new Tag($_tag);
            }
            if ($_tag === NULL || $_tag instanceof Tag || $_tag instanceof Tags)
            {
                $this->tags[] = $_tag;
            }
        }

        return $this;
    }

    /**
     * Renderiza las etiquetas del elemento.
     *
     * @param array|NULL $tags Etiquetas a renderizar.
     *
     * @return string[]
     */
    public function renderTags(?array $tags = NULL) : array
    {
        $_tags = [];
        if ($tags === NULL)
        {
            $tags = $this->tags;
        }
        if ($this->copydoc)
        {
            $tags[] = new Tags([ 'tags' => [ [ 'name' => 'see', 'value' => $this->copydoc ] ] ]);
        }
        if ($tags)
        {
            $_last = NULL;
            foreach ($tags as $_tag)
            {
                if ($_tag instanceof Tag)
                {
                    if ($_last instanceof Tags)
                    {
                        $_tags[] = '';
                    }
                    $_tags[] = (string) $_tag;
                }
                elseif ($_tag instanceof Tags)
                {
                    if ($_tags)
                    {
                        $_tags[] = '';
                    }
                    $_tags = [ ...$_tags, ...explode(PHP_EOL, $_tag) ];
                }
                else
                {
                    $_tags[] = '';
                }
                $_last = $_tag;
            }
        }

        return $_tags;
    }
}
