<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Renderiza cada uno de los items que representan un nombre de clase.
 */
class Classname extends ABase implements IItem, IUses
{
    use TName
    {
        TName::setName as private _setName;
    }

    /**
     * @inheritdoc
     */
    public function getUses() : array
    {
        return $this->getNamespaceUses();
    }

    /**
     * @see TName::setName()
     */
    public function setName(string $name) : static
    {
        if (str_contains($name, '@'))
        {
            [ $name, $this->alias ] = explode('@', $name);
        }

        return $this->_setName($name);
    }
}
