<?php

namespace jf\php\generator;

use jf\php\generator\collection\Attributes;

/**
 * Interfaz para las clases que requieren gestionar los atributos.
 */
interface IAttributes
{
    /**
     * Agrega a la colección los atributos.
     *
     * @param array|string $attributes Listado de los atributos que serán agregados.
     *
     * @return static
     */
    public function addAttributes(array|string $attributes) : static;

    /**
     * Construye el código necesario para cada uno de los atributos.
     *
     * @return array
     */
    public function buildAttributes() : array;

    /**
     * Devuelve la colección de los atributos.
     *
     * @return Attributes
     */
    public function getAttributes() : Attributes;
}
