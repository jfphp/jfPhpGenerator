<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Parámetro de un método.
 */
class Param extends ABase implements IAttributes, IItem, IUses
{
    use TAttributes;
    use TCollectionItem;
    use TDescription;
    use TDocType
    {
        TDocType::getType as private _getType;
    }

    /**
     * @see Property::$readonly
     */
    public bool $readonly = FALSE;

    /**
     * Indica que el parámetro se pasa por referencia.
     *
     * @var bool
     */
    public bool $reference = FALSE;

    /**
     * @see Property::$scope
     */
    public ?string $scope = '';

    /**
     * Indica que el parámetro es variable.
     *
     * @var bool
     */
    public bool $variadic = FALSE;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return implode(' ', $this->buildCode());
    }

    /**
     * Genera los elementos para construir la sentencia PHP para definir el parámetro.
     *
     * @return string[]
     */
    public function buildCode() : array
    {
        $_name = $this->name;
        if ($_name)
        {
            if (!$this->type && $this->variadic)
            {
                $this->addType('mixed');
            }
            $_code = $this->buildAttributes();
            if ($this->scope)
            {
                $_code[] = $this->scope;
                if ($this->readonly)
                {
                    $_code[] = 'readonly';
                }
            }
            $_code[] = $this->getPhpType();
            $_code[] = match (TRUE)
            {
                $this->variadic  => '...$' . $_name,
                $this->reference => '&$' . $_name,
                default          => '$' . $_name
            };
            $_code[] = $this->renderValue();
            $_code   = array_values(array_filter($_code));
        }
        else
        {
            $_code = [];
        }

        return $_code;
    }

    /**
     * @inheritdoc
     */
    public function getType() : ?array
    {
        if (!$this->type && $this->variadic)
        {
            $this->addType('mixed');
        }

        return $this->_getType();
    }

    /**
     * @see TDocTags::renderTags()
     */
    public function renderTags() : array
    {
        return $this->name
            ? [
                new Tag(
                    [
                        'description' => $this->description,
                        'name'        => 'param',
                        'value'       => $this->getDocType(),
                        'variable'    => $this->variadic
                            ? '...' . $this->name
                            : $this->name
                    ]
                )
            ]
            : [];
    }
}
