<?php

namespace jf\php\generator;

/**
 * Clase para manipular archivos.
 */
class File extends ABase
{
    use TFile;

    /**
     * Crea una instancia a partir del nombre de un archivo.
     *
     * @param string $filename Ruta del archivo.
     * @param array  $config   Configuración adicional a aplicar a la instancia.
     *
     * @return static
     */
    public static function fromFilename(string $filename, array $config = []) : static
    {
        $_file = new static([ 'filename' => $filename ]);
        if ($config)
        {
            $_file->assign($config);
        }

        return $_file;
    }
}
