<?php

namespace jf\php\generator;

use Composer\Autoload\ClassLoader;
use jf\core\Autoloader;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Clase con métodos de ayuda para realizar diversas operaciones.
 */
class Helpers
{
    /**
     * Caché de archivos cargados para analizar.
     *
     * @var array<string,string>
     */
    private static array $_cache = [];

    /**
     * Indica si se usan forks al momento de verificar si un FQCN existe para evitar
     * errores que aborten la ejecución.
     *
     * @var bool
     */
    public static bool $forks = FALSE;

    /**
     * Determina si el elemento existe y es una clase, enum, interfaz o trait que se
     * pueda analizar.
     *
     * Para evitar un error fatal si no se puede analizar el archivo se realiza un fork.
     *
     * @param string $fqcn FQCN del elemento a verificar.
     *
     * @return bool
     */
    public static function exists(string $fqcn) : bool
    {
        $_cache = &self::$_cache;
        if (!array_key_exists($fqcn, $_cache))
        {
            $_pid = static::$forks ? pcntl_fork() : -1;
            switch ($_pid)
            {
                case -1:
                    // No se pudo crear el fork
                    $_cache[ $fqcn ] = class_exists($fqcn) || enum_exists($fqcn) || interface_exists($fqcn) || trait_exists($fqcn)
                        ? 0
                        : 1;
                    break;

                case 0:
                    // Proceso hijo
                    $_src = [];
                    foreach (spl_autoload_functions() as $_fn)
                    {
                        if (is_array($_fn))
                        {
                            $_autoloader = '';
                            $_loader     = $_fn[0];
                            if ($_loader instanceof ClassLoader)
                            {
                                $_autoloader = (new ReflectionProperty($_loader, 'vendorDir'))->getValue($_loader) . '/autoload.php';
                            }
                            elseif ($_loader instanceof Autoloader)
                            {
                                $_reflector  = new ReflectionClass($_loader);
                                $_autoloader = $_reflector->getFileName();
                            }
                            if ($_autoloader && is_file($_autoloader))
                            {
                                $_src[] = "require_once '$_autoloader';";
                            }
                        }
                    }
                    $_src[] = "echo (class_exists('$fqcn') || enum_exists('$fqcn') || interface_exists('$fqcn') || trait_exists('$fqcn') ? 0 : 1);";
                    $_cmd   = escapeshellarg(implode(PHP_EOL, $_src));
                    $_code  = shell_exec("php -r $_cmd 2>/dev/null");
                    die($_code === NULL || $_code === FALSE || $_code === '' ? -1 : (int) $_code);

                default:
                    // Proceso padre
                    pcntl_waitpid($_pid, $_status);
                    if (pcntl_wifexited($_status))
                    {
                        $_status         = pcntl_wexitstatus($_status);
                        $_cache[ $fqcn ] = $_status === FALSE ? 1 : $_status;
                    }
                    break;
            }
        }

        return ($_cache[ $fqcn ] ?? NULL) === 0;
    }

    /**
     * Extrae las etiquetas `throws` de un método de una clase.
     * Su principal uso es agregar dichas etiquetas a otro método que lo llama.
     *
     * @param string $classname Clase que tiene el método.
     * @param string $method    Método a analizar.
     *
     * @return array<array{name:string,value:string}>
     *
     * @throws Exception
     * @throws ReflectionException
     */
    public static function extractMethodThrows(string $classname, string $method) : array
    {
        Exception::notEmpty($classname);
        Exception::notEmpty($method);
        Exception::isTrue(class_exists($classname) || trait_exists($classname), _('La clase `{0}` no existe'), $classname);
        Exception::methodExists($classname, $method);
        $_ref    = new ReflectionClass($classname);
        $_doc    = $_ref->getMethod($method)->getDocComment();
        $_tags   = static::getCommentTags($_doc)['@throws'] ?? NULL;
        $_throws = [];
        if ($_tags)
        {
            $_filename = $_ref->getFileName();
            foreach ($_tags as $_throw)
            {
                if ($_throw[0] === '\\')
                {
                    $_use   = $_throw;
                    $_throw = Formatter::extractName($_throw);
                }
                else
                {
                    if (str_contains($_throw, '\\'))
                    {
                        $_throw = Formatter::extractName($_throw);
                    }
                    $_use = static::resolve($_filename, $_throw);
                }
                $_throws[ $_use ] = [
                    'name'  => 'throws',
                    'value' => $_use
                ];
            }
            ksort($_throws);
            $_throws = array_values($_throws);
        }

        return $_throws;
    }

    /**
     * Extrae las etiquetas de un comentario PHPDoc para su posterior uso y análisis.
     *
     * @param string $comment Comentario PHPDoc a analizar.
     *
     * @return array<string,string[]>
     */
    public static function getCommentTags(string $comment) : array
    {
        $_tags = [];
        if ($comment)
        {
            $_description = [];
            $_tag         = '';
            foreach (explode(PHP_EOL, trim(substr($comment, 4, -2))) as $_line)
            {
                $_line = preg_replace('/^\*(\s+|$)/', '', trim($_line));
                if ($_line)
                {
                    if ($_line[0] === '@')
                    {
                        $_parts           = preg_split('/\s+/', $_line, 2);
                        $_tag             = $_parts[0];
                        $_tags[ $_tag ][] = $_parts[1] ?? '';
                    }
                    elseif ($_tag)
                    {
                        $_tags[ $_tag ][ count($_tags[ $_tag ]) - 1 ] .= PHP_EOL . $_line;

                        $_tag = '';
                    }
                    else
                    {
                        $_description[] = $_line;
                    }
                }
                else
                {
                    if (!$_tags)
                    {
                        $_description[] = $_line;
                    }
                    $_tag = '';
                }
            }
            while ($_description && empty(end($_description)))
            {
                array_pop($_description);
            }
            if ($_description)
            {
                $_tags['@description'] = $_description;
            }
            ksort($_tags);
        }

        return $_tags;
    }

    /**
     * Resuelve el nombre completo de una clase a partir de las cláusulas `use`
     * existentes en el archivo especificado.
     *
     * @param string $filename Ruta del archivo con las cláusulas `use` a analizar.
     * @param string $name     Nombre de la clase a resolver.
     *
     * @return string
     */
    public static function resolve(string $filename, string $name) : string
    {
        $filename = realpath($filename);
        if ($filename)
        {
            $_content = self::$_cache[ $filename ] ?? (self::$_cache[ $filename ] = file_get_contents($filename));
            if (preg_match(sprintf('/^use\s+(.*%s);$/m', preg_quote($name, '/')), $_content, $_use))
            {
                $_use   = $_use[1];
                $_index = strpos($_use, ' as ');
                if ($_index !== FALSE)
                {
                    $_use = rtrim(substr($_use, 0, $_index));
                }
                $name = '\\' . $_use;
            }
        }

        return $name;
    }
}
