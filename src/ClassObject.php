<?php

namespace jf\php\generator;

/**
 * Genera una clase.
 */
class ClassObject extends ABase implements IAttributes, IExtends, IGenerator, IImplements, IMethods, IProperties, ITraits, IUses
{
    use TAttributes;
    use TExtends;
    use TGenerator;
    use TImplements;
    use TProperties;
    use TTraits;
    use TMethods
    {
        TMethods::_modifyMethod as private __modifyMethod;
    }

    /**
     * Indica si la clase es abstracta.
     *
     * @var bool
     */
    public bool $abstract = FALSE;

    /**
     * Argumentos a usar cuando es una clase anónima.
     *
     * @var array
     */
    public array $arguments = [];

    /**
     * Indica si la clase puede extenderse.
     *
     * @var bool|NULL
     */
    public ?bool $final = NULL;

    /**
     * @inheritdoc
     */
    public function buildCode() : array
    {
        $_body = [];
        $_code = [];
        $_args = $this->arguments;
        if (!$_args || $this->description)
        {
            $this->appendItems($_code, 'renderDoc');
        }
        $this->appendItems($_code, 'buildAttributes');
        $this->appendItems($_body, 'buildTraits');
        $this->appendItems($_body, 'buildProperties', PHP_EOL . PHP_EOL);
        $this->appendItems($_body, 'buildMethods', PHP_EOL . PHP_EOL);
        $_def = [];
        if ($this->abstract)
        {
            $_def[] = 'abstract';
        }
        elseif ($this->final)
        {
            $_def[] = 'final';
        }
        if ($this->name)
        {
            $_def[] = 'class ' . $this->name;
        }
        elseif ($_args)
        {
            $_def[] = sprintf('class(%s)', implode(', ', $_args));
        }
        else
        {
            $_def[] = 'class';
        }
        $this->appendItems($_def, 'renderExtends');
        $this->appendItems($_def, 'renderImplements');
        $_code[] = implode(' ', $_def);
        $_code[] = '{';
        if ($_body)
        {
            $_code[] = Formatter::indent(implode(PHP_EOL . PHP_EOL, $_body));
        }
        $_code[] = '}';

        return $_code;
    }

    /**
     * @see TMethods::_modifyMethod()
     */
    protected function _modifyMethod(Method $method) : bool
    {
        if ($this->abstract && $method->body === NULL && $method->abstract === NULL)
        {
            $method->abstract = TRUE;
        }
        $this->__modifyMethod($method);

        return TRUE;
    }
}
