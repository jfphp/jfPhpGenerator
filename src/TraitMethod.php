<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Representa un método de un trait que se usa y que debe ser renombrado para
 * evitar una colisión.
 */
class TraitMethod extends ABase implements IItem
{
    use TName;

    /**
     * Nombre del trait que contiene el método que será reemplazado por el actual.
     *
     * @var string
     */
    public string $insteadof = '';

    /**
     * Nuevo ámbito del método.
     *
     * @var string
     */
    public string $scope = '';

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_code = [];
        $_name = $this->name;
        if ($_name)
        {
            $_ns     = $this->getNamespace();
            $_code[] = $_ns
                ? "$_ns::$_name"
                : $_name;
            if ($this->insteadof)
            {
                $_code[] = 'insteadof';
                $_code[] = $this->insteadof . ';';
            }
            else
            {
                $_alias  = $this->alias ?: '_' . $_name;
                $_code[] = 'as';
                if ($this->scope)
                {
                    $_code[] = $this->scope;
                }
                elseif ($_alias[0] === '_')
                {
                    $_code[] = 'private';
                }
                $_code[] = "$_alias;";
            }
        }

        return implode(' ', $_code);
    }
}
