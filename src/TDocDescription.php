<?php

namespace jf\php\generator;

/**
 * Documenta el elemento agregando su descripción.
 */
trait TDocDescription
{
    use TDescription;

    /**
     * Agrega la descripción al listadao de etiquetas si no existe previamente.
     *
     * @param array $tags Listado de etiquetas del elemento.
     *
     * @return Tags|NULL
     */
    public function addDocDescription(array &$tags) : ?Tags
    {
        $_description = $this->renderDescription();

        return $_description
            ? ($tags['description'] = new Tags([ 'tags' => [ [ 'description' => $_description ] ] ]))
            : ($tags['description'] ?? NULL);
    }
}
