<?php

namespace jf\php\generator;

use jf\php\generator\collection\Methods;

/**
 * Gestiona los métodos del elemento.
 *
 * @mixin IMethods
 */
trait TMethods
{
    /**
     * Colección de los métodos.
     *
     * @var Methods|NULL
     */
    public ?Methods $methods = NULL;

    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * Agrega el constructor.
     *
     * @param array|string $method Configuración del constructor o cuerpo del método.
     *
     * @return static
     */
    public function addConstructor(array|string $method) : static
    {
        $this->getMethods()->addConstructor(is_array($method) ? $method : [ 'body' => $method ]);

        return $this;
    }

    /**
     * Agrega el destructor.
     *
     * @param array|string $method Configuración del destructor o cuerpo del método.
     *
     * @return static
     */
    public function addDestructor(array|string $method) : static
    {
        $this->getMethods()->addDestructor(is_array($method) ? $method : [ 'body' => $method ]);

        return $this;
    }

    /**
     * @see IMethods::addMethods()
     */
    public function addMethods(array|string|NULL $methods) : static
    {
        if ($methods)
        {
            $this->getMethods()->addItems($methods);
        }

        return $this;
    }

    /**
     * @see IMethods::buildMethods()
     */
    public function buildMethods() : array
    {
        $_methods = $this->methods;
        if ($_methods)
        {
            foreach ($_methods->getItems() as $_key => $_method)
            {
                if (!$this->_modifyMethod($_method))
                {
                    $_methods->removeItem($_key);
                }
            }
        }

        return $_methods?->render() ?? [];
    }

    /**
     * @see TName::getFqcn()
     */
    abstract public function getFqcn() : string;

    /**
     * @see IMethods::getMethods()
     */
    public function getMethods() : Methods
    {
        return $this->methods ?? ($this->methods = Methods::fromName($this->getFqcn()));
    }

    /**
     * Modifca el método para adaptarlo al elemento actual.
     *
     * @param Method $method Método a modificar.
     *
     * @return bool
     */
    protected function _modifyMethod(Method $method) : bool
    {
        return TRUE;
    }
}
