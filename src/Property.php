<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Propiedad de una clase.
 */
class Property extends Variable implements IAttributes, IItem
{
    use TAttributes;
    use TCollectionItem;
    use TModifiers;

    /**
     * Indica si se debe agregar un método `getter` para obtener el valor de la propiedad.
     *
     * @var array|bool|string
     */
    public array|bool|string $getter = FALSE;

    /**
     * @inheritdoc
     */
    public bool $hasValue = TRUE;

    /**
     * Indica si se debe agregar un método inicializador del valor de la propiedad.
     *
     * @var array|bool|string|NULL
     */
    public array|bool|string|NULL $init = NULL;

    /**
     * Indica si la propiedad es de solamente lectura.
     *
     * @var bool
     */
    public bool $readonly = FALSE;

    /**
     * Indica si se debe agregar un método `setter` para asignar el valor de la propiedad.
     *
     * @var array|bool|string
     */
    public array|bool|string $setter = FALSE;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_code = array_filter([ ...$this->renderDoc(), ...$this->buildAttributes(), implode(' ', $this->buildCode()) ]);

        return $_code
            ? implode(PHP_EOL, $_code) . ';'
            : '';
    }

    /**
     * @inheritdoc
     */
    public function buildCode() : array
    {
        $_code = [];
        if ($this->name)
        {
            if ($this->final)
            {
                $_code[] = 'final';
            }
            if ($this->scope !== NULL)
            {
                $_code[] = $this->scope ?: 'public';
            }
            if ($this->readonly)
            {
                $_code[] = 'readonly';
            }
            elseif ($this->const)
            {
                $this->prefix = '';
                $this->name   = strtoupper($this->name);
                $_code[]      = 'const';
            }
            elseif ($this->static)
            {
                $_code[] = 'static';
            }
            $_var = parent::buildCode();
            if (!$this->const)
            {
                $_type = $this->getPhpType();
                if ($_type)
                {
                    $_code[] = $_type;
                }
            }
            array_push($_code, ...$_var);
        }

        return $_code;
    }

    /**
     * @inheritdoc
     */
    public function renderTags() : array
    {
        if ($this->override)
        {
            $_tags = $this->tags;
            array_unshift($_tags, new Tags([ 'tags' => [ [ 'name' => 'inheritdoc' ] ] ]));
            $_tags = $this->_renderTags($_tags);
        }
        else
        {
            $_tags = parent::renderTags();
        }

        return $_tags;
    }

    /**
     * @see TValue::renderValue()
     */
    public function renderValue() : string
    {
        return $this->readonly
            ? ''
            : parent::renderValue();
    }

    /**
     * Asigna el valor de la etiqueta.
     *
     * @param string $value Valor a asignar.
     *
     * @return static
     */
    public function setCopydoc(string $value) : static
    {
        if ($value && $value[0] === '\\' && str_contains($value, '::'))
        {
            [ $_class, $_method ] = explode('::', $value, 2);
            $_use = $this->addUses($_class);
            if (count($_use) > 1)
            {
                end($_use);
            }
            $_class = current($_use) ?: key($_use);
            if (str_contains($_class, '\\'))
            {
                $_class = Formatter::extractName($_class);
            }
            $value = "$_class::$_method";
        }
        $this->copydoc = $value;

        return $this;
    }

    /**
     * Asigna el nombre de la propiedad.
     *
     * @param string $name Valor a asignar.
     *
     * @return static
     */
    public function setName(string $name) : static
    {
        $this->name = $name;
        if ($name && $name[0] === '_' && ($name[1] ?? NULL) !== '_' && !$this->scope)
        {
            $this->scope = 'protected';
        }

        return $this;
    }
}
