<?php

namespace jf\php\generator;

use jf\php\generator\collection\TraitItems;

/**
 * Interfaz para las clases que requieren gestionar los traits.
 */
interface ITraits
{
    /**
     * Agrega a la colección los traits.
     *
     * @param array|string $traits Listado de los traits que serán agregados.
     *
     * @return static
     */
    public function addTraits(array|string $traits) : static;

    /**
     * Construye el código necesario para cada uno de los traits.
     *
     * @return array
     */
    public function buildTraits() : array;

    /**
     * Devuelve la colección de los traits.
     *
     * @return TraitItems
     */
    public function getTraits() : TraitItems;
}
