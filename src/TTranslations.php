<?php

namespace jf\php\generator;

/**
 * Trait para gestiona las traducciones de los textos.
 */
trait TTranslations
{
    /**
     * Textos usados para traducir algunas etiquetas.
     *
     * @var array
     */
    public static array $translations = [];

    /**
     * Traduce un texto admitiendo placeholders.
     *
     * @param string $label   Etiqueta del texto.
     * @param mixed  ...$args Argumentos posicionales a reemplazar.
     *
     * @return string
     */
    public static function tr(string $label, mixed ...$args) : string
    {
        $_tr = [];
        foreach ($args as $_index => $_arg)
        {
            $_tr[ '{' . $_index . '}' ] = $_arg;
        }

        return strtr(static::$translations[ $label ] ?? $label, $_tr);
    }
}
