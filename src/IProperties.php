<?php

namespace jf\php\generator;

use jf\php\generator\collection\Properties;

/**
 * Interfaz para las clases que requieren gestionar las propiedades.
 */
interface IProperties
{
    /**
     * Agrega a la colección las propiedades.
     *
     * @param array|string $properties Listado de las propiedades que serán agregados.
     *
     * @return static
     */
    public function addProperties(array|string $properties) : static;

    /**
     * Construye el código necesario para cada uno de las propiedades.
     *
     * @return array
     */
    public function buildProperties() : array;

    /**
     * Devuelve la colección de las propiedades.
     *
     * @return Properties
     */
    public function getProperties() : Properties;
}
