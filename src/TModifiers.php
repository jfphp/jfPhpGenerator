<?php

namespace jf\php\generator;

/**
 * Modificadores de un elemento.
 */
trait TModifiers
{
    /**
     * Indica si el elemento es abstracto.
     *
     * @var bool|NULL
     */
    public ?bool $abstract = NULL;

    /**
     * Indica si el elemento es una constante.
     *
     * @var bool|NULL
     */
    public ?bool $const = NULL;

    /**
     * Indica si el elemento puede sobrescribirse.
     *
     * @var bool|NULL
     */
    public ?bool $final = NULL;

    /**
     * Indica si el elemento sobrescribe otro.
     *
     * @var bool
     */
    public bool $override = FALSE;

    /**
     * Ámbito del elemento.
     *
     * @var string|NULL
     */
    public ?string $scope = '';

    /**
     * Indica si el elemento es estático.
     *
     * @var bool|NULL
     */
    public ?bool $static = FALSE;
}
