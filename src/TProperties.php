<?php

namespace jf\php\generator;

use jf\php\generator\collection\Properties;

/**
 * Gestiona las propiedades del elemento.
 *
 * @mixin IProperties
 */
trait TProperties
{
    use TTranslations;

    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * Colección de las propiedades.
     *
     * @var Properties|NULL
     */
    public ?Properties $properties = NULL;

    /**
     * @inheritdoc
     */
    public function __clone() : void
    {
        if ($this->properties)
        {
            $this->properties = clone $this->properties;
        }
    }

    /**
     * Agrega el getter de la propiedad.
     *
     * @param Property $property Propiedad que requiere el método.
     * @param string   $type     Tipo del método.
     *
     * @return void
     */
    protected function _addGetter(Property $property, string $type) : void
    {
        $_name   = $property->name;
        $_getter = $property->getter;
        if (!is_array($_getter))
        {
            $_getter = [ '$tpl' => $_getter ];
        }
        $_tpl = $_getter['$tpl'] ?? NULL;
        if ($_tpl && $_tpl !== TRUE)
        {
            $_r          = [
                '{property}' => $_name,
                '{type}'     => $type,
                '{Type}'     => ucfirst($type),
                ...$_getter
            ];
            $_r['{tpl}'] = Formatter::tpl($_tpl, $_r);
            $_tpl        = $_getter[ $_tpl ] ?? match ($_tpl)
            {
                '$builder'   => 'return $this->{property} ?? ($this->{property} = new {Type}());',
                '$container' => 'return $this->{property} ?? ($this->{property} = $this->container->get({type}::class));',
                default      => 'return $this->{property} ?? ($this->{property} = {tpl});'
            };
            $_tpl        = [
                'body'        => Formatter::tpl($_tpl, $_r),
                'description' => static::tr(
                    'Devuelve el valor de la propiedad `{0}` y la inicializa en caso de ser necesario',
                    $_name
                ),
                'name'        => 'get' . ABase::pascalize($_name),
                'null'        => $property->null,
                'type'        => $type
            ];
        }
        else
        {
            $_tpl = [
                'body'        => "return \$this->$_name;",
                'description' => static::tr('Devuelve el valor de la propiedad `{0}`', $_name),
                'name'        => 'get' . ABase::pascalize($_name),
                'null'        => $property->null,
                'type'        => $type
            ];
        }
        $_method = array_replace_recursive($_tpl, $_getter);
        unset($_method['$tpl']);
        $this->addMethods([ $_method ]);
    }

    /**
     * Agrega el método inicializador de la propiedad.
     *
     * @param Property $property Propiedad que requiere el método.
     * @param string   $type     Tipo del método.
     *
     * @return void
     */
    protected function _addInit(Property $property, string $type) : void
    {
        $_name = $property->name;
        $_init = $property->init;
        if ($_init === TRUE)
        {
            $_init = [ 'body' => "\$this->$_name = new $type();\n\nreturn \$this;", 'type' => 'static' ];
        }
        elseif ($_init === FALSE)
        {
            $_init = [
                'body' => "if (!\$this->$_name)\n{\n    \$this->$_name = new $type();\n}\n\nreturn \$this;",
                'type' => 'static'
            ];
        }
        elseif (is_string($_init))
        {
            $_init = [ 'body' => $_init ];
        }
        $this->addMethods(
            [
                'init' . ABase::pascalize($_name) => [
                    'description' => static::tr('Inicializa la propiedad `{0}`', $_name),
                    ...$_init
                ]
            ]
        );
    }

    /**
     * @see TMethods::addMethods()
     */
    abstract public function addMethods(array $methods) : static;

    /**
     * @see IProperties::addProperties()
     */
    public function addProperties(array|string|null $properties) : static
    {
        if ($properties)
        {
            $this->getProperties()->addItems($properties);
        }

        return $this;
    }

    /**
     * Agrega un setter para la propiedad.
     *
     * @param Property $property Propiedad que requiere el método.
     * @param string   $type     Tipo del método.
     *
     * @return void
     */
    protected function _addSetter(Property $property, string $type) : void
    {
        $_desc   = $property->renderDescription();
        $_name   = $property->name;
        $_setter = $property->setter;
        if (is_array($_setter))
        {
            unset($_setter['$tpl']);
        }
        else
        {
            $_setter = [];
        }
        $this->addMethods(
            [
                array_replace_recursive(
                    [
                        'body'        => "\$this->$_name = \$$_name;\n\nreturn \$this;",
                        'description' => $_desc
                            ? static::tr("Asigna el valor de la propiedad `{0}`:\n\n```\n{1}\n```", $_name, $_desc)
                            : static::tr('Asigna el valor de la propiedad `{0}`', $_name),
                        'name'        => 'set' . ABase::pascalize($_name),
                        'type'        => 'static',
                        'params'      => [
                            [
                                'description' => static::tr('Valor a asignar'),
                                'name'        => $_name,
                                'null'        => $property->null,
                                'type'        => $type
                            ]
                        ]
                    ],
                    $_setter
                )
            ]
        );
    }

    /**
     * @see IProperties::buildProperties()
     */
    public function buildProperties() : array
    {
        $_properties = $this->properties;
        if ($_properties)
        {
            foreach ($_properties->getItems() as $_key => $_property)
            {
                if (!$this->_modifyProperty($_property))
                {
                    $_properties->removeItem($_key);
                }
            }
        }

        return $_properties?->render() ?? [];
    }

    /**
     * @see TName::getFqcn()
     */
    abstract public function getFqcn() : string;

    /**
     * @see IProperties::getProperties()
     */
    public function getProperties() : Properties
    {
        return $this->properties ?? ($this->properties = Properties::fromName($this->getFqcn()));
    }

    /**
     * Modifca la propiedad para adaptarla al elemento actual.
     *
     * @param Property $property Propiedad a modificar.
     *
     * @return bool
     */
    protected function _modifyProperty(Property $property) : bool
    {
        $_init   = $property->init;
        $_getter = $property->getter;
        $_setter = $property->setter;
        if ($_getter || $_setter || $_init !== NULL)
        {
            $_name = $property->name;
            $_type = current(Property::parseTypes($property->getType()));
            if ($_init !== NULL)
            {
                $this->_addInit($property, $_type);
            }
            if ($_getter)
            {
                $this->_addGetter($property, $_type);
            }
            if ($_setter)
            {
                $this->_addSetter($property, $_type);
            }
            if ($_getter && $_setter && !$property->scope)
            {
                // Si tenemos getter y setter ocultamos la propiedad si no se ha especificado el ámbito.
                $property->scope = 'protected';
            }
        }

        return TRUE;
    }
}
