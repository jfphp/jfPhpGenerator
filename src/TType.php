<?php

namespace jf\php\generator;

use jf\assert\Assert;

/**
 * Gestiona el tipo de una variable, propieadad o parámetro.
 */
trait TType
{
    use TTranslations;
    use TUses;
    use TValue
    {
        TValue::setValue as private _setValue;
    }

    /**
     * Permite especificar el tipos de datos usando una intersección de tipos (>= PHP 8.2).
     *
     * @var string[]|NULL
     */
    public ?array $intersectionType = [];

    /**
     * Tipos de datos del elemento.
     *
     * @var string[]|NULL
     */
    public ?array $type = [];

    /**
     * Agrega el tipo de datos.
     *
     * @param string|string[]|NULL $type Valor a agregar.
     *
     * @return static
     */
    public function addIntersectionType(array|string|null $type) : static
    {
        return $this->_parseType($this->intersectionType, $type, '&');
    }

    /**
     * Agrega el tipo de datos.
     *
     * @param string|string[]|NULL $type Valor a agregar.
     *
     * @return static
     */
    public function addType(array|string|null $type) : static
    {
        if ($type === NULL)
        {
            $this->type = NULL;
        }
        else
        {
            $_isIntersection = is_string($type) && str_contains($type, '&');
            Exception::isFalse(
                $_isIntersection && str_contains($type, '|'),
                static::tr('No se aceptan intersecciones y uniones al mismo tiempo')
            );
            if ($_isIntersection)
            {
                $this->addIntersectionType($type);
            }
            else
            {
                $this->_parseType($this->type, $type, '|');
            }
        }

        return $this;
    }

    /**
     * Devuelve el tipo de datos tal como se debe mostrar en el código PHP.
     *
     * @return string
     */
    public function getPhpType() : string
    {
        $_null = $this->null;
        if ($this->intersectionType)
        {
            $_type = implode('&', static::parseTypes($this->intersectionType));
        }
        elseif ($_type = $this->getType())
        {
            $_type = static::parseTypes($_type);
            if (isset($_type['mixed']))
            {
                $_type = [ 'mixed' ];
            }
            else
            {
                if (isset($_type['callable']))
                {
                    unset($_type['callable']);
                }
                if ($_null)
                {
                    if (count($_type) > 1)
                    {
                        $_type[] = $_null;
                    }
                    else
                    {
                        $_type = [ '?' . current($_type) ];
                    }
                }
                if (isset($_type['false']))
                {
                    $_type['false'] = 'FALSE';
                }
                if (isset($_type['true']))
                {
                    $_type['true'] = 'TRUE';
                }
            }
            $_type = implode('|', $_type);
        }
        elseif ($_null)
        {
            $_type = $_null;
        }
        else
        {
            $_type = '';
        }

        return $_type;
    }

    /**
     * Devuelve el tipo el elemento.
     *
     * @return string[]|NULL
     */
    public function getType() : ?array
    {
        return $this->type;
    }

    /**
     * Devuelve el tipo de datos según su valor.
     *
     * @param mixed $value Valor a analizar.
     *
     * @return string
     */
    public static function inferType(mixed $value) : string
    {
        if (is_callable($value))
        {
            $_type = 'callable';
        }
        else
        {
            $_type = gettype($value);
            $_type = match ($_type)
            {
                'boolean' => 'bool',
                'double'  => 'float',
                'integer' => 'int',
                default   => $_type
            };
        }

        return $_type;
    }

    /**
     * Indica si el tipo de datos representa un array.
     *
     * @param string $type Tipo a analizar.
     *
     * @return bool
     */
    public static function isArray(string $type) : bool
    {
        return str_ends_with($type, '[]') || preg_match('/^array<[^>]+>$/', $type) || preg_match('/^array\{[^}]+}$/', $type);
    }

    /**
     * Analiza el tipo de datos, lo formatea y lo agrega al valor actual.
     *
     * @param mixed                $current Valor actual y que recibirá los nuevos tipos.
     * @param string|string[]|NULL $type    Valores a agregar.
     * @param string               $sep     Separador entre tipos.
     *
     * @return static
     */
    private function _parseType(mixed &$current, array|string|null $type, string $sep) : static
    {
        if ($type === NULL)
        {
            $current = NULL;
        }
        else
        {
            $_hasArray = FALSE;
            if (is_string($type))
            {
                $type = static::splitType($type, $sep);
                $type = array_combine($type, $type);
            }
            $type = array_filter(array_map($this->_parseUse(...), $type));
            foreach ($type as $_name)
            {
                if (strtolower($_name) === 'null')
                {
                    if (!$this->null)
                    {
                        $this->null = $_name;
                    }
                }
                else
                {
                    if ($_name[0] === '?')
                    {
                        if (!$this->null)
                        {
                            $this->null = 'NULL';
                        }
                        $_name = substr($_name, 1);
                    }
                    if (static::isArray($_name))
                    {
                        $_hasArray = TRUE;
                    }
                    if (!is_array($current))
                    {
                        $current = [];
                    }
                    $current[ $_name ] = $_name;
                }
            }
            if ($_hasArray)
            {
                unset($current['array']);
            }
            if (isset($current['false']) && isset($current['true']))
            {
                $current['bool'] = 'bool';
                unset($current['false'], $current['true']);
            }
        }

        return $this;
    }

    /**
     * Elimina los tipos de datos no permitidos en PHP tales como `resource`, etc.
     *
     * @param string[] $types Tipos a analizar.
     *
     * @return string[]
     */
    public static function parseTypes(array $types) : array
    {
        if (isset($types['mixed']) || isset($types['resource']))
        {
            $types = [ 'mixed' ];
        }
        else
        {
            $types = array_unique(
                array_filter(
                    array_map(
                        fn($type) => match (TRUE)
                        {
                            str_starts_with($type, 'class-string')                    => 'string',
                            static::isArray($type)                                    => 'array',
                            preg_match('/^([^<]+)<[^>]+>$/', $type, $_matches) === 1  => $_matches[1],
                            preg_match('/^([^{]+)\{[^}]+}$/', $type, $_matches) === 1 => $_matches[1],
                            default                                                   => $type
                        },
                        $types
                    )
                )
            );
            self::sortTypes($types);
        }

        return array_combine($types, $types);
    }

    /**
     * Asigna el valor del elemento.
     *
     * @param mixed $value Valor a asignar.
     *
     * @return static
     */
    public function setValue(mixed $value) : static
    {
        $this->_setValue($value);
        if (!$this->type)
        {
            $this->addType(self::inferType($value));
        }

        return $this;
    }

    /**
     * Ordena los tipos de datos.
     *
     * De forma resumida, primero se colocan los que empiezan con mayúsculas (clases,
     * interfaces, traits, etc.) luego se colocan los tipos de PHP y por último `NULL`
     * si el tipo lo acepta.
     *
     * @param string[] $types Tipos a analizar.
     *
     * @return void
     */
    public static function sortTypes(array &$types) : void
    {
        usort(
            $types,
            function ($type1, $type2)
            {
                $utype1 = strtoupper($type1);
                $utype2 = strtoupper($type2);

                return match (TRUE)
                {
                    $utype1 === 'NULL'     => 1,
                    $utype2 === 'NULL'     => -1,
                    $utype1 === 'FALSE'    => 1,
                    $utype2 === 'FALSE'    => -1,
                    $utype1 === 'TRUE'     => 1,
                    $utype2 === 'TRUE'     => -1,
                    ctype_upper($type1[0]) => ctype_upper($type2[0]) ? strcmp($type1, $type2) : -1,
                    ctype_upper($type2[0]) => 1,
                    default                => strcmp($type1, $type2)
                };
            }
        );
    }

    /**
     * Analzia el tipo tipo de datos y lo separa en varios si vienen concatenados con el separador especificado.
     *
     * @param string $type Tipo a analizar.
     * @param string $sep  Separador de tipos.
     *
     * @return string[]
     */
    public static function splitType(string $type, string $sep = '|') : array
    {
        $current = '';
        $chars   = [];
        $types   = [];
        for ($i = 0, $l = strlen($type); $i < $l; ++$i)
        {
            $char = $type[ $i ];
            switch ($char)
            {
                case $sep:
                    if (!$chars)
                    {
                        $types[] = $current;
                        $char    = '';
                        $current = '';
                    }
                    break;
                case '[':
                case '{':
                case '<':
                    $chars[] = $char;
                    break;
                case ']':
                case '}':
                case '>':
                    switch ($char)
                    {
                        case ']':
                            Assert::identical(array_pop($chars), '[', _('Carácter de cierre inesperado: {0}'), $char);
                            break;
                        case '}':
                            Assert::identical(array_pop($chars), '{', _('Carácter de cierre inesperado: {0}'), $char);
                            break;
                        case '>':
                            Assert::identical(array_pop($chars), '<', _('Carácter de cierre inesperado: {0}'), $char);
                            break;
                    }
                    break;
            }
            $current .= $char;
        }
        Assert::empty($chars, _('Caracteres sin cerrar: {0}'));
        if ($current)
        {
            $types[] = $current;
        }

        return $types;
    }
}
