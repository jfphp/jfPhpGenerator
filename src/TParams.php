<?php

namespace jf\php\generator;

use jf\php\generator\collection\Params;

/**
 * Gestiona los parámetros del método del elemento.
 *
 * @mixin IParams
 */
trait TParams
{
    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * Colección de los parámetros del método.
     *
     * @var Params|NULL
     */
    public ?Params $params = NULL;

    /**
     * @see IParams::addParams()
     */
    public function addParams(array|string|NULL $params) : static
    {
        if ($params)
        {
            $this->getParams()->addItems($params);
        }

        return $this;
    }

    /**
     * @see IParams::buildParams()
     */
    public function buildParams() : array
    {
        return $this->params?->render() ?? [];
    }

    /**
     * @see IParams::getParams()
     */
    public function getParams() : Params
    {
        return $this->params ?? ($this->params = Params::fromName($this->name));
    }
}
