<?php

namespace jf\php\generator;

use BackedEnum;
use UnitEnum;

/**
 * Clase con utilidades para manipular textos.
 */
class Formatter
{
    /**
     * Cantidad de elementos permitidos en una línea.
     * Si se sobrepasa este valor se formatea en varias líneas.
     *
     * @var int
     */
    public static int $count = 1;

    /**
     * Ancho máxima de una línea.
     *
     * @var int
     */
    public static int $lineWidth = 80;

    /**
     * Agrega un backslash al inicio si el valor es un FQCN.
     *
     * @param array|string $fqcn Valor a modificar.
     *
     * @return array|string
     */
    public static function addBackslash(array|string $fqcn) : array|string
    {
        if (is_array($fqcn))
        {
            $fqcn = array_map(static::addBackslash(...), $fqcn);
        }
        elseif (str_contains($fqcn, '\\') && $fqcn[0] !== '\\')
        {
            $fqcn = '\\' . $fqcn;
        }

        return $fqcn;
    }

    /**
     * Extrae el nombre a partir del FQCN.
     *
     * @param string $fqcn Nombre completo de la clase.
     * @param int    $from Índice a partir del cual extraer el nombre.
     *
     * @return string
     */
    public static function extractName(string $fqcn, int $from = 1) : string
    {
        return str_contains($fqcn, '\\')
            ? substr($fqcn, strrpos($fqcn, '\\') + $from)
            : $fqcn;
    }

    /**
     * Formatea un array.
     *
     * @param array $value Valor a formatear.
     *
     * @return string
     */
    public static function formatArray(array $value) : string
    {
        if ($value)
        {
            $_array  = [];
            $_lines  = [];
            $_scalar = TRUE;
            foreach ($value as $_key => $_val)
            {
                $_scalar = $_scalar && is_scalar($_val);
                if (is_string($_key))
                {
                    $_key = static::formatString($_key);
                }
                $_val = static::formatValue($_val);
                if (str_contains($_val, PHP_EOL))
                {
                    $_val = ltrim(static::rtrimBlanks(static::indent($_val)));
                }
                $_array[ $_key ] = $_val;
            }
            // Si el valor tiene solamente elementos escalares y tiene el valor de elementos
            // permitidos en una línea, se coloca en una línea.
            if ($_scalar && count($value) <= static::$count)
            {
                foreach ($_array as $_key => $_val)
                {
                    $_lines[] = is_int($_key)
                        ? sprintf('%s', $_val)
                        : sprintf('%s => %s', $_key, $_val);
                }
                $value = sprintf('[ %s ]', implode(', ', $_lines));
            }
            else
            {
                $_len = max(array_map('mb_strlen', array_keys($_array)));
                foreach ($_array as $_key => $_val)
                {
                    $_lines[] = is_int($_key)
                        ? sprintf('    %s', $_val)
                        : sprintf('    %s%s => %s', $_key, str_repeat(' ', $_len - mb_strlen($_key)), $_val);
                }
                $value = sprintf("[\n%s\n]", implode(",\n", $_lines));
            }
        }
        else
        {
            $value = '[]';
        }

        return $value;
    }

    /**
     * Formatea un valor booleano.
     *
     * @param bool $value Valor a formatear.
     *
     * @return string
     */
    public static function formatBoolean(bool $value) : string
    {
        return $value
            ? 'TRUE'
            : 'FALSE';
    }

    /**
     * Formatea un valor en coma flotante.
     *
     * @param float $value Valor a formatear.
     *
     * @return string
     */
    public static function formatFloat(float $value) : string
    {
        $value = (string) $value;

        return strpos($value, '.')
            ? $value
            : "$value.0";
    }

    /**
     * Formatea un texto JSON con valores unicode.
     *
     * @param string $value Valor a formatear.
     *
     * @return string
     */
    public static function formatJson(string $value) : string
    {
        return preg_replace('/\\\\u([a-z0-9]{4})/', '\u{\1}', json_encode($value));
    }

    /**
     * Formatea las filas para que cada columna tenga el mismo ancho.
     *
     * @param array $rows Filas a formatear.
     *
     * @return array
     */
    public static function formatRows(array $rows) : array
    {
        if ($rows)
        {
            $_keys   = array_keys(current($rows));
            $_last   = end($_keys);
            $_widths = [];
            foreach ($_keys as $_key)
            {
                $_widths[ $_key ] = max(
                    array_map(
                        fn($text) => str_contains($text, PHP_EOL)
                            ? max(array_map('strlen', explode(PHP_EOL, $text)))
                            : strlen($text),
                        array_column($rows, $_key)
                    )
                );
            }
            foreach ($rows as $_index => $_row)
            {
                $_cells = [];
                $_col   = 0;
                foreach ($_row as $_key => $_value)
                {
                    $_width = $_widths[ $_key ];
                    if ($_width)
                    {
                        if (!is_string($_value))
                        {
                            $_value = (string) $_value;
                        }
                        if ($_col && str_contains($_value, PHP_EOL))
                        {
                            $_lines = explode(PHP_EOL, $_value);
                            $_len   = max(array_map('strlen', $_lines));
                            $_value = ltrim(implode(PHP_EOL, Formatter::indent($_lines, $_col)));
                        }
                        else
                        {
                            $_len = strlen($_value);
                        }
                        $_cells[] = $_key === $_last
                            ? $_value
                            : $_value . str_repeat(' ', $_width - $_len);
                        // Sumamos el ancho a partir de la segunda celda y agregamos 1 para el espacio en blanco entre celdas.
                        $_col += $_width + 1;
                    }
                }
                $rows[ $_index ] = $_cells;
            }
        }

        return $rows;
    }

    /**
     * Formatea un texto.
     *
     * @param string $value Valor a formatear.
     *
     * @return string
     */
    public static function formatString(string $value) : string
    {
        return match ($value)
        {
            PHP_EOL => 'PHP_EOL',
            '\''    => '\'\\\'\'',
            '\\\''  => '\'\\\\\\\'\'',
            '\\'    => '\'\\\\\'',
            default => match (TRUE)
            {
                $value === '$this' || str_contains($value, '$this->')                   => $value,
                str_contains($value, '::') && preg_match('|^[\w:\\\\]+$|', $value) > 0  => $value,
                str_contains($value, PHP_EOL) && mb_strlen($value) < static::$lineWidth => json_encode($value),
                preg_match('/[\x00-\x1F]/', $value) > 0                                 => self::formatJson($value),
                default                                                                 => static::quote($value)
            }
        };
    }

    /**
     * Formatea el valor.
     *
     * @param mixed $value Valor a formatear.
     *
     * @return string
     */
    public static function formatValue(mixed $value) : string
    {
        return match (gettype($value))
        {
            'array'   => self::formatArray($value),
            'boolean' => self::formatBoolean($value),
            'double'  => self::formatFloat($value),
            'string'  => self::formatString($value),
            'NULL'    => 'NULL',
            default   => match (TRUE)
            {
                $value instanceof BackedEnum => self::formatValue($value->value),
                $value instanceof UnitEnum   => self::formatValue($value->name),
                default                      => (string) $value
            }
        };
    }

    /**
     * Devuelve el FQCN del elemento generado.
     *
     * @param array|string $name      Nombre del elemento generado o configuración usada para generarlo.
     * @param string       $namespace Espacio de nombres del elemento generado.
     *
     * @return string
     */
    public static function getFqcn(array|string $name, string $namespace = '') : string
    {
        if (is_array($name))
        {
            $namespace = $name['namespace'] ?? '';
            $name      = $name['name'];
        }

        return static::ltrim($namespace . '\\' . $name);
    }

    /**
     * Indenta el texto o el listado de textos.
     *
     * @param array|string $text   Texto o listado de textos a indentar.
     * @param int          $length Tamaño de la indentación.
     * @param string       $indent Texto a usar para indentar.
     *
     * @return array|string
     */
    public static function indent(array|string $text, int $length = 4, string $indent = ' ') : array|string
    {
        return is_array($text)
            ? array_map(fn($text) => self::indent($text, $length, $indent), $text)
            : preg_replace('/^/m', str_repeat($indent, $length), $text);
    }

    /**
     * Elimina el `\` al inicio del nombre y que se usa para indicar que es un elemento global.
     *
     * @param array|string $text  Texto a modificar.
     * @param string       $chars Caracteres a eliminar por la izquierda.
     *
     * @return array|string
     */
    public static function ltrim(array|string $text, string $chars = '\\') : array|string
    {
        return is_array($text)
            ? array_map(static::ltrim(...), $text)
            : ltrim(str_replace('\\\\', '\\', $text), $chars);
    }

    /**
     * Encierra un texto entre los caracteres especificados.
     *
     * @param string $text   Valor a formatear.
     * @param string $char   Carácter a usar como delimitador.
     * @param string $escape Texto a usar para escapar el carácter si existe dentro del texto.
     *
     * @return string
     */
    public static function quote(string $text, string $char = '\'', string $escape = '\\\'') : string
    {
        return $char . str_replace($char, $escape, $text) . $char;
    }

    /**
     * Elimina los caracteres en blanco al final de las líneas.
     *
     * @param string $text Texto a modificar.
     *
     * @return string
     */
    public static function rtrimBlanks(string $text) : string
    {
        return preg_replace('/[ \t]+$/m', '', $text);
    }

    /**
     * Separa un nombre cualificado en el espacio de nombres y el nombre del elemento.
     *
     * @param string $fqcn Nombre cualificado del elemento.
     *
     * @return array
     */
    public static function splitName(string $fqcn) : array
    {
        $_index = strrpos($fqcn, '\\');

        return $_index === FALSE
            ? [ '', $fqcn ]
            : [ self::ltrim(substr($fqcn, 0, $_index)), substr($fqcn, $_index + 1) ];
    }

    /**
     * Reemplaza los marcadores presentes en la plantilla.
     *
     * @param string $tpl          Plantilla a modificar.
     * @param array  $placeholders Valores de reemplazo para los marcadores.
     *
     * @return string
     */
    public static function tpl(string $tpl, array $placeholders) : string
    {
        return preg_replace_callback(
            '/(\{[^}]+})/',
            static fn($matches) => $placeholders[ $matches[1] ] ?? $matches[0],
            $tpl,
        );
    }

    /**
     * Desindenta el texto o el listado de textos.
     *
     * @param array|string $text   Texto o listado de textos a desindentar.
     * @param int          $length Tamaño de la indentación a eliminar.
     * @param string       $indent Texto usado para indentar.
     *
     * @return array|string
     */
    public static function unindent(array|string $text, int $length = 4, string $indent = ' ') : array|string
    {
        return is_array($text)
            ? array_map(fn($text) => self::unindent($text, $length, $indent), $text)
            : preg_replace("/^$indent{{$length}}/m", '', $text);
    }

    /**
     * Formatea el texto a un ancho máximo.
     *
     * @param string $text      Texto a formatear.
     * @param int    $tolerance Tolerancia para evitar dejar palabras cortas en las descripciones.
     *
     * @return string
     */
    public static function wordwrap(string $text, int $tolerance = 12) : string
    {
        if ($text)
        {
            $text = wordwrap($text, static::$lineWidth);
            if ($tolerance > 0)
            {
                $_index = mb_strrpos($text, "\n");
                $_len   = mb_strlen($text);
                if ($_index !== FALSE && $_index < $_len - 1 && $_index > $_len - $tolerance && $text[ -1 ] !== '`')
                {
                    $text = mb_substr($text, 0, $_index) . ' ' . mb_substr($text, $_index + 1);
                }
            }
        }

        return $text;
    }
}
