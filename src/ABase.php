<?php

namespace jf\php\generator;

use jf\Base\AAssignExtras;
use jf\Base\TDasherize;

/**
 * Clase base para el resto de clases del proyecto.
 */
abstract class ABase extends AAssignExtras
{
    use TDasherize;

    /**
     * Devuelve el nombre corto de la clase.
     *
     * @return string
     */
    public static function getClassname() : string
    {
        return Formatter::extractName(static::class);
    }

    /**
     * @inheritdoc
     */
    public function toArray() : array
    {
        $_data = parent::toArray();
        foreach ($_data as $_k => $_v)
        {
            if ($_v === NULL)
            {
                unset($_data[ $_k ]);
            }
        }

        return $_data;
    }
}
