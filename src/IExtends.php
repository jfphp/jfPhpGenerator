<?php

namespace jf\php\generator;

use jf\php\generator\collection\Classnames;

/**
 * Interfaz para las clases que requieren gestionar los elementos de los que
 * extiende la clase o interfaz.
 */
interface IExtends
{
    /**
     * Agrega a la colección los elementos de los que extiende la clase o interfaz.
     *
     * @param array|string $extends Listado de los elementos de los que extiende la clase o interfaz que serán agregados.
     *
     * @return static
     */
    public function addExtends(array|string $extends) : static;

    /**
     * Construye el código necesario para cada uno de los elementos de los que
     * extiende la clase o interfaz.
     *
     * @return array
     */
    public function buildExtends() : array;

    /**
     * Devuelve la colección de los elementos de los que extiende la clase o interfaz.
     *
     * @return Classnames
     */
    public function getExtends() : Classnames;
}
