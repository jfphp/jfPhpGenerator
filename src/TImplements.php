<?php

namespace jf\php\generator;

use jf\php\generator\collection\Classnames;

/**
 * Gestiona los elementos que implementa la clase del elemento.
 *
 * @mixin IImplements
 */
trait TImplements
{
    /**
     * Colección de los elementos que implementa la clase.
     *
     * @var Classnames|NULL
     */
    public ?Classnames $implements = NULL;

    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * @see IImplements::addImplements()
     */
    public function addImplements(array|string|NULL $implements) : static
    {
        if ($implements)
        {
            $this->getImplements()->addItems($implements);
        }

        return $this;
    }

    /**
     * @see IImplements::buildImplements()
     */
    public function buildImplements() : array
    {
        return $this->implements?->render() ?? [];
    }

    /**
     * @see IImplements::getImplements()
     */
    public function getImplements() : Classnames
    {
        return $this->implements ?? ($this->implements = Classnames::fromName($this->name));
    }

    /**
     * Renderiza el valor del trait.
     *
     * @return string
     */
    public function renderImplements() : string
    {
        return $this->implements
            ? 'implements ' . implode(', ', $this->buildImplements())
            : '';
    }
}
