<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Genera una función de PHP.
 */
class FunctionItem extends ABase implements IAttributes, IGenerator, IItem, IParams, IUses
{
    use TAttributes;
    use TCollectionItem;
    use TDocDescription;
    use TParams;
    use TDocTags
    {
        TDocTags::renderTags as protected _renderTags;
    }
    use TDocType
    {
        TDocType::getUses as protected _getUses;
    }

    /**
     * Cuerpo del método.
     *
     * Si el método es NULL no se agrega nada al resultado (como si fuera un método
     * abstracto).
     *
     * Si es una cadena vacía se agrega como un método sin código.
     *
     * @var string|NULL
     */
    public ?string $body = NULL;

    /**
     * Palabra clave de PHP a usar para definir la función ([ function | fn ]).
     *
     * @var string
     */
    public string $keyword = 'function';

    /**
     * Comentario adicional para el valor de retorno de la función.
     *
     * @var string|NULL
     */
    public ?string $return = NULL;

    /**
     * @see TModifiers::$static
     */
    public ?bool $static = FALSE;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return implode(
            PHP_EOL,
            array_filter([ ...$this->renderDoc(), ...$this->buildAttributes(), implode(PHP_EOL, $this->buildCode()) ])
        );
    }

    /**
     * Construye la sentencia PHP para definir el elemento.
     *
     * @return string[]
     */
    public function buildCode() : array
    {
        $_keyword = $this->keyword ?: 'function';
        $_name    = $this->name;
        $_code    = [];
        if ($this->static === NULL && $this->body)
        {
            $this->static = !str_contains($this->body, '$this');
        }
        if ($this->static)
        {
            $_code[] = 'static';
        }
        $_code[] = $_keyword;
        $_code[] = $this->params
            ? sprintf('%s(%s)', $_name, implode(', ', $this->buildParams()))
            : sprintf('%s()', $_name);
        $_type   = $this->getPhpType() ?: 'void';
        if ($this->getType() !== NULL)
        {
            $_code[] = ':';
            $_code[] = $_type;
        }
        $_code = [ implode(' ', $_code) ];
        $_body = $this->body;
        if ($_body === '')
        {
            // Tratamos de inferir un body en función del valor de retorno.
            foreach (explode('|', $_type) as $_type)
            {
                $_body = match (strtolower($_type))
                {
                    'bool', 'true' => 'return TRUE;',
                    'false'        => 'return FALSE;',
                    'int'          => 'return 0;',
                    'float'        => 'return 0.0;',
                    'static'       => $this->static ? 'return static;' : 'return $this;',
                    'string'       => 'return \'\';',
                    'void'         => '',
                    default        => $this->null
                        ? sprintf('return %s;', $this->null)
                        : NULL
                };
                if ($_body !== NULL)
                {
                    $this->body = $_body;
                    break;
                }
            }
            if ($_body)
            {
                $_body = Formatter::indent($_body);
            }
        }
        if ($_body !== NULL)
        {
            if ($_keyword === 'fn' && $_body && !str_contains($_body, PHP_EOL))
            {
                $_code[0] .= ' => ' . trim($_body);
            }
            else
            {
                if ($_keyword === 'fn')
                {
                    $_code[0] .= ' =>';
                }
                $_code[] = '{';
                if ($_body)
                {
                    $_code[] = $_body;
                }
                $_code[] = '}';
            }
        }

        return $_code;
    }

    /**
     * @see TUses::getUses()
     */
    public function getUses() : array
    {
        return [ ...$this->_getUses(), ...static::extractUses($this) ];
    }

    /**
     * @see TTags::renderTags()
     */
    public function renderTags() : array
    {
        $_tags = $this->tags;
        if (!$this->copydoc)
        {
            $_mtags = [];
            if ($this->getType() !== NULL)
            {
                $_tag = $this->addDocType($_mtags, 'return');
                if ($_tag && $this->return)
                {
                    end($_tag->tags)->description = $this->return;
                }
            }
            if ($this->params)
            {
                $_params = [];
                foreach ($this->params as $_param)
                {
                    $_params = [ ...$_params, ...$_param->renderTags() ];
                }
                if ($_params)
                {
                    $_mtags['params'] = new Tags([ 'tags' => $_params ]);
                }
            }
            $this->addDocDescription($_mtags);
            foreach ($_mtags as $_name => $_tag)
            {
                unset($_tags[ $_name ]);
                array_unshift($_tags, $_tag);
            }
        }

        return $this->_renderTags($_tags);
    }

    /**
     * Agrega el cuerpo del método.
     *
     * @param string|string[]|NULL $body Cuerpo del método a agregar.
     *
     * @return static
     */
    public function setBody(array|string|NULL $body) : static
    {
        if ($body === NULL)
        {
            $this->body = NULL;
        }
        else
        {
            if (is_array($body))
            {
                $body = implode(PHP_EOL, $body);
            }
            if (str_contains($body, '\\'))
            {
                $body = preg_replace_callback(
                    '/(^|[\s(])((?:\\\\\w+)+(?:@\w+)?)/m',
                    function ($matches)
                    {
                        [ $_name, $_alias ] = explode('@', $matches[2]) + [ '', '' ];
                        $_name = ltrim($_name, '\\');
                        if ((ctype_upper($_name[0]) || str_contains($_name, '\\')) && Helpers::exists($_name))
                        {
                            $this->addUses([ $_name => $_alias ]);
                            $_name = $matches[1] . ($_alias ?: Formatter::extractName($_name));
                        }
                        else
                        {
                            $_name = $matches[0];
                        }

                        return $_name;
                    },
                    $body
                );
            }
            $this->body = rtrim(
                $body && !ctype_space($body[0])
                    ? Formatter::rtrimBlanks(Formatter::indent($body))
                    : $body
            );
        }

        return $this;
    }

    /**
     * Asigna el valor de la etiqueta.
     *
     * @param string $value Valor a asignar.
     *
     * @return static
     */
    public function setCopydoc(string $value) : static
    {
        if ($value && $value[0] === '\\' && str_contains($value, '::'))
        {
            [ $_class, $_method ] = explode('::', $value, 2);
            $_use = $this->addUses($_class);
            if (count($_use) > 1)
            {
                end($_use);
            }
            $_class = current($_use) ?: key($_use);
            if (str_contains($_class, '\\'))
            {
                $_class = Formatter::extractName($_class);
            }
            $value = "$_class::$_method";
        }
        $this->copydoc = $value;

        return $this;
    }
}
