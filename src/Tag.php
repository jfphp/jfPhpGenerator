<?php

namespace jf\php\generator;

/**
 * Representa una etiqueta para documentar un elemento.
 */
class Tag extends ABase implements IUses
{
    use TDescription;
    use TUses
    {
        TUses::getUses as private _getUses;
    }

    /**
     * Nombre de la etiqueta.
     *
     * @var string
     */
    public string $name = '';

    /**
     * Nombres de etiquetas que pueden contener FQCNs.
     *
     * @var string[]
     */
    public array $tagWithUses = [
        'extends',
        'implements',
        'mixin',
        'see',
        'throws',
        'use'
    ];

    /**
     * Valor de la etiqueta.
     *
     * @var string
     */
    public string $value = '';

    /**
     * Nombre de la variable, parámetro, propiedad, etc.
     * Facilita agregar el `$` como prefijo.
     *
     * @var string
     */
    public string $variable = '';

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return implode(
            ' ',
            array_filter(
                array_map(
                    'trim',
                    [
                        $this->getName(),
                        $this->value,
                        $this->getVariable(),
                        $this->renderDescription()
                    ]
                )
            )
        );
    }

    /**
     * Devuelve el nombre de la etiqueta.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name
            ? '@' . ltrim($this->name, '@')
            : '';
    }

    /**
     * @see TUses::getUses()
     */
    public function getUses() : array
    {
        return [ ...$this->_getUses(), ...static::extractUses($this) ];
    }

    /**
     * Devuelve el valor usado como nombre de variable.
     *
     * @return string
     */
    public function getVariable() : string
    {
        $_variable = $this->variable;
        // Verificamos que no tenga el '$' en alguna parte en vez del principio ya
        // que en el caso de los variadic lo tendrían después de `...`.
        if ($_variable && !str_contains($_variable, '$'))
        {
            $_variable = str_starts_with($_variable, '...')
                ? '...$' . substr($_variable, 3)
                : '$' . $_variable;
        }

        return $_variable;
    }

    /**
     * Asigna el valor de la etiqueta.
     *
     * @param string $value Valor a asignar.
     *
     * @return static
     */
    public function setValue(string $value) : static
    {
        if ($value && $value[0] === '\\' && in_array($this->name, $this->tagWithUses))
        {
            $_use = $this->addUses($value);
            if (count($_use) > 1)
            {
                end($_use);
            }
            $value = current($_use) ?: key($_use);
            if (str_contains($value, '\\'))
            {
                $value = Formatter::extractName($value);
            }
        }
        $this->value = $value;

        return $this;
    }
}
