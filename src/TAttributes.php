<?php

namespace jf\php\generator;

use jf\php\generator\collection\Attributes;

/**
 * Gestiona los atributos del elemento.
 *
 * @mixin IAttributes
 */
trait TAttributes
{
    /**
     * Colección de los atributos.
     *
     * @var Attributes|NULL
     */
    public ?Attributes $attributes = NULL;

    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * @see IAttributes::addAttributes()
     */
    public function addAttributes(array|string|NULL $attributes) : static
    {
        if ($attributes)
        {
            $this->getAttributes()->addItems($attributes);
        }

        return $this;
    }

    /**
     * @see IAttributes::buildAttributes()
     */
    public function buildAttributes() : array
    {
        return $this->attributes?->render() ?? [];
    }

    /**
     * @see IAttributes::getAttributes()
     */
    public function getAttributes() : Attributes
    {
        return $this->attributes ?? ($this->attributes = Attributes::fromName($this->name));
    }
}
