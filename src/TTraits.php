<?php

namespace jf\php\generator;

use jf\php\generator\collection\TraitItems;

/**
 * Gestiona los traits del elemento.
 *
 * @mixin ITraits
 */
trait TTraits
{
    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * Colección de los traits.
     *
     * @var TraitItems|NULL
     */
    public ?TraitItems $traits = NULL;

    /**
     * @see ITraits::addTraits()
     */
    public function addTraits(array|string|NULL $traits) : static
    {
        if ($traits)
        {
            $this->getTraits()->addItems($traits);
        }

        return $this;
    }

    /**
     * @see ITraits::buildTraits()
     */
    public function buildTraits() : array
    {
        return $this->traits?->render() ?? [];
    }

    /**
     * @see ITraits::getTraits()
     */
    public function getTraits() : TraitItems
    {
        return $this->traits ?? ($this->traits = TraitItems::fromName($this->name));
    }
}
