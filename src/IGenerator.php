<?php

namespace jf\php\generator;

/**
 * Interfaz para los elementos que generan código tales como clases,
 * enumeraciones, interfaces y traits.
 */
interface IGenerator
{
    /**
     * Construye la sentencia PHP para definir el elemento.
     *
     * @return string[]
     */
    public function buildCode() : array;
}
