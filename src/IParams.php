<?php

namespace jf\php\generator;

use jf\php\generator\collection\Params;

/**
 * Interfaz para las clases que requieren gestionar los parámetros del método.
 */
interface IParams
{
    /**
     * Agrega a la colección los parámetros del método.
     *
     * @param array|string $params Listado de los parámetros del método que serán agregados.
     *
     * @return static
     */
    public function addParams(array|string $params) : static;

    /**
     * Construye el código necesario para cada uno de los parámetros del método.
     *
     * @return array
     */
    public function buildParams() : array;

    /**
     * Devuelve la colección de los parámetros del método.
     *
     * @return Params
     */
    public function getParams() : Params;
}
