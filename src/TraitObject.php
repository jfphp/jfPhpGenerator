<?php

namespace jf\php\generator;

/**
 * Genera un trait.
 */
class TraitObject extends ABase implements IAttributes, IGenerator, IMethods, IProperties, ITraits, IUses
{
    use TAttributes;
    use TGenerator;
    use TMethods;
    use TProperties;
    use TTraits;

    /**
     * Construye la sentencia PHP para definir el elemento.
     *
     * @return string[]
     */
    public function buildCode() : array
    {
        $_code = [];
        if ($this->name)
        {
            $_body = [];
            $this->appendItems($_code, 'renderDoc');
            $this->appendItems($_code, 'buildAttributes');
            $this->appendItems($_body, 'buildTraits');
            $this->appendItems($_body, 'buildProperties', PHP_EOL . PHP_EOL);
            $this->appendItems($_body, 'buildMethods', PHP_EOL . PHP_EOL);
            $_code[] = 'trait ' . $this->name;
            $_code[] = '{';
            if ($_body)
            {
                $_code[] = Formatter::indent(implode(PHP_EOL . PHP_EOL, $_body));
            }
            $_code[] = '}';
        }

        return $_code;
    }
}
