<?php

namespace jf\php\generator;

use Psr\Log\LoggerInterface;

/**
 * Trait para manipular archivos.
 */
trait TFile
{
    use TTranslations;

    /**
     * Contenido del archivo gestionado por la instancia.
     *
     * @var IGenerator[]|string[]
     */
    public array $content = [];

    /**
     * Activa el modo depuración donde el contenido se muestra por pantalla en vez de
     * escribirse en disco.
     *
     * @var bool
     */
    public bool $debug = TRUE;

    /**
     * Ruta del archivo siendo gestionado por la instancia.
     *
     * @var string
     */
    public string $filename = '';

    /**
     * Texto a agregar al final del contenido.
     *
     * @var string
     */
    public string $footer = '';

    /**
     * Texto a agregar al inicio del contenido.
     *
     * @var string
     */
    public string $header = '';

    /**
     * Función a llamar para mostrar las trazas por pantalla.
     *
     * @var LoggerInterface|NULL
     */
    public ?LoggerInterface $logger = NULL;

    /**
     * Indica si se sobrescriben los archivos existentes.
     *
     * @var bool
     */
    public bool $overwrite = FALSE;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return $this->header . implode(PHP_EOL, $this->content) . $this->footer;
    }

    /**
     * Elimina el contenido cargado.
     *
     * @return static
     */
    public function clear() : static
    {
        $this->content = [];

        return $this;
    }

    /**
     * Devuelve el nombre del archivo sin incluir el directorio.
     *
     * @return string
     */
    public function getBasename() : string
    {
        return basename($this->filename);
    }

    /**
     * Devuelve el nombre del directorio del archivo.
     *
     * @return string
     */
    public function getDirname() : string
    {
        return dirname($this->filename);
    }

    /**
     * Carga el contenido del archivo.
     *
     * @return static
     */
    public function load() : static
    {
        $_file = $this->filename;
        if ($_file && is_file($_file))
        {
            $this->content[] = file_get_contents($_file);
        }

        return $this;
    }

    /**
     * Agrega una entrada al registro.
     *
     * @param string $label   Etiqueta del texto.
     * @param mixed  ...$args Argumentos para renderizar la etiqueta.
     *
     * @return void
     */
    public function log(string $label, mixed ...$args) : void
    {
        $this->logger?->info(static::tr($label, ...$args));
    }

    /**
     * Encierra en un recuadro el texto especificado.
     *
     * @param string $text Texto a formatear.
     *
     * @return string
     */
    public static function rect(string $text) : string
    {
        $_sep = str_repeat('─', mb_strlen($text) + 2);

        return sprintf("┌%s┐\n│ %s │\n└%s┘\n", $_sep, $text, $_sep);
    }

    /**
     * Calcula la ruta relativa entre 2 archivos.
     *
     * @param string $from Ruta inicial.
     * @param string $to   Ruta destino.
     *
     * @return string
     */
    public static function relativePath(string $from, string $to) : string
    {
        $_offset = is_dir($from) || is_dir($to) ? 0 : 1;
        $from    = self::splitPath($from);
        $to      = self::splitPath($to);
        while ($from && $to && $from[0] === $to[0])
        {
            array_shift($from);
            array_shift($to);
        }

        return implode(DIRECTORY_SEPARATOR, [ ...array_fill(0, max(0, count($from) - $_offset), '..'), ...$to ]);
    }

    /**
     * Guarda el contenido de la instancia en un archivo.
     *
     * @param string $filename Ruta del archivo donde se guardará el resultado.
     *
     * @return string
     *
     * @throws Exception
     */
    public function save(string $filename = '') : string
    {
        $_content = (string) $this;
        if ($_content)
        {
            if (!$filename)
            {
                $filename = $this->filename ?: 'php://stdout';
            }
            if ($this->debug)
            {
                $this->log("Código generado: {0} bytes\n{1}{2}", strlen($_content), static::rect($filename), $_content);
            }
            else
            {
                $_isWrapper = str_contains($filename, '://');
                if ($_isWrapper)
                {
                    file_put_contents($filename, $_content);
                }
                else
                {
                    $_dirname = dirname($filename);
                    $_umask   = umask(0);
                    if (!is_dir($_dirname))
                    {
                        /** @noinspection PhpUsageOfSilenceOperatorInspection */
                        @Exception::mkdir($_dirname, 0o777, TRUE);
                    }
                    Exception::isWritable($_dirname);
                    $_exists = is_file($filename);
                    if ($_exists && file_get_contents($filename) === $_content)
                    {
                        // Leemos y comparamos para evitar escribir si el archivo es idéntico preservando el TBW de los SSD.
                        $this->log('Archivo `{0}` idéntico', $filename, strlen($_content));
                    }
                    elseif (!$_exists || $this->overwrite)
                    {
                        $this->log('Escribiendo archivo `{0}`: {1} bytes', $filename, strlen($_content));
                        file_put_contents($filename, $_content);
                        chmod($filename, 0o666);
                    }
                    else
                    {
                        $this->log('El archivo `{0}` ya existe', $filename);
                    }
                    umask($_umask);
                }
            }
        }

        return $_content;
    }

    /**
     * Asigna el contenido del archivo.
     *
     * @param string|string[] $content Valor a asignar.
     *
     * @return static
     */
    public function setContent(array|string $content) : static
    {
        $this->content = is_array($content)
            ? $content
            : explode(PHP_EOL, $content);

        return $this;
    }

    /**
     * Asigna la ruta del archivo.
     *
     * @param string $filename Valor a asignar.
     *
     * @return static
     */
    public function setFilename(string $filename) : static
    {
        if (is_file($filename))
        {
            $this->filename = realpath($filename);
            $this->load();
        }
        else
        {
            $this->filename = $filename;
        }

        return $this;
    }

    /**
     * Separa la ruta de un archivo en sus segmentos omitiendo aquellos segmentos que
     * están vacíos.
     *
     * @param string $path Ruta del archivo a separar.
     *
     * @return string[]
     */
    public static function splitPath(string $path) : array
    {
        return preg_split('|[\\\\/]|', $path, -1, PREG_SPLIT_NO_EMPTY);
    }
}
