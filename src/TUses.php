<?php

namespace jf\php\generator;

use jf\assert\Assert;

/**
 * Gestiona los elementos a importar.
 *
 * Formatos soportados:
 *
 * ```
 * - A        --> [ A   => ''          ]
 * - \A       --> [ A   => ''          ]
 * - \A\B     --> [ A\B => ''          ]
 * - \A\B@C   --> [ A\B => C           ]
 * - A<B>     --> [ A   => '', B => '' ]
 * - A@B<C@D> --> [ A   => B,  C => D  ]
 * ```
 */
trait TUses
{
    use TTranslations;

    /**
     * Elementos a importar.
     *
     * La clave es el FQCN del elemento mientras que su valor si está vacío se usará
     * la última parte del FQCN, y en caso contrario se asume como su alias.
     *
     * Ejemplos:
     *
     * ```
     * 'A\B' => ''   --> use A\B; y se usaría B para referenciar al elemento.
     * 'A\B' => 'AB' --> use A\B as AB; y se usaría AB para referenciar al elemento.
     * ```
     *
     * @var array<string,string>
     */
    public array $uses = [];

    /**
     * Devuelve la clave a usar para el elemento a importar.
     *
     * @param string $use  FQCN del elemento a importar.
     * @param array  $uses Listado de elementos a importar.
     *
     * @return string
     */
    private function _addUseKey(string $use, array &$uses) : string
    {
        if ($use[0] === '\\')
        {
            $_use               = $this->addUses($use);
            $_alias             = end($_use);
            $uses[ key($_use) ] = $_alias;
            $_value             = $_alias ?: Formatter::extractName(key($_use));
        }
        else
        {
            $_value = $use;
        }

        return $_value;
    }

    /**
     * Agrega los elementos a importar.
     *
     * @param array|string $uses Elementos a asignar.
     *
     * @return array
     */
    public function addUses(array|string $uses) : array
    {
        if (is_string($uses))
        {
            $uses = [ $uses ];
        }
        $_uses = [];
        foreach ($uses as $_key => $_value)
        {
            // Si la clave es un texto, el valor es el alias, si no, el valor es el
            // nombre del use y se descompone en sus partes para analizarlas.
            if (is_int($_key))
            {
                $_key   = $_value;
                $_value = '';
            }
            if (preg_match('/^([^<]+)<([^>]+)>$/', $_key, $_matches))
            {
                // Si tenemos un tipo \A\Collection<\B\Item> se agrega primero \A\Collection
                // y luego \B\Item y se vuelven a contatenar pero ahora usando los valores
                // luego de analizarse quedando como resultado Collection<Item>.
                $_u1    = $this->_addUseKey($_matches[1], $_uses);
                $_u2    = $this->_addUseKey($_matches[2], $_uses);
                $_value = "$_u1<$_u2>";
            }
            elseif (str_contains($_key, '@'))
            {
                $_key   = explode('@', $_key);
                $_use   = $this->addUses([ $_key[0] => $_key[1] ]);
                $_key   = key($_use);
                $_value = current($_use);
            }
            else
            {
                $_isArray = str_ends_with($_key, '[]');
                if ($_isArray)
                {
                    $_key = substr($_key, 0, -2);
                }
                $_key = ltrim($_key, '\\');
                if ($_value && str_ends_with($_key, '\\' . $_value))
                {
                    $_value = '';
                }
                if (isset($this->uses[ $_key ]))
                {
                    Exception::identical($this->uses[ $_key ], $_value, static::tr('Alias no coinciden: `{0}` !== `{2}`'));
                }
                $this->uses[ $_key ] = $_value;
                if ($_isArray)
                {
                    $_key .= '[]';
                }
            }
            $_uses[ $_key ] = $_value;
        }

        return $_uses;
    }

    /**
     * Extrae los elementos de las cláusulas `use` de manera recursiva.
     *
     * @param array|object $values Valores a analizar.
     *
     * @return string[]
     */
    public static function extractUses(array|object $values) : array
    {
        $_uses = [];
        foreach ($values as $value)
        {
            $_moreUses = NULL;
            if ($value instanceof IUses)
            {
                $_moreUses = $value->getUses();
            }
            elseif ($value && (is_array($value) || is_object($value)))
            {
                $_moreUses = static::extractUses($value);
            }
            if ($_moreUses)
            {
                foreach ($_moreUses as $_fqcn => $_alias) {
                    if (array_key_exists($_fqcn, $_uses)) {
                        Assert::identical($_uses[$_fqcn], $_alias, 'El alias de {0} no coindide: {2} !== {4}', $_fqcn);
                    } else {
                        $_uses[$_fqcn] = $_alias;
                    }
                }
            }
        }

        return $_uses;
    }

    /**
     * Devuelve los elementos a importar.
     *
     * @return array
     */
    public function getUses() : array
    {
        return $this->uses;
    }

    /**
     * Analiza el elemento a importar y lo agrega a la lista de importaciones y
     * devuelve el nombre a usar.
     *
     * @param string $use Nombre a importar que será analizado.
     *
     * @return string
     */
    protected function _parseUse(string $use) : string
    {
        if ($use && $use[0] === '\\')
        {
            $_uses = $this->addUses($use);
            $use   = end($_uses) ?: Formatter::splitName(key($_uses))[1]; // Si no tiene valor lo extraemos de la clave.
        }

        return $use;
    }

    /**
     * Renderiza el valor de los elementos a importar.
     *
     * @param string $namespace Espacio de nombres a eliminar del principio de los nombres.
     *
     * @return string[]
     */
    public function renderUses(string $namespace = '') : array
    {
        $_uses = [];
        if ($namespace && $namespace[0] === '\\')
        {
            $namespace = Formatter::ltrim($namespace);
        }
        foreach ($this->getUses() as $_fqcn => $_alias)
        {
            if ($namespace && !$_alias && str_starts_with($_fqcn, $namespace))
            {
                [ $_ns, ] = Formatter::splitName($_fqcn);
                if ($_ns === $namespace)
                {
                    $_fqcn = '';
                }
            }
            if ($_fqcn)
            {
                $_uses[] = $_alias && !str_ends_with($_fqcn, "\\$_alias") && preg_match('/^\w+/', $_alias)
                    ? sprintf('use %s as %s;', $_fqcn, $_alias)
                    : sprintf('use %s;', $_fqcn);
            }
        }
        sort($_uses, SORT_FLAG_CASE | SORT_NATURAL);

        return $_uses;
    }
}
