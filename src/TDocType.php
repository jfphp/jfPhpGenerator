<?php

namespace jf\php\generator;

/**
 * Documenta el tipo de datos de un elemento.
 */
trait TDocType
{
    use TType;

    /**
     * Agrega la etiqueta para documentar el tipo de datos.
     *
     * @param array  $tags    Listado de etiquetas del elemento.
     * @param string $tagname Nombre de la etiqueta usada para documentar el tipo de dato.
     *
     * @return Tags|NULL
     */
    public function addDocType(array &$tags, string $tagname = 'var') : ?Tags
    {
        $_type = $this->getDocType();

        return $_type
            ? ($tags[ $tagname ] = new Tags([ 'tags' => [ [ 'name' => $tagname, 'value' => $_type ] ] ]))
            : ($tags[ $tagname ] ?? NULL);
    }

    /**
     * Devuelve el tipo de datos tal como se debe mostrar en la documentación.
     *
     * @return string
     */
    public function getDocType() : string
    {
        if ($this->intersectionType)
        {
            $_type = $this->intersectionType;
            self::sortTypes($_type);
            $_type = implode('&', $_type);
        }
        elseif ($_type = $this->getType())
        {
            $_type = array_unique(array_filter($_type));
            if (in_array('mixed', $_type))
            {
                $_type = 'mixed';
            }
            else
            {
                // Convertimos a mayúsculas los valores FALSE y TRUE como constantes.
                foreach ([ 'false', 'true' ] as $_t)
                {
                    $_i = array_search($_t, $_type);
                    if ($_i !== FALSE)
                    {
                        $_type[ $_i ] = strtoupper($_t);
                    }
                }
                self::sortTypes($_type);
                $_null = $this->null;
                if ($_null)
                {
                    $_type[] = $_null;
                }
                $_type = implode('|', $_type);
            }
        }
        elseif ($this->value === NULL && $this->null)
        {
            $_type = $this->null;
        }
        else
        {
            $_type = '';
        }

        return $_type;
    }
}
