<?php

namespace jf\php\generator\collection;

use jf\php\generator\Param;

/**
 * Colección de parámetros de un método o función.
 *
 * @extends ACollection<Param>
 */
class Params extends ACollection
{
    use TType;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct(Param::class);
    }

    /**
     * @inheritdoc
     */
    public function sort() : static
    {
        // Se evita el ordenado de los parámetros ya que hacerlo cambiaría la firma del método.

        return $this;
    }
}
