<?php

namespace jf\php\generator\collection;

use jf\assert\php\BadMethodCallException;
use jf\php\generator\Attribute;

/**
 * Colección de atributos.
 *
 * @extends ACollection<Attribute>
 */
class Attributes extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct(string $classname = Attribute::class)
    {
        BadMethodCallException::isA($classname, Attribute::class, TRUE);
        parent::__construct($classname);
    }
}
