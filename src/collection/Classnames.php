<?php

namespace jf\php\generator\collection;

use jf\php\generator\Classname;

/**
 * Colección de nombres de clases.
 *
 * @extends ACollection<Classname>
 */
class Classnames extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct(Classname::class);
    }

    /**
     * @inheritdoc
     */
    public function addItems(array|string $items) : static
    {
        if ($items)
        {
            if (is_string($items))
            {
                $items = [ $items ];
            }
            foreach ($items as $name => $alias)
            {
                if (!$alias)
                {
                    $alias = '';
                }
                if (is_string($name))
                {
                    $items[ $name ] = [
                        'alias' => $alias,
                        'name'  => $name
                    ];
                }
                elseif (is_string($alias))
                {
                    $items[ $name ] = [ 'name' => $alias ];
                }
            }
        }

        return parent::addItems($items);
    }

    /**
     * @inheritdoc
     */
    public function sort() : static
    {
        $this->sortValues(fn(Classname $classname1, Classname $classname2) => strcasecmp((string) $classname1, (string) $classname2));

        return $this;
    }
}
