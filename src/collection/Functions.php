<?php

namespace jf\php\generator\collection;

use jf\assert\php\BadMethodCallException;
use jf\php\generator\FunctionItem;

/**
 * Colección de funciones.
 *
 * @extends ACollection<FunctionItem>
 */
class Functions extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct(string $classname = FunctionItem::class)
    {
        BadMethodCallException::isA($classname, FunctionItem::class, TRUE);
        parent::__construct($classname);
    }
}
