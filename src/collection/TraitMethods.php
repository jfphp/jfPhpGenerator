<?php

namespace jf\php\generator\collection;

use jf\assert\php\BadMethodCallException;
use jf\php\generator\TraitMethod;

/**
 * Colección de traits que se agregan a una clases o trait.
 *
 * @extends ACollection<TraitMethod>
 */
class TraitMethods extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct(string $classname = TraitMethod::class)
    {
        BadMethodCallException::isA($classname, TraitMethod::class, TRUE);
        parent::__construct($classname);
    }
}
