<?php

namespace jf\php\generator\collection;

use jf\php\generator\TraitItem;

/**
 * Colección de traits que se agregan a un elemento.
 *
 * @extends ACollection<TraitItem>
 */
class TraitItems extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct(TraitItem::class);
    }

    /**
     * @inheritdoc
     */
    public function addItems(array|string $items) : static
    {
        if (is_string($items))
        {
            $items = [ $items ];
        }
        foreach ($items as $_key => $_value)
        {
            if (!is_array($_value))
            {
                $items[ $_key ] = is_string($_key)
                    ? [ 'alias' => $_value ?? '', 'name' => $_key ]
                    : [ 'name' => $_value ?? '' ];
            }
        }

        return parent::addItems($items);
    }

    /**
     * @inheritdoc
     */
    public function sort() : static
    {
        usort(
            $this->_items,
            fn(TraitItem $trait1, TraitItem $trait2) => match (TRUE)
            {
                empty($trait1->methods) => empty($trait2->methods)
                    ? strcasecmp($trait1->alias ?: $trait1->name, $trait2->alias ?: $trait2->name)
                    : -1,
                empty($trait2->methods) => 1,
                default                 => strcasecmp($trait1->alias ?: $trait1->name, $trait2->alias ?: $trait2->name),
            }
        );

        return $this;
    }
}
