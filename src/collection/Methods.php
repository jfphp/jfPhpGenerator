<?php

namespace jf\php\generator\collection;

use jf\assert\Assert;
use jf\php\generator\Method;
use jf\php\generator\TTranslations;

/**
 * Colección de métodos de una clase.
 *
 * @extends ACollection<Method>
 */
class Methods extends ACollection
{
    use TTranslations;

    /**
     * @inheritdoc
     */
    public function __construct(string $classname = Method::class)
    {
        Assert::isA($classname, Method::class, TRUE);
        parent::__construct($classname);
    }

    /**
     * Agrega el método constructor.
     *
     * @param array $config Configuración adicional.
     *
     * @return static
     */
    public function addConstructor(array $config = []) : static
    {
        return $this->push(
            [
                'body'        => '',
                'description' => static::tr('Constructor de la clase `{0}`', $this->name),
                'name'        => '__construct',
                'override'    => TRUE,
                'scope'       => 'public',
                'type'        => NULL,
                ...$config
            ]
        );
    }

    /**
     * Agrega el método desstructor.
     *
     * @param array $config Configuración adicional.
     *
     * @return static
     */
    public function addDestructor(array $config = []) : static
    {
        return $this->push(
            [
                'description' => static::tr('Destructor de la clase `{0}`', $this->name),
                'name'        => '__destruct',
                'override'    => TRUE,
                'scope'       => 'public',
                'type'        => NULL,
                ...$config
            ]
        );
    }

    /**
     * Ordena los métodos por su nombre.
     *
     * @return static
     */
    public function sort() : static
    {
        $this->sortKeys(
            fn(string $method1, string $method2) => match (TRUE)
            {
                str_starts_with($method1, '__') => str_starts_with($method2, '__')
                    ? strcasecmp($method1, $method2)
                    : -1,
                str_starts_with($method2, '__') => 1,
                default                         => strcasecmp(ltrim($method1, '_'), ltrim($method2, '_'))
            }
        );

        return $this;
    }
}
