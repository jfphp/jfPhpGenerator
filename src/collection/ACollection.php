<?php

namespace jf\php\generator\collection;

use jf\Collection\ANamed;
use jf\php\generator\ABase;
use jf\php\generator\Exception;

/**
 * Clase base de las colecciones.
 *
 * @template T of ABase
 *
 * @extends ANamed<T>
 */
abstract class ACollection extends ANamed
{
    /**
     * Valores por defecto a aplicar a los elementos antes de construirlos.
     *
     * @var array<string,scalar>
     */
    public array $defaults = [];

    /**
     * Nombre de la instancia que gestiona la colección.
     *
     * @var string
     */
    public string $name = '';

    /**
     * @inheritdoc
     */
    public function __clone() : void
    {
        foreach ($this->_items as $_key => $_item)
        {
            if (is_object($_item))
            {
                $this->_items[ $_key ] = clone $_item;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function addItems(array $items) : static
    {
        $_class    = $this->classname;
        $_defaults = $this->defaults;
        $_items    = [];
        foreach ($items as $_name => $_item)
        {
            if (is_object($_item))
            {
                Exception::isA($_item, $_class);
                $_items[ $_item->name ] = $_item;
            }
            else
            {
                if (is_string($_item))
                {
                    $_item = [ 'description' => $_item ];
                }
                $_current = $_item['name'] ?? NULL;
                if (!$_current)
                {
                    $_item['name'] = $_current = $_name;
                }
                foreach ($_defaults as $_k => $_v)
                {
                    if (!array_key_exists($_k, $_item))
                    {
                        $_item[ $_k ] = $_v;
                    }
                }
                $_items[ $_current ] = $_item;
            }
        }

        return parent::addItems($_items);
    }

    /**
     * Construye una instancia a partir y asigna el nombre que identifica al
     * propietario de la colección.
     *
     * @param string $name Nombre de la instancia que gestiona la colección.
     *
     * @return static
     */
    public static function fromName(string $name) : static
    {
        $collection       = new static();
        $collection->name = $name;

        return $collection;
    }

    /**
     * Renderiza cada uno de los métodos.
     *
     * @return string[]
     */
    public function render() : array
    {
        return array_values(array_unique(array_map('strval', $this->sort()->values())));
    }

    /**
     * Ordena los métodos por su nombre.
     *
     * @return static
     */
    public function sort() : static
    {
        $_regexp = '/^[^a-zA-Z\d]+/';
        $this->sortKeys(
            fn(string $name1, string $name2) => strcasecmp(
                preg_replace($_regexp, '', $name1),
                preg_replace($_regexp, '', $name2)
            )
        );

        return $this;
    }
}
