<?php

namespace jf\php\generator\collection;

/**
 * Trait para agregar un tipo por defecto en los elementos de algunas colecciones.
 */
trait TType
{
    /**
     * Tipo por defecto a asignar a los elementos que no tengan el tipo asignado.
     *
     * @var string
     */
    public string $type = 'string';

    /**
     * @see ACollection::createItem()
     */
    protected function createItem(array $values = []) : object
    {
        $_item = parent::createItem($values);
        if ($_item && $this->type && empty($_item->type))
        {
            // Si no se ha asignado un tipo, le asignamos el tipo por defecto.
            $_item->addType($this->type);
        }

        return $_item;
    }
}
