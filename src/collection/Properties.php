<?php

namespace jf\php\generator\collection;

use jf\assert\Assert;
use jf\php\generator\Property;

/**
 * Colección de propiedades de una clase.
 *
 * @extends ACollection<Property>
 */
class Properties extends ACollection
{
    use TType;

    /**
     * @inheritdoc
     */
    public function __construct(string $classname = Property::class)
    {
        Assert::isA($classname, Property::class, TRUE);
        parent::__construct($classname);
    }

    /**
     * @inheritdoc
     */
    public function sort() : static
    {
        $_regexp = '/^[^a-zA-Z\d]+/';
        $_cmp    = fn(string $name1, string $name2) => strcasecmp(
            preg_replace($_regexp, '', $name1),
            preg_replace($_regexp, '', $name2)
        );

        uasort(
            $this->_items,
            // Ordena las propiedades por orden alfabético poniendo las constantes de primero
            fn(Property $property1, Property $property2) => match (TRUE)
            {
                $property1->const => $property2->const ? $_cmp($property1->name, $property2->name) : -1,
                $property2->const => 1,
                default           => $_cmp($property1->name, $property2->name)
            }
        );

        return $this;
    }
}
