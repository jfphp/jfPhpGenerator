<?php

namespace jf\php\generator\collection;

use jf\assert\php\BadMethodCallException;
use jf\php\generator\ClassObject;

/**
 * Colección de clases.
 *
 * @extends ACollection<ClassObject>
 */
class Classes extends ACollection
{
    /**
     * @inheritdoc
     */
    public function __construct(string $classname = ClassObject::class)
    {
        BadMethodCallException::isA($classname, ClassObject::class, TRUE);
        parent::__construct($classname);
    }
}
