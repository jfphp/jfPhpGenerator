<?php

namespace jf\php\generator;

/**
 * Gestiona la descripción del elemento.
 */
trait TDescription
{
    /**
     * Descripción del elemento.
     *
     * @var string
     */
    public string $description = '';

    /**
     * Renderiza el valor de la descripción.
     *
     * @return string
     */
    public function renderDescription() : string
    {
        $_description = trim($this->description, " \r\n\t\v\0\xB.;,:");
        if ($_description)
        {
            $_description = ucfirst(Formatter::wordwrap($_description));
            if (!str_ends_with($_description, '```'))
            {
                $_description .= '.';
            }
        }

        return $_description;
    }

    /**
     * Asigna la descripción del elemento.
     *
     * @param string|string[] $description Valor a asignar.
     *
     * @return static
     */
    public function setDescription(array|string $description) : static
    {
        $this->description = is_array($description)
            ? implode(PHP_EOL, $description)
            : $description;

        return $this;
    }
}
