<?php

namespace jf\php\generator;

/**
 * Trait para las clases que generan código de elementos tales como clases,
 * enumeraciones, interfaces y traits.
 *
 * Permite implementar la interfaz `IGenerator`.
 *
 * @mixin IGenerator
 */
trait TGenerator
{
    use TDocDescription;
    use TDocTags
    {
        TDocTags::renderTags as private _renderTags;
    }
    use TName
    {
        TName::setName as private _setName;
    }
    use TUses
    {
        TUses::getUses as private _getUses;
    }

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return Formatter::rtrimBlanks(implode(PHP_EOL, $this->buildCode()));
    }

    /**
     * Agrega el resultado de un método a la lista.
     *
     * @param array  $list   Lista donde agregar los elementos.
     * @param string $method Método a ejecutar.
     * @param string $glue   Texto a usar para unir el resultado.
     *
     * @return static
     */
    public function appendItems(array &$list, string $method, string $glue = PHP_EOL) : static
    {
        $items = $this->$method();
        if ($items)
        {
            $list[] = is_array($items)
                ? implode($glue, $items)
                : $items;
        }

        return $this;
    }

    /**
     * @see IGenerator::buildCode()
     */
    abstract public function buildCode() : array;

    /**
     * @see TUses::getUses()
     */
    public function getUses() : array
    {
        return [ ...$this->_getUses(), ...static::extractUses($this) ];
    }

    /**
     * @inheritdoc
     */
    public function renderTags(?array $tags = NULL) : array
    {
        $_tags = [];
        $this->addDocDescription($_tags);
        $_others = $this->_renderTags($tags);
        if ($_tags && $_others)
        {
            $_tags[] = NULL;
        }

        return [ ...$_tags, ...$_others ];
    }

    /**
     * @inheritdoc
     */
    public function setName(string $name) : static
    {
        if ($name)
        {
            if ($this->package)
            {
                $this->_setName(ltrim($name, '\\'));
            }
            else
            {
                $this->_setName(
                    str_starts_with($name, '\\')
                        ? $name
                        : "\\$name"
                );
            }
        }

        return $this;
    }
}
