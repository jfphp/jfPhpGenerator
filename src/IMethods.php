<?php

namespace jf\php\generator;

use jf\php\generator\collection\Methods;

/**
 * Interfaz para las clases que requieren gestionar los métodos.
 */
interface IMethods
{
    /**
     * Agrega a la colección los métodos.
     *
     * @param array|string $methods Listado de los métodos que serán agregados.
     *
     * @return static
     */
    public function addMethods(array|string $methods) : static;

    /**
     * Construye el código necesario para cada uno de los métodos.
     *
     * @return array
     */
    public function buildMethods() : array;

    /**
     * Devuelve la colección de los métodos.
     *
     * @return Methods
     */
    public function getMethods() : Methods;
}
