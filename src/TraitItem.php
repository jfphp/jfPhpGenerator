<?php

namespace jf\php\generator;

use jf\Collection\IItem;
use jf\php\generator\collection\TraitMethods;

/**
 * Renderiza cada uno de los items que representan un trait y que generan las
 * sentencias `use XXXX;` en la definición de las clases.
 */
class TraitItem extends ABase implements IItem, IUses
{
    use TDocTags;
    use TName;

    /**
     * Colección de métodos del trait a importar que serán modificados.
     *
     * @var TraitMethods|NULL
     */
    public ?TraitMethods $methods = NULL;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_code = [];
        $_name = $this->alias ?: $this->getName();
        if ($_name)
        {
            $_tags = $this->renderDoc();
            if ($_tags)
            {
                array_push($_code, ...$_tags);
            }
            if ($this->methods)
            {
                $this->methods->each(fn($method) => $method->namespace = $_name);
                $_code[] = 'use ' . $_name;
                $_code[] = '{';
                array_push($_code, ...Formatter::indent($this->buildMethods()));
                $_code[] = '}';
            }
            else
            {
                $_code[] = 'use ' . $_name . ';';
            }
        }

        return Formatter::rtrimBlanks(implode(PHP_EOL, $_code));
    }

    /**
     * Agrega la configuración para algunos métodos que importarán desde el trait.
     *
     * @param array $methods Configuración de los métodos del trait.
     *
     * @return static
     */
    public function addMethods(array $methods) : static
    {
        if ($methods)
        {
            $this->getMethods()->addItems($methods);
        }

        return $this;
    }

    /**
     * Construye el listado de métodos.
     *
     * @return string[]
     */
    public function buildMethods() : array
    {
        return $this->methods?->render() ?? [];
    }

    /**
     * Devuelve el valor de la propiedad `methods` y la inicializa en caso de ser necesario.
     *
     * @return TraitMethods
     */
    public function getMethods() : TraitMethods
    {
        return $this->methods ?? ($this->methods = TraitMethods::fromName($this->name));
    }

    /**
     * @inheritdoc
     */
    public function getUses() : array
    {
        return $this->getNamespaceUses();
    }
}
