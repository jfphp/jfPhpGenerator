<?php

namespace jf\php\generator;

use jf\assert\Assert;

/**
 * Excepciones lanzadas por la aplicación.
 */
class Exception extends Assert
{
    /**
     * @inheritdoc
     */
    public const CODE = 653390590;
}
