<?php

namespace jf\php\generator;

/**
 * Representa un grupo de etiquetas para documentar un elemento.
 */
class Tags extends ABase
{
    use TTags;

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $tags = $this->tags;
        if ($tags)
        {
            $rows = [];
            foreach ($tags as $tag)
            {
                $rows[] = [
                    $tag->getName(),
                    $tag->value,
                    $tag->getVariable(),
                    $tag->renderDescription()
                ];
            }
            $_tags = implode(
                PHP_EOL,
                array_map(
                    fn($row) => trim(implode(' ', $row)),
                    Formatter::formatRows($rows)
                )
            );
        }
        else
        {
            $_tags = '';
        }

        return $_tags;
    }
}
