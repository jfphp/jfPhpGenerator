<?php

namespace jf\php\generator;

use jf\Collection\IItem;

/**
 * Gestiona un nombre que puede pertenecer a un espacio de nombres y tener un alias.
 *
 * @mixin IItem
 */
trait TName
{
    /**
     * Alias a usar para el trait.
     *
     * @var string
     */
    public string $alias = '';

    /**
     * Nombre del elemento.
     *
     * @var string
     */
    public string $name = '';

    /**
     * Espacio de nombres del elemento.
     *
     * @var string
     */
    public string $namespace = '';

    /**
     * Espacio de nombres del paquete.
     *
     * @var string
     */
    public string $package = '';

    /**
     * Subespacio de nombres del elemento.
     *
     * @var string
     */
    public string $subnamespace = '';

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return $this->alias ?: $this->getName();
    }

    /**
     * @see IItem::getCollectionKey()
     */
    public function getCollectionKey() : string
    {
        return implode('\\', array_filter([ $this->package, $this->namespace, $this->subnamespace, $this->name, $this->alias ]));
    }

    /**
     * Devuelve el FQCN del elemento actual.
     *
     * @return string
     */
    public function getFqcn() : string
    {
        return Formatter::ltrim($this->getNamespace() . '\\' . $this->name);
    }

    /**
     * Devuelve el nombre del elemento.
     *
     * @return string
     */
    public function getName() : string
    {
        return Formatter::ltrim(
            implode(
                '\\',
                array_filter([
                    $this->subnamespace,
                    $this->name
                ])
            )
        );
    }

    /**
     * Devuelve el nombre con su alias.
     *
     * @return array<string,string>
     */
    public function getNameAndAliasUses() : array
    {
        return $this->name
            ? [ $this->getName() => $this->alias ]
            : [];
    }

    /**
     * Devuelve el espacio de nombres del elemento.
     *
     * @return string
     */
    public function getNamespace() : string
    {
        return implode(
            '\\',
            array_filter([
                $this->package,
                $this->namespace,
                $this->subnamespace
            ])
        );
    }

    /**
     * Devuelve el nombre como un valor que puede ser usado para importarse mediante
     * `use`.
     *
     * @return array<string,string>
     */
    public function getNamespaceUses() : array
    {
        $_uses = [];
        $_ns   = $this->getNamespace();
        if ($_ns || $this->name)
        {
            $_alias = $this->alias;
            $_fqcn  = $this->getFqcn();
            if ($_ns === '\\' || $_alias || str_contains($_fqcn, '\\'))
            {
                $_uses[ $_fqcn ] = $_alias;
            }
        }

        return $_uses;
    }

    /**
     * Devuelve el nombre como un valor que puede ser usado para importarse mediante
     * `use`.
     *
     * @return array<string,string>
     */
    public function getNameUses() : array
    {
        $_ns = $this->getNamespace();

        return $_ns || $this->name
            ? [ $_ns => $this->name ]
            : [];
    }

    /**
     * Asigna el nombre del elemento.
     *
     * @param string $name Nombre a asignar.
     *
     * @return static
     */
    public function setName(string $name) : static
    {
        if ($name && str_contains($name, '\\'))
        {
            [ $_ns, $this->name ] = Formatter::splitName($name);
            // Si se llama a \Name hacemos que `namespace = \` y `name = Name` y
            // cuando se renderize se detectará que el alias no hace falta.
            if ($name[0] === '\\')
            {
                $this->namespace = $_ns ?: '\\';
            }
            else
            {
                $this->subnamespace = $_ns;
            }
        }
        else
        {
            $this->name = $name;
        }

        return $this;
    }

    /**
     * Asigna el valor de la propiedad `namespace`:
     *
     * ```
     * Espacio de nombres del elemento.
     * ```
     *
     * @param string $namespace Valor a asignar.
     *
     * @return static
     */
    public function setNamespace(string $namespace) : static
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Asigna el nombre del paquete.
     *
     * @param string $package Valor a asignar.
     *
     * @return static
     */
    public function setPackage(string $package) : static
    {
        $this->package = $package;
        if ($package && $this->namespace === '\\')
        {
            $this->setNamespace('');
        }

        return $this;
    }

    /**
     * Asigna el valor de la propiedad `subnamespace`:
     *
     * ```
     * Subespacio de nombres del elemento.
     * ```
     *
     * @param string $subnamespace Valor a asignar.
     *
     * @return static
     */
    public function setSubnamespace(string $subnamespace) : static
    {
        $this->subnamespace = $subnamespace;

        return $this;
    }
}
