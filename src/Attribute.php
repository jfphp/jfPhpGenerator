<?php

namespace jf\php\generator;

/**
 * Representa un atributo de PHP.
 */
class Attribute extends Classname
{
    /**
     * Argumentos del atributo.
     *
     * @var string[]
     */
    public array $arguments = [];

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_args = $this->arguments;
        if ($_args)
        {
            $_numeric = [];
            $_named   = [];
            foreach ($_args as $_index => $_arg)
            {
                $_arg = Formatter::formatValue($_arg);
                if (is_numeric($_index))
                {
                    $_numeric[] = $_arg;
                }
                else
                {
                    $_named[] = $_index . ': ' . $_arg;
                }
            }
            $_args = [ ...$_numeric, ...$_named ];
        }

        return $_args
            ? sprintf('#[%s(%s)]', $this->name, implode(', ', $_args))
            : sprintf('#[%s]', $this->name);
    }
}