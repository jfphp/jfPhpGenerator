<?php

namespace jf\php\generator;

/**
 * Genera una enumeración.
 */
class EnumObject extends ABase implements IAttributes, IGenerator, IImplements, IMethods, IProperties, ITraits, IUses
{
    use TAttributes;
    use TGenerator;
    use TImplements;
    use TMethods;
    use TProperties;
    use TTraits;

    /**
     * Tipo de la enumeración.
     *
     * @var string
     */
    public string $type = '';

    /**
     * @inheritdoc
     */
    public function buildCode() : array
    {
        $_code = [];
        if ($this->name)
        {
            $this->appendItems($_code, 'renderDoc');
            $this->appendItems($_code, 'buildAttributes');
            $_def  = [ 'enum', $this->name ];
            $_type = $this->getType();
            if ($_type)
            {
                $_def[] = ':';
                $_def[] = $_type;
            }
            $this->appendItems($_def, 'renderImplements');
            $_code[] = implode(' ', $_def);
            $_code[] = '{';
            $_body   = [];
            $this->appendItems($_body, 'buildTraits');
            $this->appendItems($_body, 'buildProperties', PHP_EOL . PHP_EOL);
            $this->appendItems($_body, 'buildMethods', PHP_EOL . PHP_EOL);
            if ($_body)
            {
                $_code[] = Formatter::indent(implode(PHP_EOL . PHP_EOL, $_body));
            }
            $_code[] = '}';
        }

        return $_code;
    }

    /**
     * Crea un setter que puede ser agregado a una clase que usa la enumeración.
     *
     * @param string $name   Nombre del parámetro y la propiedad.
     * @param array  $config Configuración a aplicar para construir el setter.
     * @param string $tpl    Plantilla a usar para renderizar el método.
     *
     * @return Method
     *
     * @throws Exception
     */
    public function buildSetter(string $name = '', array $config = [], string $tpl = 'enum-setter-nullable.yaml') : Method
    {
        $_enum = $this->name;
        if (!$name)
        {
            $name = lcfirst(self::pascalize($_enum));
        }
        $_type     = $this->getType();
        $_replaces = [
            '[[desc]]'  => lcfirst($this->description),
            '[[enum]]'  => $_enum,
            '[[name]]'  => $name,
            '[[Name]]'  => ucfirst($name),
            '[[type]]'  => $_type,
            '[[value]]' => $_type === 'string' ? '' : 1
        ];
        if (str_ends_with($tpl, '.yaml'))
        {
            if (!is_file($tpl))
            {
                $_tpl = dirname(__DIR__) . '/tpl/' . $tpl;
                if (is_file($_tpl))
                {
                    $tpl = $_tpl;
                }
            }
            Exception::isFile($tpl);
            $tpl = file_get_contents($tpl);
        }

        return Method::fromArray(array_replace_recursive(yaml_parse(strtr($tpl, $_replaces)), $config));
    }

    /**
     * Devuelve el tipo de la enumeración.
     *
     * @return string
     */
    public function getType() : string
    {
        if (!$this->type)
        {
            $_types = array_unique(
                array_map(
                    fn(Property $property) => Property::inferType($property->value),
                    $this->getProperties()->values()
                )
            );
            Exception::arrayCountLessOrEqual(
                $_types,
                1,
                _('La enumeración `{0}` tiene más de un tipo de valor: {2}'),
                $this->name,
                json_encode($_types)
            );
            $_type = strtolower(current($_types));
            if ($_type !== 'null')
            {
                $this->type = $_type;
            }
        }

        return $this->type;
    }

    /**
     * @inheritdoc
     */
    protected function _modifyProperty(Property $property) : bool
    {
        $property->assign(
            [
                'const'  => FALSE,
                'prefix' => '',
                'scope'  => 'case',
                'type'   => NULL
            ]
        );

        return TRUE;
    }
}
