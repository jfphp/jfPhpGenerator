<?php

namespace jf\php\generator;

/**
 * Clase para manipular archivos.
 */
class Generator extends File implements IUses
{
    use TUses;

    /**
     * @inheritdoc
     */
    public string $footer = PHP_EOL;

    /**
     * @inheritdoc
     */
    public string $header = "<?php\n\n";

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $_lines     = [];
        $_namespace = '';
        foreach ($this->content as $_content)
        {
            if (is_object($_content) && method_exists($_content, 'getNamespace'))
            {
                $_ns = Formatter::ltrim($_content->getNamespace());
                if ($_ns && $_ns !== '\\')
                {
                    if ($_namespace)
                    {
                        Exception::identical(
                            $_namespace,
                            $_ns,
                            _('Todos los elemenos deben pertenecer al mismo espacio de nombres')
                        );
                    }
                    else
                    {
                        $_namespace = $_ns;
                    }
                }
            }
            $_lines[] = (string) $_content;
        }
        $_uses = $this->renderUses($_namespace);
        if ($_uses)
        {
            array_unshift($_lines, implode(PHP_EOL, $_uses));
        }
        if ($_namespace)
        {
            array_unshift($_lines, 'namespace ' . $_namespace . ';');
        }
        $_code = $this->header;
        if ($_lines)
        {
            $_code .= implode(PHP_EOL . PHP_EOL, array_filter($_lines));
        }
        $_code .= $this->footer;

        return $_code;
    }

    /**
     * Agrega una clase al archivo que será generado.
     *
     * @param ClassObject|array $class Configuración para construir la clase.
     *
     * @return ClassObject
     */
    public function addClass(ClassObject|array $class) : ClassObject
    {
        return ($this->content[] = $class instanceof ClassObject
            ? $class
            : new ClassObject($class));
    }

    /**
     * Agrega una enumeración al archivo que será generado.
     *
     * @param EnumObject|array $enum Configuración para construir la enumeración.
     *
     * @return EnumObject
     */
    public function addEnum(EnumObject|array $enum) : EnumObject
    {
        return ($this->content[] = $enum instanceof EnumObject
            ? $enum
            : new EnumObject($enum));
    }

    /**
     * Agrega una interfaz al archivo que será generado.
     *
     * @param InterfaceObject|array $interface Configuración para construir la interfaz.
     *
     * @return InterfaceObject
     */
    public function addInterface(InterfaceObject|array $interface) : InterfaceObject
    {
        return ($this->content[] = $interface instanceof InterfaceObject
            ? $interface
            : new InterfaceObject($interface));
    }

    /**
     * Agrega un trait al archivo que será generado.
     *
     * @param TraitObject|array $trait Configuración para construir el trait.
     *
     * @return TraitObject
     */
    public function addTrait(TraitObject|array $trait) : TraitObject
    {
        return ($this->content[] = $trait instanceof TraitObject
            ? $trait
            : new TraitObject($trait));
    }

    /**
     * Genera un listado de archivos a partir de una configuración y devuelve el
     * contenido de cada archivo generado.
     *
     * @param string $outdir Ruta donde se escribirán los archivos generados.
     * @param array  $config Configuración de los archivos a generar.
     *
     * @return array<string,string>
     *
     * @throws Exception
     */
    public static function generate(string $outdir, array $config) : array
    {
        $_check     = static fn(array &$item, $key, $value) => empty($item[ $key ]) ? $item[ $key ] = $value : NULL;
        $_generated = [];
        $_namespace = $config['namespace'] ?? '';
        $_package   = $config['package'] ?? '';
        foreach ([ 'classes', 'enumerations', 'interfaces', 'traits' ] as $_type)
        {
            if (!empty($config[ $_type ]))
            {
                foreach ($config[ $_type ] as $_name => $_item)
                {
                    if (str_contains($_name, '\\'))
                    {
                        [ $_item['subnamespace'], $_name ] = Formatter::splitName($_name);
                    }
                    $_check($_item, 'package', $_package);
                    $_check($_item, 'name', $_name);
                    $_check($_item, 'namespace', $_namespace);
                    $_file      = new static($config['file'] ?? []);
                    $_generator = match ($_type)
                    {
                        'enumerations' => $_file->addEnum($_item),
                        'interfaces'   => $_file->addInterface($_item),
                        'traits'       => $_file->addTrait($_item),
                        default        => $_file->addClass($_item),
                    };
                    $_outfile   = ltrim(str_replace([ $_package, '\\' ], [ '', '/' ], $_generator->getFqcn()) . '.php', '/');
                    if ($outdir)
                    {
                        $_outfile = $outdir . '/' . $_outfile;
                    }
                    if ($_outfile !== __FILE__ || $_file->debug)
                    {
                        $_generated[ $_outfile ] = $_file->save($_outfile);
                    }
                }
            }
        }

        return $_generated;
    }

    /**
     * @inheritdoc
     */
    public function getUses() : array
    {
        return static::extractUses($this->content);
    }
}