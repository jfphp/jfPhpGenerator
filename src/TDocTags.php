<?php

namespace jf\php\generator;

/**
 * Gestiona las etiquetas PHPDoc del elemento.
 */
trait TDocTags
{
    use TTags;

    /**
     * Construye los valores para documentar el elemento.
     *
     * @return string[]
     */
    public function renderDoc() : array
    {
        $_tags = $this->renderTags();

        return $_tags
            ? [
                '/**',
                ...array_map(
                    fn($tag) => $tag ? Formatter::indent((string) $tag, 1, ' * ') : ' *',
                    array_values($_tags)
                ),
                ' */'
            ]
            : [];
    }
}
