<?php

namespace jf\php\generator;

/**
 * Interfaz para aquellas clases cuyo contenido que pueden hacer referencia a otros
 * elementos en otros espacios de nombres.
 */
interface IUses
{
    /**
     * Extrae los elementos a importar a partir del contenido de la instancia.
     *
     * @return array
     */
    public function getUses() : array;
}
