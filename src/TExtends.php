<?php

namespace jf\php\generator;

use jf\php\generator\collection\Classnames;

/**
 * Gestiona los elementos de los que extiende la clase o interfaz del elemento.
 *
 * @mixin IExtends
 */
trait TExtends
{
    /**
     * Colección de los elementos de los que extiende la clase o interfaz.
     *
     * @var Classnames|NULL
     */
    public ?Classnames $extends = NULL;

    /**
     * @see TName::$name
     */
    public string $name = '';

    /**
     * @see IExtends::addExtends()
     */
    public function addExtends(array|string|NULL $extends) : static
    {
        if ($extends)
        {
            $this->getExtends()->addItems($extends);
        }

        return $this;
    }

    /**
     * @see IExtends::buildExtends()
     */
    public function buildExtends() : array
    {
        return $this->extends?->render() ?? [];
    }

    /**
     * @see IExtends::getExtends()
     */
    public function getExtends() : Classnames
    {
        return $this->extends ?? ($this->extends = Classnames::fromName($this->name));
    }

    /**
     * Renderiza el valor del trait.
     *
     * @return string
     */
    public function renderExtends() : string
    {
        return $this->extends
            ? 'extends ' . implode(', ', $this->buildExtends())
            : '';
    }
}
