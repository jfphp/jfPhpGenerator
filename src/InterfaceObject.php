<?php

namespace jf\php\generator;

/**
 * Genera una interfaz.
 */
class InterfaceObject extends ABase implements IAttributes, IExtends, IGenerator, IMethods, IProperties, IUses
{
    use TAttributes;
    use TExtends;
    use TGenerator;
    use TMethods;
    use TProperties;

    /**
     * Indica si la interfaz puede extenderse.
     *
     * @var bool|NULL
     */
    public ?bool $final = NULL;

    /**
     * Construye la sentencia PHP para definir el elemento.
     *
     * @return string[]
     */
    public function buildCode() : array
    {
        $_code = [];
        if ($this->name)
        {
            $this->appendItems($_code, 'renderDoc');
            $_def = [];
            if ($this->final)
            {
                $_def[] = 'final';
            }
            $_def[] = 'interface';
            $_def[] = $this->name;
            $this->appendItems($_def, 'renderExtends');
            $_code[] = implode(' ', $_def);
            $_code[] = '{';
            $_body   = [];
            $this->appendItems($_body, 'buildProperties', PHP_EOL . PHP_EOL);
            $this->appendItems($_body, 'buildMethods', PHP_EOL . PHP_EOL);
            if ($_body)
            {
                $_code[] = Formatter::indent(implode(PHP_EOL . PHP_EOL, $_body));
            }
            $_code[] = '}';
        }

        return $_code;
    }

    /**
     * @inheritdoc
     */
    protected function _modifyMethod(Method $method) : bool
    {
        $method->assign(
            [
                'abstract' => NULL,
                'body'     => NULL
            ]
        );

        return TRUE;
    }

    /**
     * @inheritdoc
     */
    protected function _modifyProperty(Property $property) : bool
    {
        $property->assign([ 'const' => TRUE ]);

        return TRUE;
    }
}
