<?php

namespace jf\php\generator;

/**
 * Trait que gestiona el valor del elemento.
 */
trait TValue
{
    /**
     * Indica que al elemento se le ha asignado un valor.
     *
     * Solamente útil en caso de que el valor sea nulo ya que si `value` no es nulo se
     * asume que se le ha asignado un valor.
     *
     * @var bool
     */
    public bool $hasValue = FALSE;

    /**
     * Indica si el elemento puede tener un valor nulo.
     *
     * En ese caso se debe colocar el valor a usar para indicarlo (p.e. `null` o `NULL`).
     *
     * @var string
     */
    public string $null = '';

    /**
     * Valor por defecto de la variable, normalmente un valor escalar.
     *
     * @var mixed
     */
    public mixed $value = NULL;

    /**
     * Renderiza el valor del trait.
     *
     * @return string
     */
    public function renderValue() : string
    {
        if ($this->value !== NULL)
        {
            $_value = '= ' . Formatter::formatValue($this->value);
        }
        elseif ($this->null && $this->hasValue)
        {
            $_value = '= ' . $this->null;
        }
        else
        {
            $_value = '';
        }

        return $_value;
    }

    /**
     * Asigna el valor al elemento.
     *
     * @param mixed $value Valor a asignar.
     *
     * @return static
     */
    public function setValue(mixed $value) : static
    {
        $this->hasValue = TRUE;
        $this->value    = $value;

        return $this;
    }
}
