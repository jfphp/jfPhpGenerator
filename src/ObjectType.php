<?php

namespace jf\php\generator;

/**
 * Enumera los tipos de elementos que se pueden crear.
 */
enum ObjectType : string
{
    /**
     * El objeto es una clase.
     */
    case ClassObject = ClassObject::class;

    /**
     * El objeto es una enumeración.
     */
    case EnumObject = EnumObject::class;

    /**
     * El objeto es una interfaz.
     */
    case InterfaceObject = InterfaceObject::class;

    /**
     * El objeto es un trait.
     */
    case TraitObject = TraitObject::class;

    /**
     * Trata de inferir una enumeración a partir de la instancia recibida.
     *
     * @param object $object Objeto a analizar.
     *
     * @return ObjectType|NULL
     */
    public static function tryFromObject(object $object) : ?ObjectType
    {
        return match (TRUE)
        {
            $object instanceof ClassObject     => self::ClassObject,
            $object instanceof EnumObject      => self::EnumObject,
            $object instanceof InterfaceObject => self::InterfaceObject,
            $object instanceof TraitObject     => self::TraitObject,
            default                            => NULL
        };
    }
}
